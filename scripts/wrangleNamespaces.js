const fs = require('fs').promises;
const path = require('path');
const GeneralTranslation = require('../src/datasets/General_Translations.json');

const fileWriter = async (locale, filename, rawdata) => {
  const data = JSON.stringify(rawdata);
  await fs.writeFile(
    path.join(__dirname, `../locales/${locale}/${filename}.json`),
    data,
    'utf8'
  );
  console.log(`saved to locales/${locale}/${filename}.json \n`);
};

const wrangleData = async (data) => {
  const availableNameSpaces = ['common.json', 'home.json', 'stories.json', 'resources.json', 'timeline.json']
  let enHost = {
    'common.json': {},
    'home.json': {},
    'stories.json': {},
    'resources.json': {},
    'timeline.json' : {}
  }
  let mmHost = {
    'common.json': {},
    'home.json': {},
    'stories.json': {},
    'resources.json': {},
    'timeline.json' : {}
  }

  data.map((eachRow) => {
    const { locale_filename, key } = eachRow;

    try {
      availableNameSpaces.map(currentNameSpace => {
        if (currentNameSpace === locale_filename) {
          enHost[currentNameSpace][key] = eachRow['value_ln:en']
          mmHost[currentNameSpace][key] = eachRow['value_ln:mm']
        }
      })
    } catch (e) { }
  });

  for (const currentNameSpace of availableNameSpaces) {
    const nameSpace = currentNameSpace.split('.json')[0]
    await fileWriter('en', nameSpace, enHost[currentNameSpace])
    await fileWriter('mm', nameSpace, mmHost[currentNameSpace])
  }
};

wrangleData(GeneralTranslation);
