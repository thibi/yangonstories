'use strict';

const airtableApiKey = require('./airtable_key.json')
const imageDownloader = require('./image-downloader.js')

const fs = require('fs');
const path = require('path');
let Airtable = require('airtable');

/*----------------------------------------

Metadata

//----------------------------------------*/

let datasetsToFetch = {
  'Eviction Mapping': {
    'baseId': 'appY75OEGhHRsODj9',
    'tableName': 'Eviction Mapping',
    'datasetName': 'Eviction Mapping'
  },
  'Fire Mapping': {
    'baseId': 'appY75OEGhHRsODj9',
    'tableName': 'Fire Mapping',
    'datasetName': 'Fire Mapping'
  },
  'Nationalisation Mapping': {
    'baseId': 'appY75OEGhHRsODj9',
    'tableName': 'Nationalisation Mapping',
    'datasetName': 'Nationalisation Mapping'
  },
  'Post-Coup Hlaing Tharyar': {
    'baseId': 'appY75OEGhHRsODj9',
    'tableName': 'Post-Coup Hlaing Tharyar',
    'datasetName': 'Post-Coup Hlaing Tharyar'
  },
  'Consolidated Resources': {
    'baseId': 'appWNINZAG7ZDmLYs',
    'tableName': 'Consolidated',
    'datasetName': 'Consolidated Resources'
  },
  'Cartoon Stories': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Cartoon Stories',
    'datasetName': 'Cartoon Stories'
  },
  'Video Stories': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Films',
    'datasetName': 'Video Stories'
  },
  'Interviews': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Interviews',
    'datasetName': 'Interviews'
  },
  'Poems': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Poems',
    'datasetName': 'Poems'
  },
  'Evictions Story': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Evictions Story',
    'datasetName': 'Evictions Story'
  },
  'Nationalisations Story': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Nationalisations Story',
    'datasetName': 'Nationalisations Story'
  },
  'Fire Story': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Fire Story',
    'datasetName': 'Fire Story'
  },
  'Timeline': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Timeline',
    'datasetName': 'Timeline'
  },
  'Post-coup Timelines': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Post-coup Timelines',
    'datasetName': 'Post-coup Timelines'
  },
  'General Translations': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'General Translations',
    'datasetName': 'General Translations'
  },
  'Post-Coup Essays Text': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Post-Coup Essays Text',
    'datasetName': 'Post-Coup Essays Text'
  },
  'Timeline Long Text': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'Timeline Long Text',
    'datasetName': 'Timeline Long Text'
  },
  'About YS': {
    'baseId': 'app7WqfJltYe9SUGX',
    'tableName': 'About YS',
    'datasetName': 'About YS'
  }
};

let filepaths = {
  'datasets': '../src/datasets/',
  'images': '../public/images/dataset-images/'
}

/*----------------------------------------

Test

//----------------------------------------*/

// let data = JSON.stringify(test);
// fs.writeFileSync('test.json', data);

/*----------------------------------------

Fetch Airtable data

//----------------------------------------*/

async function fetchData(tableData) {
  const { baseId, tableName, datasetName } = tableData;

  console.log("Fetching data from: ", tableName);

  let base = new Airtable({ apiKey: airtableApiKey.key }).base(baseId);

  let fetchedData = [];

  await base(tableName).select({
    // Selecting the first 3 records in Grid view:
    maxRecords: 1000,
    view: "Grid view"
  }).eachPage(async function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    function getImageID(d) {
      const id = d['id'];
      const type2ndHalf = d['type'].split("/")[1];
      const type = type2ndHalf === "heic" ? "png" : type2ndHalf;
      return id + "/" + type;
    }

    await records.forEach(function (record) {
      // console.log('Retrieved', record);
      const fields = record._rawJson.fields;
      let imageFieldName = null;

      if ('Images' in fields) imageFieldName = 'Images';
      if ('Image' in fields) imageFieldName = 'Image';
      if ('IMAGE' in fields) imageFieldName = 'IMAGE';

      if (imageFieldName) {
        const imageIds = fields[imageFieldName].map(d => getImageID(d))
        getImageFromAirtableResponse(fields[imageFieldName], imageIds)
        fields['ImageIds'] = imageIds;
      }

      fetchedData.push(fields);
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    await fetchNextPage();

  }, function done(err) {
    if (err) { console.error(err); return; }

    let data = JSON.stringify(fetchedData, null, 2);
    const fileName = datasetName.split(" ").join("_")
    fs.writeFileSync(path.join(__dirname, filepaths['datasets'] + fileName + '.json'), data);
  });

}

/*----------------------------------------

Download images from URL

//----------------------------------------*/

function getImageFromAirtableResponse(imagesObj, imageIds) {
  console.log(imageIds);


  async function makeImgObj() {
    let imgs = [];
    for (let i = 0; i < imageIds.length; i++) {
      const [fileNameId, fileType] = imageIds[i].split("/");

      const filenameSm = fileNameId + "_sm";
      const filenameLg = fileNameId + "_lg";

      // console.log(fileNameId);
      // console.log(imagesObj[i]["thumbnails"])

      const uriSm = imagesObj[i]["thumbnails"]["small"]["url"];
      const uriLg = imagesObj[i]["thumbnails"]["large"]["url"];

      if (fileType === 'pdf') {
        imgs.push({
          'uri': imagesObj[i]['url'],
          'filename': fileNameId + "_attachment"
        })
      }

      imgs.push({
        'uri': uriSm,
        'filename': filenameSm
      })

      imgs.push({
        'uri': uriLg,
        'filename': filenameLg
      })
    }
    return imgs;
  }

  makeImgObj()
    .then((data) => {
      imageDownloader({
        imgs: data,
        dest: filepaths['images'], //destination folder
      })
        .then((info) => {
          console.log('all done', info.map(d => d.path))
        })
        .catch((error, response, body) => {
          console.log('something goes bad!')
          console.log(error)
        })
    })


}


/*----------------------------------------

Fetch data

//----------------------------------------*/

// uncomment below to fetch data from Airtable and write it into the repo

// fetchData(datasetsToFetch['Eviction Mapping']);
// fetchData(datasetsToFetch['Fire Mapping']);
// fetchData(datasetsToFetch['Nationalisation Mapping']);
// fetchData(datasetsToFetch['Post-Coup Hlaing Tharyar']);
// fetchData(datasetsToFetch['Consolidated Resources']);
// fetchData(datasetsToFetch['Cartoon Stories']);
// fetchData(datasetsToFetch['Interviews']); [Done]
// fetchData(datasetsToFetch['Poems']); [Done]
// fetchData(datasetsToFetch['Video Stories']);
// fetchData(datasetsToFetch['Evictions Story']); [Done]
// fetchData(datasetsToFetch['Nationalisations Story']); [Done]
// fetchData(datasetsToFetch['Fire Story']); [Done]
// fetchData(datasetsToFetch['Timeline']);
// fetchData(datasetsToFetch['Post-coup Timelines']);
// fetchData(datasetsToFetch['General Translations']);
fetchData(datasetsToFetch['Post-Coup Essays Text']);
//fetchData(datasetsToFetch['Timeline Long Text']);
//fetchData(datasetsToFetch['About YS']);