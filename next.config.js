/** @type {import('next').NextConfig} */
// import { i18n } from './next-i18next.config'
// eslint-disable-next-line 
const nextTranslate = require('next-translate');
const isProd = process.env.NODE_ENV === 'production'
module.exports = {
  eslint: {
    dirs: ['src'],
  },
  typescript: {
    // !! WARN !!
    // Dangerously allow production builds to successfully complete even if
    // your project has type errors.
    // !! WARN !!
    ignoreBuildErrors: true,
  },

  reactStrictMode: true,
  // assetPrefix: isProd ? '/yangonstories/' : ''
  assetPrefix: isProd ? '' : '',
  ...nextTranslate()

  // Uncoment to add domain whitelist
  // images: {
  //   domains: [
  //     'res.cloudinary.com',
  //   ],
  // },
};
