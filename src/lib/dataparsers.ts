import { htyCenter, naypyitawCenter, yangonCenter } from '@/lib/constants';
import {
  makeCrossfilterDimsAndGroups,
  makeEmptyFilters,
  makeTwoDigit,
} from './utils';
import { max, min, sum } from 'd3-array';

import MapEvictionsData from '../datasets/Eviction_Mapping.json';
import MapFireData from '../datasets/Fire_Mapping.json';
import MapHlaingTharyarData from '../datasets/Post-Coup_Hlaing_Tharyar.json';
import MapNationalisationData from '../datasets/Nationalisation_Mapping.json';
import crossfilter from 'crossfilter2';
import { getFilterCatForTagDimension } from '../lib/utils';
import { nest } from 'd3-collection';
import resourcesData from '../datasets/Consolidated_Resources.json';
import timelineData from '../datasets/Timeline.json';

function getYearRange(d: any) {
  return d['Year'] !== ''
    ? d['Year'] + '-' + d['Year']
    : d['Starting year'] + '-' + d['Ending year'];
}

export const eventTypes = [
  'Policy & Legal schemes',
  'Major events',
  'City development',
  'Case of spatial violence',
];

export const householdsCats = [
  ['Unknown', []],
  ['0 to 99', [0, 99]],
  ['100 to 499', [100, 499]],
  ['500 to 999', [500, 499]],
  ['1000 or more', [1000, 9999999999]],
];

export function parseTimelineData() {
  // console.log(timelineData)
  const nestedData = nest()
    .key((d: any) => d['Category'])
    .key((d: any) =>
      d['Category'] === 'Period' ? d['Title'] : getYearRange(d)
    )
    .object(timelineData);

  const periods = {};
  let events: string | any[] = [];

  const periodNames = Object.keys(nestedData.Period);

  for (let i = 0; i < periodNames.length; i++) {
    periods[periodNames[i]] = {};

    const periodStart = nestedData.Period[periodNames[i]][0]['Starting year'];
    const periodEnd = nestedData.Period[periodNames[i]][0]['Ending year'];

    periods[periodNames[i]].name = periodNames[i];
    periods[periodNames[i]].yearRange = [periodStart, periodEnd];
    periods[periodNames[i]].events = timelineData
      .filter((d) => {
        const [start] = getYearRange(d).split('-');
        const inPeriod = start >= periodStart && start <= periodEnd;
        // (end >= periodStart && end <= periodEnd) ||
        // (periodStart >= start && periodStart <= end) ||
        // (periodEnd >= start && periodEnd <= end);

        return eventTypes.includes(d['Category']) && inPeriod;
      })
      .map((d: any, j: number) => {
        return {
          id: (i + 1) * 1000 + j,
          yearRange: getYearRange(d)
            .split('-')
            .map((d) => +d),
          eventType: d['Category'],
          title: d['Title'],
          description: d['Description'],
          remarks: d['Remarks'],
        };
      });

    events = events.concat(periods[periodNames[i]].events);
  }

  const periodsSortedArray = Object.values(periods)
    .map((period) => {
      return {
        name: period.name,
        yearRange: period.yearRange,
      };
    })
    .sort((a, b) => {
      return a.yearRange[0] - b.yearRange[0];
    });

  return {
    periods: periods,
    periodsSortedArray: periodsSortedArray,
    events: events,
    allYearsRange: [
      min(periodNames.map((d, i) => +periods[periodNames[i]].yearRange[0])),
      max(periodNames.map((d, i) => +periods[periodNames[i]].yearRange[1])),
    ],
    validLabels: nest()
      .key((d: any) => d.yearRange[0])
      .object(events),
  };
}

const assignHouseholdCat = (n: any, cats: any) => {
  // console.log(cats,n)
  // cat looks like this: ['0 to 99',[0,99]]
  for (let i = 0; i < cats.length; i++) {
    const cat = cats[i];
    let catMin = 0;
    let catMax = 0;
    if (cat[1].length > 0) {
      catMin = +cat[1][0];
      catMax = +cat[1][1];
    }

    if (+n >= catMin && +n <= catMax) return cat[0];
  }
  return householdsCats[householdsCats.length - 1][0];
};

/*
******************************
  Evictions data
******************************
*/

export function parseMapEvictionsData() {
  // 0: "ID"
  // 1: "Data entry"
  // 2: "Map data entry"
  // 3: "Type of spatial violence"
  // 4: "Type of squatter area (type of affected settlement?)"
  // 5: "Coordinates X"
  // 6: "Coordinates Y"
  // 7: "Mapping remarks"
  // 8: "Location "
  // 9: "Township (menu)"
  // 10: "Number of affected"
  // 11: "Number of affected on Mapping system"
  // 12: "New Location"
  // 13: "Time period (menu)"
  // 14: "Year "
  // 15: "Remarks"
  // 16: "Shorten source of information (To be shown on website)"
  // 17: "Source of information"
  // 18: "Rationale for the eviction"
  // 19: "Detailed Rationales"
  // 20: "Internal Information"

  // console.log(evictionsData)

  const evictionsData = MapEvictionsData;

  const filterCats = [
    'Period',
    'Households',
    { filterCat: 'Rationale', tagDimension: true },
    'Township',
  ];

  const filterCatsNameOnly = filterCats.map((cat) => {
    return getFilterCatForTagDimension(cat).filterCat;
  });

  const filters = Object.fromEntries(
    filterCatsNameOnly.map((cat) => [cat, []])
  );
  let parsedData = [];

  // console.log(filters);

  for (let i = 0; i < evictionsData.length; i++) {
    const row = evictionsData[i];

    // console.log(row);

    const parseYear = (yearString) => {
      if (!yearString) return '';
      if (yearString.includes('-')) return +yearString.split('-')[0];
      if (yearString.includes('?')) return +yearString.split('?')[0];
      return +yearString;
    };

    const id = row['ID'];
    const year = parseYear(row['Year']);
    const period = row['Time period (menu)'] || 'Unknown';
    const households = +row['Number of affected households on Mapping system'];
    const householdsFull = row['Number of affected'];
    const rationale = row['Rationale for the eviction'] || ['Unknown'];
    const detailedRationale = row['Detailed Rationales'] || '';
    const remarks = row['Remarks'] || '';
    const township = row['Township (menu)'] || 'Unknown';

    // console.log(households)

    if (period && !filters['Period'].includes(period)) {
      filters['Period'].push(period);
    }

    for (let i = 0; i < rationale.length; i++) {
      if (rationale[i] && !filters['Rationale'].includes(rationale[i])) {
        filters['Rationale'].push(rationale[i]);
      }
    }

    if (township && !filters['Township'].includes(township)) {
      filters['Township'].push(township);
    }

    const householdsCat = assignHouseholdCat(households, householdsCats);

    // console.log(householdsCat)

    const pointDataAvailable =
      !isNaN(+row['Coordinates X']) && !isNaN(+row['Coordinates Y']);

    parsedData.push({
      type: 'Feature',
      id: id,
      properties: {
        id: id,
        tableId: id,
        Year: year,
        Period: period,
        HouseholdsNum: households,
        Households: householdsCat,
        HouseholdsFull: householdsFull,
        Rationale: rationale,
        DetailedRationale: detailedRationale,
        InfoSource: row['Shorten source of information'],
        Remarks: remarks,
        Township: township,
        Location: row['Location'],
        pointDataAvailable: pointDataAvailable,
        latitude: +row['Coordinates Y'],
        longitude: +row['Coordinates X'],
      },
      geometry: {
        type: 'Point',
        coordinates: pointDataAvailable
          ? [+row['Coordinates X'], +row['Coordinates Y']]
          : [naypyitawCenter.longitude, naypyitawCenter.latitude],
      },
    });
  }

  parsedData = parsedData.sort(
    (a, b) => b.properties.HouseholdsNum - a.properties.HouseholdsNum
  );

  filters['Households'] = householdsCats.map((cat) => cat[0]);
  filters['Rationale'] = filters['Rationale'].sort();
  filters['Township'] = filters['Township'].sort();

  const xf = crossfilter(parsedData.map((row) => row['properties']));

  return {
    filterCats: filterCats,
    filters: filters,
    xfilter: xf,
    data: {
      type: 'FeatureCollection',
      features: [...parsedData],
    },
    tableData: [...parsedData.map((d) => d.properties)],
  };
}

/*
******************************
  Fire data
******************************
*/

export function parseMapFireData() {
  // console.log(fireData);
  // Date: "10"
  // ID: "0001"
  // Month: "1"
  // Nb of affected structures (counted in the system): "100"
  // Nb of houses/shops/households affected: "100 houses"
  // Original name or link of the Source: "https://www.rfa.org/burmese/news/sanmyoe-village-fireburn-01192022014722.html"
  // Place (as close as possible): "San Myo Village"
  // Shorten form of Source: "Radio Free Asia, 2022"
  // State/Region: "Magwe"
  // Township: "Gangaw"
  // TspCentroidLat: "22.2502722159964"
  // TspCentroidLon: "22.2502722159964"
  // Type of Fire: "arson"
  // What happened: "SAC forces set fire to 100 houses on 18 January"
  // X: "94.13989613"
  // Y: "22.64823627"
  // Year: "2022"

  const fireData = MapFireData;

  const filterCats = ['State', 'Township', 'Households'];

  const filters = Object.fromEntries(filterCats.map((cat) => [cat, []]));
  const parsedData = [];

  // console.log(fireData)

  for (let i = 0; i < fireData.length; i++) {
    const row = fireData[i];

    const id = row['ID'];
    // const year = +row['Year'].split('-')[0];
    const year = +row['Year'];
    const households =
      +row['Nb of affected structures (counted in the system)'];
    const householdsFull = row['Nb of houses/shops/households affected'];
    const township = row['Township'];
    const state = row['State/Region'];
    const period = 'Post Feb 2021 Coup';

    // if (!filters['Period'].includes(period)) {
    //   filters['Period'].push(period);
    // }

    if (!filters['State'].includes(state)) {
      filters['State'].push(state);
    }

    if (!filters['Township'].includes(township)) {
      filters['Township'].push(township);
    }

    const householdsCat = assignHouseholdCat(households, householdsCats);

    const pointDataAvailable =
      !isNaN(+row['TspCentroidLon']) && !isNaN(+row['TspCentroidLat']);

    parsedData.push({
      id: township,
      tableId: id,
      Year: year,
      HouseholdsNum: households,
      Households: householdsCat,
      HouseholdsFull: householdsFull,
      Township: township,
      Period: 'Post Feb 2021 Coup',
      State: state,
      InfoSource: row['Shorten form of Source'],
      Location: row['Place (as close as possible)'],
      pointDataAvailable: pointDataAvailable,
      latitude: +row['TspCentroidLat'],
      longitude: +row['TspCentroidLon'],
    });
  }

  filters['Households'] = householdsCats.map((cat) => cat[0]);
  filters['Township'] = filters['Township'].sort();

  const nestedData = nest()
    .key((d: any) => d['Township'])
    .entries(parsedData);

  // console.log(nestedData);

  const dataByTownship = [];

  for (let i = 0; i < nestedData.length; i++) {
    const row = nestedData[i].values;
    const sumHouseholds = sum(row, (d) => d['HouseholdsNum']);
    const newEntry = {
      type: 'Feature',
      id: row[0].id,
      properties: {
        id: row[0].id,
        Year: Array.from(new Set(row.map((d) => d.Year))),
        HouseholdsNum: sumHouseholds,
        Households: assignHouseholdCat(sumHouseholds, householdsCats),
        Period: row[0].Period,
        Township: row[0].Township,
        State: row[0].State,
        pointDataAvailable: row[0].pointDataAvailable,
        latitude: row[0]['latitude'],
        longitude: row[0]['longitude'],
      },
      geometry: {
        type: 'Point',
        coordinates: row[0].pointDataAvailable
          ? [row[0]['longitude'], row[0]['latitude']]
          : [yangonCenter.longitude, yangonCenter.latitude],
      },
    };
    dataByTownship.push(newEntry);
  }

  // console.log(dataByTownship);

  const xf = crossfilter(dataByTownship.map((row) => row['properties']));

  return {
    filterCats: filterCats,
    filters: filters,
    xfilter: xf,
    data: {
      type: 'FeatureCollection',
      features: [...dataByTownship],
    },
    tableData: [...parsedData],
  };
}

/*
******************************
  Nationalisation data
******************************
*/

export function parseMapNationalisationData() {
  // DDMMYY: "27-11-2021"
  // Date: "27"
  // ID: "0001"
  // Month: "11"
  // Place (as close as possible): "Center of Phaya Gyi village, Twante Tsp"
  // Shorten form of Source: "DVB, 2021"
  // Source: "http://burmese.dvb.no/archives/502884"
  // State/Region: "Yangon"
  // Township: "Twantay"
  // TspCentroidLat: "16.7444520169079"
  // TspCentroidLon: "95.9527307498435"
  // What happened: "More than 30 soldiers and police cordoned off the house of protest leader Ko Htet, erecting a sign and blocking it with restricted ropes. "
  // Y: "16.657393"
  // Year: "2021"
  // x: "96.016559"

  const nationalisationData = MapNationalisationData;
  // console.log(nationalisationData);

  const filterCats = ['State', 'Township'];

  const filters = Object.fromEntries(filterCats.map((cat) => [cat, []]));
  const parsedData = [];

  for (let i = 0; i < nationalisationData.length; i++) {
    const row = nationalisationData[i];

    const id = row['ID'];
    // const year = +row['Year'].split('-')[0];
    const year = +row['Year'];
    const township = row['Township'];
    const state = row['State/Region'];
    const period = 'Post Feb 2021 Coup';

    // if (!filters['Period'].includes(period)) {
    //   filters['Period'].push(period);
    // }

    if (!filters['State'].includes(state)) {
      filters['State'].push(state);
    }

    if (!filters['Township'].includes(township)) {
      filters['Township'].push(township);
    }

    const pointDataAvailable =
      !isNaN(+row['TspCentroidLon']) && !isNaN(+row['TspCentroidLat']);

    parsedData.push({
      id: township,
      tableId: id,
      Year: year,
      Township: township,
      Period: 'Post Feb 2021 Coup',
      State: state,
      Location: row['Place (as close as possible)'],
      Detail: row['Detail'],
      WhatIsThereNow: row['What is there now?'],
      InfoSource: row['Shorten form of Source'],
      pointDataAvailable: pointDataAvailable,
      latitude: +row['TspCentroidLat'],
      longitude: +row['TspCentroidLon'],
    });
  }

  filters['Township'] = filters['Township'].sort();

  const nestedData = nest()
    .key((d: any) => d['Township'])
    .entries(parsedData);

  // console.log(nestedData);

  const dataByTownship = [];

  for (let i = 0; i < nestedData.length; i++) {
    const row = nestedData[i].values;
    const sumEvents = sum(row, () => 1);
    const newEntry = {
      type: 'Feature',
      id: row[0].id,
      properties: {
        id: row[0].id,
        Year: Array.from(new Set(row.map((d) => d.Year))),
        Nationalisations: sumEvents,
        Period: row[0].Period,
        Township: row[0].Township,
        State: row[0].State,
        pointDataAvailable: row[0].pointDataAvailable,
        latitude: row[0]['latitude'],
        longitude: row[0]['longitude'],
      },
      geometry: {
        type: 'Point',
        coordinates: row[0].pointDataAvailable
          ? [row[0]['longitude'], row[0]['latitude']]
          : [naypyitawCenter.longitude, naypyitawCenter.latitude],
      },
    };
    dataByTownship.push(newEntry);
  }

  // console.table(dataByTownship.map(d => d.properties));

  const xf = crossfilter(dataByTownship.map((row) => row['properties']));

  return {
    filterCats: filterCats,
    filters: filters,
    xfilter: xf,
    data: {
      type: 'FeatureCollection',
      features: [...dataByTownship],
    },
    tableData: [...parsedData],
  };
}

/*
******************************
  Hlaing Thar Yar data
******************************
*/

export function parseMapHtyData() {
  // Category: "Protest/ Strike"
  // Data Entry: "MK"
  // Day: 5
  // Headline: "Peaceful protest in Hlaing Thar Yar Township"
  // ID: "MK1020502"
  // Lat: 16.91692647
  // Location: "No.56/57, Min Gyi Mahar Min Gaung Street, Shwe Lin Pan Industrial Zone, Yangon"
  // Long: 96.05911964
  // Media: "https://drive.google.com/file/d/1FWGRpb3PYctP7yQ9VieG2VcmQEztibtL/view?usp=sharing"
  // Media Credit: "The Voice Journal"
  // Month: 2
  // Source: "https://www.facebook.com/116494362285184/posts/792060924728521/"
  // Text: "Garment workers from Hangkei-Myanmar garment factory in Shwe Lin Ban Industrial Zone, Hlaing Thar Yar joined anti-coup protesting during their lunch time."
  // Year: 2021

  const htyData = MapHlaingTharyarData;

  const filterCats = ['Category', 'Month'];

  const filterCatsNameOnly = filterCats.map((cat) => {
    return getFilterCatForTagDimension(cat).filterCat;
  });

  const filters = Object.fromEntries(
    filterCatsNameOnly.map((cat) => [cat, []])
  );
  const parsedData = [];

  // console.log(filters);

  for (let i = 0; i < htyData.length; i++) {
    const row = htyData[i];

    // console.log(row);

    // const parseYear = (yearString) => {
    //   if (!yearString) return '';
    //   if (yearString.includes('-')) return +yearString.split('-')[0];
    //   if (yearString.includes('?')) return +yearString.split('?')[0];
    //   return +yearString;
    // }

    const id = row['ID'];
    const dateString =
      row['Year'] +
      '-' +
      makeTwoDigit(row['Month']) +
      '-' +
      makeTwoDigit(row['Day']);
    const monthYear = row['Year'] + '-' + makeTwoDigit(row['Month']);
    const location = row['Location'] + '-' + row['Month'];
    const headline = row['Headline'];
    const category = row['Category'];
    const media = row['Media'];
    const mediaCredit = row['Media Credit'];
    const source = row['Source'];
    const text = row['Text'];

    // console.log(households)

    if (category && !filters['Category'].includes(category)) {
      filters['Category'].push(category);
    }

    if (monthYear && !filters['Month'].includes(monthYear)) {
      filters['Month'].push(monthYear);
    }

    const pointDataAvailable = !isNaN(+row['Long']) && !isNaN(+row['Lat']);

    parsedData.push({
      type: 'Feature',
      id: id,
      properties: {
        id: id,
        Date: dateString,
        Month: monthYear,
        Location: location,
        Headline: headline,
        Category: category,
        Media: media,
        MediaCredit: mediaCredit,
        Source: source,
        Text: text,
        pointDataAvailable: pointDataAvailable,
        latitude: pointDataAvailable ? +row['Lat'] : htyCenter.latitude,
        longitude: pointDataAvailable ? +row['Long'] : htyCenter.longitude,
      },
      geometry: {
        type: 'Point',
        coordinates: pointDataAvailable
          ? [+row['Long'], +row['Lat']]
          : [htyCenter.longitude, htyCenter.latitude],
      },
    });
  }

  // console.table(parsedData.map(d=>d.properties))

  filters['Category'] = filters['Category'].sort();
  filters['Month'] = filters['Month'].sort();

  const xf = crossfilter(parsedData.map((row) => row['properties']));

  return {
    filterCats: filterCats,
    filters: filters,
    xfilter: xf,
    data: {
      type: 'FeatureCollection',
      features: [...parsedData],
    },
    tableData: [...parsedData.map((d) => d.properties)],
  };
}

/*
******************************
  Resources data
******************************
*/

export function parseResourcesData() {
  const dataset = resourcesData;
  // const filterCats = ['Year', 'Author / Newspaper', 'Period', 'Content type'];
  const filterCats = ['Period', 'Content type', 'Year'];
  const fieldsToParse = [
    { fieldName: 'ID', fieldType: 'id', showInTable: false },
    { fieldName: 'Title', fieldType: 'string', showInTable: true },
    { fieldName: 'Author / Newspaper', fieldType: 'string', showInTable: true },
    { fieldName: 'Year', fieldType: 'number', showInTable: true },
    { fieldName: 'Period', fieldType: 'string', showInTable: true },
    { fieldName: 'Content type', fieldType: 'string', showInTable: true },
    { fieldName: 'URL', fieldType: 'url', showInTable: true },
    { fieldName: 'ImageIds', fieldType: 'attachment', showInTable: false },
  ];

  return parseGeneralDataset(dataset, filterCats, fieldsToParse);
}

export function parseGeneralDataset(
  dataSet,
  filterCats,
  fieldsToParse,
  produceMapData,
  geoFields
) {
  // "Title": "Expansion of Yangon",
  // "Author / Newspaper": "Urban and Regional Planning Division, Housing Department",
  // "Year": 1984,
  // "Period": "Ne Win",
  // "Content type": "Maps",
  // "Notes": "The map shows different boundaries over the time since the period of King Alaung Pha Ya (1755) ",
  // "ImageIds": [
  //   "attFQ1tQHudZGfqT9/jpeg"
  // ]

  // console.log(dataSet);
  const numberForUnknown = -999999;

  const filters = Object.fromEntries(filterCats.map((cat) => [cat, []]));
  const parsedData = [];
  const mapData = [];
  const idField =
    fieldsToParse.filter((field) => field.fieldType === 'id').length === 1
      ? fieldsToParse.filter((field) => field.fieldType === 'id')[0].fieldName
      : null;
  const attachmentFields =
    fieldsToParse.filter((field) => field.fieldType === 'attachment').length > 0
      ? fieldsToParse
          .filter((field) => field.fieldType === 'attachment')
          .map((field) => field.fieldName)
      : null;

  if (produceMapData) {
    if (!geoFields)
      throw 'Asked to produce map data but geo fields not defined';
    if (!geoFields.lon || !geoFields.lat)
      throw "Asked to produce map data but geo fields don't have lat lon specified";
  }

  for (let i = 0; i < dataSet.length; i++) {
    const row = dataSet[i];
    const parsedRow = {};

    // Add ID field to parsedRow
    if (idField === null) {
      parsedRow['id'] = i;
    } else {
      parsedRow['id'] = row[idField];
    }

    // Add geo data to parsedRow
    if (produceMapData) {
      const pointDataAvailable =
        !isNaN(+row[geoFields.lon]) && !isNaN(+row[geoFields.lat]);

      parsedRow['pointDataAvailable'] = pointDataAvailable;
      parsedRow['latitude'] = +row[geoFields.lat];
      parsedRow['longitude'] = +row[geoFields.lon];
    }

    // Add attachments to parsedRow
    if (attachmentFields !== null) {
      for (const aField of attachmentFields) {
        parsedRow[aField] = row[aField] ? row[aField] : [];
      }
    }

    // Loop through fields to fill out filter categories and parse remaining fields
    for (const field of fieldsToParse) {
      const { fieldName, fieldType } = field;

      // Make sure there are no undefined values
      const value =
        typeof row[fieldName] === 'undefined'
          ? fieldType === 'number'
            ? numberForUnknown
            : 'Unknown'
          : row[fieldName];

      // Add values to filter categories
      if (filterCats.includes(fieldName)) {
        if (!filters[fieldName].includes(value)) {
          if (fieldType === 'number' && value !== numberForUnknown) {
            filters[fieldName].push(value);
          } else if (fieldType !== 'number') {
            filters[fieldName].push(value);
          }
        }
      }

      // Add data fields
      if (!['id', 'attachment', 'lat', 'lon'].includes(fieldType)) {
        if (fieldType === 'number') {
          parsedRow[fieldName] = +value;
        }
        if (['string', 'url'].includes(fieldType)) {
          parsedRow[fieldName] = value;
        }
      }
    }

    parsedData.push(parsedRow);

    // Create map data row
    if (produceMapData) {
      const parsedMapRow = {
        type: 'Feature',
        id: parsedRow['id'],
        properties: {
          ...parsedRow,
        },
        geometry: {
          type: 'Point',
          coordinates: parsedRow.pointDataAvailable
            ? [parsedRow['longitude'], parsedRow['latitude']]
            : [naypyitawCenter.longitude, naypyitawCenter.latitude],
        },
      };
      mapData.push(parsedMapRow);
    }
  }

  // Sort filter categories
  for (const filter of filterCats) {
    filters[filter] = filters[filter].sort();
  }

  // console.log(parsedData);
  // console.log(filters);

  const xf = crossfilter(parsedData);
  const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(filterCats, xf);

  const tableColumns = fieldsToParse
    .filter((field) => field.showInTable)
    .map((field) => {
      return {
        accessor: field.fieldName,
        Header: field.fieldName,
        type: field.fieldType,
      };
    });

  return {
    filterCats: filterCats,
    filters: filters,
    xFilter: xf,
    xfDims: xfDims,
    xfGroups: xfGroups,
    emptyFilters: makeEmptyFilters(filters),
    mapData:
      produceMapData && mapData
        ? {
            type: 'FeatureCollection',
            features: [...mapData],
          }
        : null,
    tableData: [...parsedData],
    tableColumns: tableColumns,
  };
}
