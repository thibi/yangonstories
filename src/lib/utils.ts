const isProd = process.env.NODE_ENV === 'production';

const withBase = (val: string) => {
  // return isProd ? `/yangonstories/${val}` : `${val}`;
  return `${val}`;
};

const truncate = (str, n) => {
  return str.length > n ? str.substr(0, n - 1) + '...' : str;
};

export { isProd, truncate, withBase };

export const flattenArray = (nestedArray) => {
  const flattenedArray = [];

  for (let i = 0; i < nestedArray.length; i++) {
    for (let j = 0; j < nestedArray[i].length; j++) {
      flattenedArray.push(nestedArray[i][j]);
    }
  }

  return flattenedArray;
};

export const getFilterCatForTagDimension = (filterCat) => {
  if (typeof filterCat === 'object') {
    return filterCat;
  } else {
    return {
      filterCat: filterCat,
      tagDimension: false,
    };
  }
};

export const makeCrossfilterDimsAndGroups = (filterCats, xf) => {
  const filterGroups = {};
  const filterDimensions = {};

  // console.log(xf);

  for (let i = 0; i < filterCats.length; i++) {
    const { filterCat, tagDimension } = getFilterCatForTagDimension(
      filterCats[i]
    );

    filterDimensions[filterCat] = xf.dimension((d) => {
      return d[filterCat];
    }, tagDimension);
  }

  for (let i = 0; i < filterCats.length; i++) {
    const { filterCat } = getFilterCatForTagDimension(filterCats[i]);
    filterGroups[filterCat] = filterDimensions[filterCat].group();
  }

  return {
    xfDims: filterDimensions,
    xfGroups: filterGroups,
  };
};

export const arrayStringParser = (arrayString, seperator) => {
  try {
    const arrayParsed = JSON.parse(arrayString);
    if (seperator === undefined) seperator = ', ';
    return arrayParsed.join(seperator);
  } catch (e) {
    return arrayString;
  }
  // return arrayString;
};

export const makeEmptyFilters = (filtersObj: any) => {
  const entries = Object.keys(filtersObj).map((key) => [key, []]);
  return Object.fromEntries(entries);
};

export const filterHandler =
  (
    dataset,
    filtersForDataSet,
    filtersSetter,
    xfForDataSet,
    xfDimsForDataSet,
    xfGroupsForDataset,
    filterAll
  ) =>
  (filterCat: any, filterVal: any) => {
    // console.log(xfGroupsForDataset[filterCat].all(),filterVal)

    filterVal = Array.isArray(filterVal) ? filterVal : [filterVal];
    const currentFiltersTemp = Object.assign({}, filtersForDataSet);

    if (filterAll) {
      currentFiltersTemp[filterCat] = [...filterVal];
    } else {
      // Update currentFiltersTemp with newly filtered value(s)
      for (let i = 0; i < filterVal.length; i++) {
        // if currently in filter, remove it
        if (currentFiltersTemp[filterCat].includes(filterVal[i])) {
          currentFiltersTemp[filterCat] = currentFiltersTemp[filterCat].filter(
            (currentFilterItem: any) => currentFilterItem !== filterVal[i]
          );
        }
        // if currently not in filter, add it
        else {
          currentFiltersTemp[filterCat].push(filterVal[i]);
        }
      }
    }

    // console.log(currentFiltersTemp);
    xFilterHandler(
      dataset,
      currentFiltersTemp,
      xfForDataSet,
      xfDimsForDataSet,
      xfGroupsForDataset,
      filterCat,
      filterVal
    );
    filtersSetter(currentFiltersTemp);
  };

export const buildFilteredDataSet = (
  dataset,
  xfDims,
  currentFilters,
  table,
  emptyMeansNone
) => {
  // console.log(dataset, xfDims, currentFilters, table, emptyMeansNone);
  let ids = [];
  const filtersCount = Object.values(currentFilters).reduce(
    (total, newArray) => total + newArray.length,
    0
  );

  if (xfDims == null || Object.keys(xfDims).length === 0) {
    return dataset;
  }

  ids =
    filtersCount === 0 && emptyMeansNone
      ? []
      : xfDims[Object.keys(currentFilters)[0]].top(Infinity).map((d) => d.id);

  if (table === true) return buildFilteredDataSetTableData(dataset, ids);

  return buildFilteredDataSetMapFeatures(dataset, ids);
};

export const buildFilteredDataSetMapFeatures = (dataset, ids) => {
  const features = dataset.features.filter((d) => ids.includes(d.id));

  return {
    type: 'FeatureCollection',
    features: [...features],
  };
};

export const buildFilteredDataSetTableData = (dataset, ids) => {
  return dataset.filter((d) => ids.includes(d.id));
};

export const xFilterHandler = (
  dataset,
  currentFilters,
  xfForDataSet,
  xfDimsForDataSet,
  xfGroupsForDataset,
  filterCat
) => {
  // currentFilters
  // console.log(currentFilters);
  // console.log("dimentions for " + filterCat,xfDimsForDataSet);

  function multivalue_filter(values) {
    return function (v) {
      return values.indexOf(v) !== -1;
    };
  }

  if (currentFilters[filterCat].length === 0) {
    xfDimsForDataSet[filterCat].filterAll();
  } else {
    xfDimsForDataSet[filterCat].filterFunction(
      multivalue_filter(currentFilters[filterCat])
    );
  }
};

export const getImagePath = (imageId, size) => {
  const sizeSuffix = size === 'sm' ? '_sm' : '_lg';
  const [fileName, ext] = imageId.split('/');

  return withBase(
    '/images/dataset-images/' + fileName + sizeSuffix + '.' + ext
  );
};

export const isURL = (str) => {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i'
  ); // fragment locator
  return !!pattern.test(str);
};

export const makeTwoDigit = (str) => {
  if (typeof str !== 'string') str = String(str);
  if (str.length === 1) return '0' + str;
  if (str.length === 0) return '00' + str;
  return str;
};

export function swapKeysAndValues(obj) {
  // 👇️ [['color', 'blue'], ['fruit', 'apple']]
  const swapped = Object.entries(obj).map(([key, value]) => [value, key]);

  return Object.fromEntries(swapped);
}
