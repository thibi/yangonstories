export const yangonCenter = {
  longitude: +96.16769,
  latitude: +16.8634,
};

export const naypyitawCenter = {
  longitude: +96.115,
  latitude: +19.7475,
};

export const htyCenter = {
  longitude: 96.06390640912895,
  latitude: 16.862438261902767,
};
// export const periodStyles = {
//   'Colonial Period': { background: '#6B322F', color: 'white' },
//   'World War II and Japanese Occupation': {
//     background: '#E96867',
//     color: 'black',
//   },
//   '(Post) Independence': { background: '#6B322F', color: 'black' },
//   'Parliamentary Government': { background: '#6B322F', color: 'white' },
//   'Caretaker Government': { background: '#E96867', color: 'black' },
//   'Union Goverment': { background: '#6B322F', color: 'white' },
//   'Ne Win and the BSPP Regime': { background: '#EEB373', color: 'black' },
//   'Ne Win': { background: '#EEB373', color: 'black' },
//   'SLORC and SPDC': { background: '#5A4D50', color: 'black' },
//   'Transitional Period': { background: '#91BCB2', color: 'black' },
//   Transition: { background: '#91BCB2', color: 'black' },
//   'Coup and Post-coup': { background: '#F0E9C6', color: 'black' },
//   'Post Feb 2021 Coup': { background: '#F0E9C6', color: 'black' },
// };

export const periodStyles = {
  'Colonial Period': { background: '#6B322F', color: 'white' },
  'World War II and Japanese Occupation': {
    background: '#6B322F',
    color: 'white',
  },
  '(Post) Independence': { background: '#E96867', color: 'black' },
  'Parliamentary Government': { background: '#E96867', color: 'black' },
  'Caretaker Government': { background: '#E96867', color: 'black' },
  'Union Goverment': { background: '#E96867', color: 'black' },
  'Ne Win and the BSPP Regime': { background: '#EEB373', color: 'black' },
  'Ne Win': { background: '#EEB373', color: 'black' },
  'SLORC and SPDC': { background: '#5A4D50', color: 'white' },
  'Transitional Period': { background: '#91BCB2', color: 'black' },
  Transition: { background: '#91BCB2', color: 'black' },
  'Coup and Post-coup': { background: '#F0E9C6', color: 'black' },
  'Post Feb 2021 Coup': { background: '#F0E9C6', color: 'black' },
  Unknown: { background: '#fffff5', color: 'black' },
  background: { background: 'rgba(91,75,81,0.1)', color: '#444' },
};

export const postCoupCategoryStyles = {
  Violence: { background: '#6B322F', color: 'white' },
  'Protest/ Strike': { background: '#E96867', color: 'black' },
  Resistance: { background: '#EEB373', color: 'black' },
  'Announcements and Statements': { background: '#5A4D50', color: 'white' },
  'Coercion and Abuse of Power': { background: '#91BCB2', color: 'black' },
  Unknown: { background: '#fffff5', color: 'black' },
  background: { background: 'rgba(91,75,81,0.1)', color: '#444' },
};

export const detailPeriodToAggregatedPeriod = {
  'Colonial Period': 'Colonial Period',
  'World War II and Japanese Occupation': 'Colonial Period',
  'Parliamentary Government': '(Post) Independence',
  'Caretaker Government': '(Post) Independence',
  'Union Goverment': '(Post) Independence',
  'Ne Win and the BSPP Regime': 'Ne Win',
  'SLORC and SPDC': 'SLORC and SPDC',
  'Transitional Period': 'Transition',
  'Coup and Post-coup': 'Post Feb 2021 Coup',
};

export const householdSizes = {
  Unknown: 5,
  '0 to 99': 5,
  '100 to 499': 10,
  '500 to 999': 15,
  '1000 or more': 20,
};

export const mapboxZoomLevels = [9, 11, 12];

export const topTimeLineDefaultWidth = 400;
export const topTimeLineHeight = 100;
export const topTimeLineBarHeight = 30;
export const timelineHeight = 4000;
export const timelineBarWidth = 20;
