import { useEffect, useState } from 'react';

const ProgressiveImage = (props) => {
  const { src, className, placeholderSrc, alt } = props;
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    setLoaded(false);
  }, [src]);

  // console.log(loaded)

  return (
    <>
      {loaded ? (
        <></>
      ) : (
        <div className='flex items-center justify-center'>
          <img src={placeholderSrc} alt={alt || ''} className={className} />
        </div>
      )}
      <img
        src={src}
        alt={alt || ''}
        className={className}
        onLoad={() => setLoaded(true)}
        style={!loaded ? { display: 'none' } : {}}
      />
    </>
  );
};
export default ProgressiveImage;
