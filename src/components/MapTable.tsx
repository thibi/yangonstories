import {
  ChevronDoubleLeftIcon,
  ChevronDoubleRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@heroicons/react/solid';
import React, { useMemo } from 'react';
import { usePagination, useTable } from 'react-table';

// import { yangonCenter } from '@/lib/constants';

export default function MapTable(props) {
  // console.log(props)
  const data = useMemo(() => props.data, [props.data]);

  const columns = useMemo(() => props.columns, [props.columns]);

  const {
    headerGroups,
    page,
    prepareRow,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      initialState: { pageIndex: 0 },
    },
    usePagination
  );

  const getCellRender = (cell, cellType) => {
    if (cellType === 'url') {
      // console.log(cell.value)
      return cell.value === 'Unknown' ? (
        ''
      ) : (
        <a href={cell.value} className='underline'>
          Link
        </a>
      );
    }
    return cell.render('Cell');
  };

  return (
    <>
      <div className='px-4 sm:px-6 lg:px-8'>
        <div className='flex flex-col mt-8'>
          <div className='-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8'>
            <div className='inline-block min-w-full py-2 align-middle md:px-6 lg:px-8'>
              <div className='overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg'>
                <table className='min-w-full divide-y divide-gray-300'>
                  <thead className=' bg-brown'>
                    {headerGroups.map((headerGroup, i) => (
                      <tr
                        key={'headerrow' + i}
                        {...headerGroup.getHeaderGroupProps()}
                      >
                        {headerGroup.headers.map((column) => (
                          <th
                            key={'headercell' + i}
                            {...column.getHeaderProps()}
                            scope='col'
                            className='px-3 py-3.5 text-left text-sm font-semibold text-white'
                          >
                            {column.render('Header')}
                          </th>
                        ))}
                      </tr>
                    ))}
                  </thead>
                  <tbody className='bg-white divide-y divide-gray-200'>
                    {page.map((row, i) => {
                      prepareRow(row);
                      return (
                        <tr key={'datarow' + i} {...row.getRowProps()}>
                          {row.cells.map((cell, j) => {
                            const { column } = cell;
                            return (
                              <td
                                key={'cell' + j}
                                {...cell.getCellProps()}
                                className='max-w-xs px-3 py-4 text-sm text-gray-500'
                              >
                                {getCellRender(cell, column.type)}
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='flex items-center justify-between px-4 py-3 md:mx-4 bg-white border-gray-200 sm:px-6'>
        <div className='flex-1 flex justify-between sm:hidden'>
          <button
            className='relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
          >
            Previous
          </button>
          <button
            className='ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50'
            onClick={() => nextPage()}
            disabled={!canNextPage}
          >
            Next
          </button>
        </div>
        <div className='hidden sm:flex-1 sm:flex sm:items-center sm:justify-between'>
          <div>
            <p className='text-sm text-gray-700'>
              Showing <span className='font-medium'>{pageSize}</span> results ,
              Page &nbsp;
              <span className='font-medium'>{pageIndex + 1}</span> of{' '}
              <span className='font-medium'>{pageOptions.length}</span>
            </p>
          </div>
          <div>
            <nav
              className='relative z-0 inline-flex -space-x-px rounded-md shadow-sm'
              aria-label='Pagination'
            >
              <button
                className='relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-l-md hover:bg-gray-50'
                onClick={() => gotoPage(0)}
                disabled={!canPreviousPage}
              >
                <span className='sr-only'>First</span>
                <ChevronDoubleLeftIcon className='w-5 h-5' aria-hidden='true' />
              </button>
              <button
                className='relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 hover:bg-gray-50'
                onClick={() => previousPage()}
                disabled={!canPreviousPage}
              >
                <span className='sr-only'>Previous</span>
                <ChevronLeftIcon className='w-5 h-5' aria-hidden='true' />
              </button>
              <span className='relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-700 bg-white border border-gray-300'>
                ...
              </span>
              <button
                onClick={() => nextPage()}
                disabled={!canNextPage}
                className='relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 hover:bg-gray-50'
              >
                <span className='sr-only'>Next</span>
                <ChevronRightIcon className='w-5 h-5' aria-hidden='true' />
              </button>
              <button
                onClick={() => gotoPage(pageCount - 1)}
                disabled={!canNextPage}
                className='relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-r-md hover:bg-gray-50'
              >
                <span className='sr-only'>Last</span>
                <ChevronDoubleRightIcon
                  className='w-5 h-5'
                  aria-hidden='true'
                />
              </button>
            </nav>
          </div>
        </div>
      </div>
    </>
  );
}
