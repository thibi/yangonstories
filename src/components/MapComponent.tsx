import React, { useCallback, useState } from 'react';
import Map, {
  FullscreenControl,
  Layer,
  NavigationControl,
  Popup,
  ScaleControl,
  Source,
} from 'react-map-gl';

import { yangonCenter } from '@/lib/constants';
import { MBTOKEN } from '@/lib/mbtoken';
import { arrayStringParser } from '@/lib/utils';

// import MapControlPanel from './MapControlPanel';
import FiltersGroupComponent from './FiltersGroupComponent';
import MapTable from './MapTable';

export default function MapComponent(props) {
  const {
    data,
    filters,
    currentFilters,
    filterHandler,
    legends,
    popUpFields,
    currentGroupCounts,
    tableData,
  } = props;
  let initialViewState = {
    latitude: yangonCenter.latitude,
    longitude: yangonCenter.longitude,
    width: '100%',
    height: '100%',
    zoom: 9,
    bearing: 0,
    pitch: 0,
    minZoom: 1,
    maxZoom: 15,
  };

  if (legends.initialViewState) {
    initialViewState = {
      ...initialViewState,
      ...legends.initialViewState,
    };
  }
  // console.log("data",data);
  // console.log("tableData",tableData);

  const [popupInfo, setPopupInfo] = useState(null);
  const [showTable, setShowTable] = useState(false);
  const [showTableRowIds, setShowTableRowIds] = useState([]);

  const getPopUpFields = (fields, obj) => {
    const returnObj = {};
    for (let i = 0; i < fields.length; i++) {
      returnObj[fields[i]] = obj[fields[i]];
    }
    returnObj.latitude = Number(obj.latitude);
    returnObj.longitude = Number(obj.longitude);
    return returnObj;
  };

  const onClick = useCallback(
    (event) => {
      // console.log(event)
      const pointLayer = event.features && event.features[0];
      if (
        pointLayer &&
        pointLayer.properties &&
        pointLayer.layer.id === 'points'
      ) {
        const id = pointLayer.properties.id;
        setShowTable(true);
        setShowTableRowIds(tableData.filter((d) => d.id === id));
      } else {
        setShowTable(false);
        setShowTableRowIds([]);
      }
    },
    [tableData]
  );

  const onHover = useCallback(
    (event) => {
      const pointLayer = event.features && event.features[0];
      if (pointLayer && pointLayer.layer.id === 'points') {
        setPopupInfo({
          ...getPopUpFields(Object.keys(popUpFields), pointLayer.properties),
        });
      }
    },
    [popUpFields]
  );

  const pointLayer = {
    id: 'points',
    type: 'circle',
    paint: legends.mapboxStyles,
  };

  const getLegendElement = (cat, value) => {
    if (cat === 'Households') {
      return (
        <div
          style={{
            display: 'inline-block',
            width: '40px',
            height: '40px',
            textAlign: 'center',
          }}
        >
          <span
            style={{
              height: legends.otherStyles['circle-radius'].getValue(value),
              width: legends.otherStyles['circle-radius'].getValue(value),
              backgroundColor: '#fff',
              borderRadius: '50%',
              border: '1px solid #222',
              display: 'inline-block',
              marginLeft: '5px',
              position: 'relative',
            }}
          ></span>
        </div>
      );
    } else if (cat === 'Period') {
      return (
        <div
          style={{
            display: 'inline-block',
            width: '20px',
            height: '20px',
            textAlign: 'center',
          }}
        >
          <span
            style={{
              height: '20px',
              width: '20px',
              backgroundColor:
                legends.otherStyles['circle-color'].getValue(value),
              border: '1px solid #222',
              display: 'inline-block',
              marginLeft: '5px',
              position: 'relative',
            }}
          ></span>
        </div>
      );
    } else return <span></span>;
  };

  return (
    <div className='grid w-full grid-cols-5 h-content'>
      {filters && (
        <div className='w-fixed max-h-[800px] px-4 overflow-y-auto'>
          <div className='control-panel'>
            <h3>Filters</h3>
            <hr />
            {Object.keys(filters).map((filterKey) => {
              const filter = filters[filterKey];
              return (
                <div key={filterKey} className='m-5 input'>
                  <h4 className='text-xl'>{filterKey}</h4>
                  {/* {filter.map((filterValue) => { */}

                  {/* return ( */}
                  <FiltersGroupComponent
                    filterKey={filterKey}
                    filter={filter}
                    currentFilter={currentFilters[filterKey]}
                    currentGroupCounts={currentGroupCounts[filterKey].all()}
                    filterHandler={filterHandler}
                    getLegendElement={getLegendElement}
                  />
                  {/* ); */}
                  {/* })} */}
                </div>
              );
            })}
          </div>
        </div>
      )}
      <div role='main' className='flex-grow col-span-4 px-3 pt-1'>
        <>
          <Map
            initialViewState={initialViewState}
            mapStyle='mapbox://styles/mapbox/dark-v9'
            mapboxAccessToken={MBTOKEN}
            onMouseMove={onHover}
            onMouseDown={onClick}
            interactiveLayerIds={['points', 'background', 'water', 'building']}
          >
            {/* <GeolocateControl position="top-left" /> */}
            <FullscreenControl position='top-left' />
            <NavigationControl position='top-left' />
            <ScaleControl />

            {data && data.features && (
              <Source type='geojson' data={data}>
                <Layer {...pointLayer} />
              </Source>
            )}

            {popupInfo && (
              <Popup
                anchor='top'
                longitude={Number(popupInfo.longitude)}
                latitude={Number(popupInfo.latitude)}
                // location={popupInfo.Location}
                // affected={popupInfo.HouseholdsNum}
                // year={popupInfo.Year}
                // rationale={popupInfo.Rationale}
                {...getPopUpFields(Object.keys(popUpFields), popupInfo)}
                closeOnMove={true}
                closeOnClick={true}
                onClose={() => setPopupInfo(null)}
              >
                <div>
                  {Object.values(popUpFields).map((field) => {
                    return (
                      <div key={field.key}>
                        <b>{field.label}:</b>{' '}
                        {arrayStringParser(popupInfo[field.key])} <br />
                      </div>
                    );
                  })}
                </div>
              </Popup>
            )}
          </Map>

          <div className='control-panel'>Hello</div>
        </>
        {showTable && (
          <MapTable
            columns={legends.otherStyles['table-columns']}
            data={showTableRowIds}
          />
        )}
      </div>
    </div>
  );
}
