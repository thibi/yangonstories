import { ChevronRightIcon } from '@heroicons/react/solid';
import Link from 'next/link';
import useTranslation from 'next-translate/useTranslation';

const StoriesMenu = (props) => {
  const { currentSelection, optionsProp, titleProp, optionsSet } = props;

  const { t } = useTranslation('common');

  // console.log(optionsSet);

  const options = optionsProp
    ? optionsProp
    : optionsSet === 'stories'
    ? [
        {
          id: 'featured_stories',
          title: t('featured_stories'),
          href: '/stories/feature_stories',
        },
        {
          id: 'cartoons',
          title: t('cartoons'),
          href: '/stories/cartoons',
        },
        {
          id: 'interviews',
          title: t('interviews'),
          href: '/stories/interviews',
        },
        {
          id: 'videos',
          title: t('video_clips'),
          href: '/stories/videos',
        },
        {
          id: 'poems',
          title: t('poems'),
          href: '/stories/poems',
        },
        {
          id: 'booklet',
          title: t('booklet'),
          href: '/stories/booklet',
        },
      ]
    : optionsSet === 'postcoup'
    ? [
        {
          id: 'events',
          title: t('events'),
          href: '/postcoup/events',
        },
        {
          id: 'timeline',
          title: t('timeline'),
          href: '/postcoup/timeline',
        },
        {
          id: 'map',
          title: t('map'),
          href: '/map',
          query: { id: 'postcoup' },
        },
        {
          id: 'interviews',
          title: t('interviews'),
          href: '/stories/interviews',
          query: { id: 'postcoup' },
        },
        {
          id: 'videos',
          title: t('video_clips'),
          href: '/stories/videos',
          query: { id: 'postcoup' },
        },
        {
          id: 'essay',
          title: t('essay'),
          href: '/postcoup/essay',
        },
      ]
    : [];

  const title = titleProp
    ? titleProp
    : optionsSet === 'stories'
    ? t('stories')
    : optionsSet === 'postcoup'
    ? t('postcoup_violence')
    : '';

  return (
    <div className='items-center col-span-1 p-2 '>
      <h2 className='text-xl uppercase'>{title}</h2>
      <ul className='ml-4 text-xl'>
        {options.map((option, i) => {
          return (
            <li className='mt-6 text-xl font-bold' key={'menu' + i}>
              {option.id === currentSelection ? (
                <ChevronRightIcon className='inline-block w-6 h-6 text-black' />
              ) : (
                <></>
              )}
              <Link
                href={{
                  pathname: option.href,
                  query: option.query ? option.query : '',
                }}
              >
                {option.title}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default StoriesMenu;
