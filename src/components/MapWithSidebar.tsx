import { Dialog, Transition } from '@headlessui/react';
import { XIcon } from '@heroicons/react/outline';
import React, { Fragment, useCallback, useState } from 'react';
import Map, {
  FullscreenControl,
  Layer,
  NavigationControl,
  Popup,
  ScaleControl,
  Source,
} from 'react-map-gl';

import { yangonCenter } from '@/lib/constants';
// import mapboxgl from 'mapbox-gl';
import { MBTOKEN } from '@/lib/mbtoken';
import { arrayStringParser } from '@/lib/utils';

// import MapControlPanel from './MapControlPanel';
import FiltersGroupComponent from './FiltersGroupComponent';
import MapTableOnly from './MapTableOnly';

export default function MapComponent(props) {
  const {
    data,
    filters,
    currentFilters,
    filterHandler,
    legends,
    popUpFields,
    currentGroupCounts,
    tableData,
    sidebarOpen,
    setSidebarOpen,
  } = props;
  let initialViewState = {
    latitude: yangonCenter.latitude,
    longitude: yangonCenter.longitude,
    width: '100%',
    height: '100%',
    zoom: 10,
    bearing: 0,
    pitch: 0,
    minZoom: 1,
    maxZoom: 13,
  };

  if (legends.initialViewState) {
    initialViewState = {
      ...initialViewState,
      ...legends.initialViewState,
    };
  }
  // console.log("data",data);
  // console.log("tableData",tableData);

  const [popupInfo, setPopupInfo] = useState(null);
  const [showTable, setShowTable] = useState(false);
  const [showTableRowIds, setShowTableRowIds] = useState([]);
  // const [open, setOpen] = useState(false);

  const getPopUpFields = (fields, obj) => {
    const returnObj = {};
    for (let i = 0; i < fields.length; i++) {
      returnObj[fields[i]] = obj[fields[i]];
    }
    returnObj.latitude = Number(obj.latitude);
    returnObj.longitude = Number(obj.longitude);
    return returnObj;
  };

  const onClick = useCallback(
    (event) => {
      const pointLayer = event.features && event.features[0];
      // console.log(pointLayer)
      if (
        pointLayer &&
        pointLayer.properties &&
        pointLayer.layer.id === 'points'
      ) {
        const id = pointLayer.properties.id;
        setShowTable(true);
        setShowTableRowIds(tableData.filter((d) => d.id === id));
      } else {
        setShowTable(false);
        setShowTableRowIds([]);
      }
    },
    [tableData]
  );

  const onHover = useCallback(
    (event) => {
      const pointLayer = event.features && event.features[0];
      // console.log(pointLayer)
      if (pointLayer && pointLayer.layer.id === 'points') {
        setPopupInfo({
          ...getPopUpFields(Object.keys(popUpFields), pointLayer.properties),
        });
      }
    },
    [popUpFields]
  );

  const pointLayer = {
    id: 'points',
    type: 'circle',
    paint: legends.mapboxStyles,
  };

  const getLegendElement = (cat, value) => {
    if (cat === 'Households') {
      return (
        <div
          style={{
            display: 'inline-block',
            width: '40px',
            height: '40px',
            textAlign: 'center',
          }}
        >
          <span
            style={{
              height: legends.otherStyles['circle-radius'].getValue(value),
              width: legends.otherStyles['circle-radius'].getValue(value),
              backgroundColor: '#fff',
              borderRadius: '50%',
              border: '1px solid #222',
              display: 'inline-block',
              marginLeft: '5px',
              position: 'relative',
            }}
          ></span>
        </div>
      );
    } else if (cat === 'Period') {
      return (
        <div
          style={{
            display: 'inline-block',
            width: '20px',
            height: '20px',
            textAlign: 'center',
          }}
        >
          <span
            style={{
              height: '20px',
              width: '20px',
              backgroundColor:
                legends.otherStyles['circle-color'].getValue(value),
              border: '1px solid #222',
              display: 'inline-block',
              marginLeft: '5px',
              position: 'relative',
            }}
          ></span>
        </div>
      );
    } else if (cat === 'Category') {
      return (
        <div
          style={{
            display: 'inline-block',
            width: '20px',
            height: '20px',
            textAlign: 'center',
          }}
        >
          <span
            style={{
              height: '20px',
              width: '20px',
              backgroundColor:
                legends.otherStyles['circle-color'].getValue(value),
              border: '1px solid #222',
              display: 'inline-block',
              marginLeft: '5px',
              position: 'relative',
            }}
          ></span>
        </div>
      );
    } else return <span></span>;
  };

  return (
    <>
      <Transition.Root show={sidebarOpen} as={Fragment}>
        <Dialog
          as='div'
          className='relative z-40'
          onClose={() => setSidebarOpen(false)}
        >
          <Transition.Child
            as={Fragment}
            enter='transition-opacity ease-linear duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='transition-opacity ease-linear duration-300'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='fixed inset-0' />
          </Transition.Child>

          <div className='fixed inset-0 z-40 flex'>
            <Transition.Child
              as={Fragment}
              enter='transition ease-in-out duration-300 transform'
              enterFrom='-translate-x-full'
              enterTo='translate-x-0'
              leave='transition ease-in-out duration-300 transform'
              leaveFrom='translate-x-0'
              leaveTo='-translate-x-full'
            >
              <Dialog.Panel className='relative mt-[96px] flex flex-col flex-1 w-full max-w-xs pt-5 pb-4 bg-white'>
                <Transition.Child
                  as={Fragment}
                  enter='ease-in-out duration-300'
                  enterFrom='opacity-0'
                  enterTo='opacity-100'
                  leave='ease-in-out duration-300'
                  leaveFrom='opacity-100'
                  leaveTo='opacity-0'
                >
                  <div className='absolute top-0 pt-2 -mr-12 right-[60px]'>
                    <button
                      type='button'
                      className='flex items-center justify-center w-10 h-10 ml-1 rounded-full outline-none ring-2 ring-inset ring-brown'
                      onClick={() => setSidebarOpen(false)}
                    >
                      <span className='sr-only'>Close sidebar</span>
                      <XIcon
                        className='w-6 h-6 text-brown'
                        aria-hidden='true'
                      />
                    </button>
                  </div>
                </Transition.Child>
                <div className='flex items-center flex-shrink-0 px-4'>
                  {/* <button onClick={() => setSidebarOpen(true)}>open</button> */}
                </div>
                <div className='flex-1 h-0 mt-5 overflow-y-auto'>
                  <nav className='px-2 space-y-1'>
                    {filters && (
                      <div className='px-4 overflow-y-auto w-fixed max-h-84vh'>
                        <div className='control-panel'>
                          <h3>Filters</h3>
                          <hr />
                          {Object.keys(filters).map((filterKey) => {
                            const filter = filters[filterKey];
                            return (
                              <div key={filterKey} className='m-5 input'>
                                <h4 className='text-xl'>{filterKey}</h4>
                                {/* {filter.map((filterValue) => { */}

                                {/* return ( */}
                                <FiltersGroupComponent
                                  filterKey={filterKey}
                                  filter={filter}
                                  currentFilter={currentFilters[filterKey]}
                                  currentGroupCounts={currentGroupCounts[
                                    filterKey
                                  ].all()}
                                  filterHandler={filterHandler}
                                  getLegendElement={getLegendElement}
                                />
                                {/* ); */}
                                {/* })} */}
                              </div>
                            );
                          })}
                        </div>
                      </div>
                    )}
                  </nav>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </Dialog>
      </Transition.Root>

      <div
        className='flex flex-col max-w-full'
        style={{ width: '100%', height: '100vh' }}
      >
        <main role='main' className='flex-1'>
          <Map
            initialViewState={initialViewState}
            mapStyle='mapbox://styles/mapbox/dark-v9'
            mapboxAccessToken={MBTOKEN}
            onMouseMove={onHover}
            onMouseDown={onClick}
            interactiveLayerIds={['points', 'background', 'water', 'building']}
          >
            {/* <GeolocateControl position="top-left" /> */}
            <FullscreenControl position='top-left' />
            <NavigationControl position='top-left' />
            <ScaleControl />

            {data && data.features && (
              <Source type='geojson' data={data}>
                <Layer {...pointLayer} />
              </Source>
            )}

            {popupInfo && (
              <Popup
                anchor='top'
                longitude={Number(popupInfo.longitude)}
                latitude={Number(popupInfo.latitude)}
                // location={popupInfo.Location}
                // affected={popupInfo.HouseholdsNum}
                // year={popupInfo.Year}
                // rationale={popupInfo.Rationale}
                {...getPopUpFields(Object.keys(popUpFields), popupInfo)}
                closeOnMove={true}
                closeOnClick={true}
                onClose={() => setPopupInfo(null)}
              >
                <div>
                  {Object.values(popUpFields).map((field) => {
                    return (
                      <div key={field.key}>
                        <b>{field.label}:</b>{' '}
                        {arrayStringParser(popupInfo[field.key])} <br />
                      </div>
                    );
                  })}
                </div>
              </Popup>
            )}
          </Map>

          {showTable && (
            <div className='fixed z-40 flex justify-center w-screen md:px-10 bottom-0'>
              <div className='pb-2 overflow-x-hidden  max-h-[40vh] flex flex-col'>
                <div className='flex justify-end'>
                  <button
                    type='button'
                    className='flex items-center justify-center w-10 h-10 m-2 ml-1 rounded-full outline-none ring-2 ring-inset ring-white'
                    onClick={() => setShowTable(false)}
                  >
                    <span className='sr-only'>Close sidebar</span>
                    <XIcon className='w-6 h-6 text-white' aria-hidden='false' />
                  </button>
                </div>
                <MapTableOnly
                  columns={legends.otherStyles['table-columns']}
                  data={showTableRowIds}
                />
              </div>
            </div>
          )}
        </main>
      </div>

      {/* <Transition.Root show={showTable} as={Fragment}>
        <Dialog
          as='div'
          className='relative z-10'
          onClose={() => setShowTable(false)}
        >
          <Transition.Child
            as={Fragment}
            enter='ease-out duration-300'
            enterFrom='opacity-0'
            enterTo='opacity-100'
            leave='ease-in duration-200'
            leaveFrom='opacity-100'
            leaveTo='opacity-0'
          >
            <div className='fixed inset-0' />
          </Transition.Child>
          <div className='fixed inset-0 z-10 overflow-y-auto'>
            <div className='flex items-end justify-center min-h-full p-4 text-center sm:items-center sm:p-0'>
              <Transition.Child
                as={Fragment}
                enter='ease-out duration-300'
                enterFrom='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
                enterTo='opacity-100 translate-y-0 sm:scale-100'
                leave='ease-in duration-200'
                leaveFrom='opacity-100 translate-y-0 sm:scale-100'
                leaveTo='opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95'
              >
                <Dialog.Panel
                  className='absolute inset-x-0 bottom-0 w-5/6 overflow-hidden text-left transition-all transform bg-white rounded-lg shadow-xl'
                  style={{ width: '100%' }}
                >
                  <MapTableOnly
                    columns={legends.otherStyles['table-columns']}
                    data={showTableRowIds}
                  />
                  <div className='flex flex-row-reverse px-4 py-4 sm:px-6 lg:px-8'>
                    <button
                      type='button'
                      className='inline-flex items-center justify-center px-4 py-2 text-sm font-medium bg-gray-100 border border-transparent rounded-md shadow-sm hover:bg-gray-300 focus:outline-none focus:ring-2 focus:bg-gray-200 focus:ring-offset-2 sm:w-auto'
                      onClick={() => setShowTable(false)}
                    >
                      Hide
                    </button>
                  </div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition.Root> */}
    </>
  );
}
