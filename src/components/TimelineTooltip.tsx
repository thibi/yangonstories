import React from 'react';

import { timelineBarWidth } from '@/lib/constants';
import { isURL } from '@/lib/utils';

export default function TimelineTooltip(props) {
  const { highlight, fixToolTip, setFixToolTip } = props;
  const { eventId, event, clientRect } = highlight
    ? highlight
    : { eventId: null, event: null, node: null };

  // console.log(highlight);
  const x = timelineBarWidth;
  const y = clientRect && clientRect.y + timelineBarWidth * 1.5;

  const yearDisplay =
    event && event.yearRange
      ? event.yearRange[0] === event.yearRange[1]
        ? event.yearRange[0]
        : event.yearRange[0] + ' - ' + event.yearRange[1]
      : '';

  // if (event && event.remarks) {
  // console.log(event.remarks)
  // console.log(isURL(event.remarks));
  // }

  return eventId ? (
    <div>
      <div
        className='fixed block p-2 text-xs text-left bg-white border-2 border-black rounded-lg pointer-events-none w-60'
        style={{ top: y, left: x, zIndex: 50 }}
      >
        <p className='font-bold'>{event.title}</p>
        <p>{yearDisplay ? '(' + yearDisplay + ')' : ''}</p>
        {event.description ? (
          <p>
            <span className='font-bold'> Description:</span> {event.description}
          </p>
        ) : (
          <></>
        )}
        {event.remarks ? (
          <p className='pointer-events-auto'>
            <span className='font-bold'> Remarks: </span>
            {isURL(event.remarks) ? (
              <a
                className='underline'
                href={event.remarks}
                rel='noreferrer'
                target={'_blank'}
              >
                External Link
              </a>
            ) : (
              event.remarks
            )}
          </p>
        ) : (
          <></>
        )}
        <div
          className='p-1 m-2 font-bold text-center text-white bg-[#444] rounded-lg hover:text-[#444] hover:bg-white hover:border-[#444] border-2 pointer-events-auto cursor-pointer'
          onClick={() => setFixToolTip(false)}
        >
          {fixToolTip
            ? 'Click here to close this box'
            : 'Click to leave this box open'}
        </div>
      </div>
    </div>
  ) : (
    <></>
  );
}
