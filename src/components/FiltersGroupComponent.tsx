import React from 'react';

// import { yangonCenter } from '@/lib/constants';

export default function FiltersGroupComponent(props) {
  const {
    filterKey,
    filter,
    currentFilter,
    currentGroupCounts,
    filterHandler,
    getLegendElement,
  } = props;
  // console.log(currentGroupCounts);

  return (
    <>
      {filter.map((filterValue) => {
        const currentGroup = currentGroupCounts
          ? currentGroupCounts.filter((d) => d.key === filterValue)[0]
          : null;

        if (!currentGroup || currentGroup.value === 0) return <></>;

        return (
          <div
            key={filterKey + '_' + filterValue}
            className={'filter-item flex'}
          >
            <div className='fl'>
              <input
                type='checkbox'
                defaultChecked={currentFilter.includes(filterValue)}
                onChange={() => filterHandler(filterKey, filterValue)}
              />
            </div>
            {getLegendElement ? (
              <div>{getLegendElement(filterKey, filterValue)}</div>
            ) : (
              <></>
            )}
            <div className='pl-2'>
              <label>
                {filterValue}{' '}
                {currentGroup ? <b>({currentGroup.value})</b> : <></>}
              </label>
            </div>
          </div>
        );
      })}
    </>
  );
}
