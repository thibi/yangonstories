import ImageGallery from '@/components/ImageGallery';
import React from 'react';
import { useRouter } from 'next/router';
import { getImagePath } from '@/lib/utils';
import DWChart from 'react-datawrapper-chart';
import ReactMarkdown from 'react-markdown';

export default function ParagraphParser(props) {
  const { data } = props;
  const router = useRouter();

  return data ? (
    <div>
      {Object.keys(data)
        .sort()
        .map((step, i) => {
          const images =
            data[step].image &&
            data[step].image.length > 0 &&
            data[step].image.map((row) => {
              const imageId =
                row['ImageIds'] && row['ImageIds'].length > 0
                  ? row['ImageIds'][0]
                  : '';

              return {
                caption:
                  router.locale == 'mm' && row['caption_mm']
                    ? row['caption_mm']
                    : row['caption_en'],
                image: getImagePath(imageId, 'lg'),
                thumbnail: getImagePath(imageId, 'sm'),
                reference: row['reference'],
              };
            });

          return (
            <div className='grid grid-cols-5' key={'scrolly' + i}>
              {data[step].title && (
                <div className='col-span-5'>
                  <h2 className='text-xl font-bold'>
                    {router.locale == 'mm' && data[step].title[0].caption_mm
                      ? data[step].title[0].caption_mm
                      : data[step].title[0].caption_en}
                  </h2>
                </div>
              )}
              {data[step].embed && data[step].embed.length > 0 ? (
                <div className='col-span-5 my-20 text-center'>
                  <h2 className='mb-8 font-bold'>
                    {router.locale == 'mm'
                      ? data[step].embed[0].caption_mm
                      : data[step].embed[0].caption_en}
                  </h2>
                  <DWChart
                    title='Map'
                    src={data[step].embed[0].embed_chart_link}
                  />
                </div>
              ) : (
                <></>
              )}
              <div
                className={`col-span-5 bg-white ${
                  data[step].image ? 'md:col-span-3' : ''
                }`}
              >
                {data[step].paragraph &&
                  data[step].paragraph.map((paragraph, j) => {
                    const multilinePara =
                      router.locale == 'mm' && paragraph.caption_mm
                        ? paragraph.caption_mm
                        : paragraph.caption_en;
                    return (
                      <p
                        className='my-10 airtable-contents'
                        key={'caption' + j}
                      >
                        <ReactMarkdown>{multilinePara}</ReactMarkdown>
                      </p>
                    );
                  })}
              </div>
              <div
                className={
                  'col-span-5 px-5 ' +
                  (!data[step].paragraph
                    ? 'md:col-span-5 my-10'
                    : 'md:col-span-2')
                }
              >
                {data[step].image && data[step].image.length > 0 ? (
                  <ImageGallery images={images} captionPosition={'bottom'} />
                ) : (
                  <></>
                )}
              </div>
            </div>
          );
        })}
    </div>
  ) : (
    <></>
  );
}
