// import { yangonCenter } from '@/lib/constants';
import {
  ArrowCircleLeftIcon,
  ArrowCircleRightIcon,
} from '@heroicons/react/solid';
import React, { useState } from 'react';

import { isURL } from '@/lib/utils';

import ProgressiveImage from './ProgressiveImage';
import placeholderSrc from '../../public/images/YangonStories_Icon_Black.png';

export default function ImageGallery(props) {
  const { images, captionPosition, darkBg } = props;

  const [showReference, setShowReference] = useState(false);
  const [nowShowing, setNowShowing] = useState(0);

  const getCaption = (position) => (
    <p
      className={
        'col-span-1 ' + (position === 'bottom' ? ' text-xs text-center' : '')
      }
    >
      {images[nowShowing].caption}
      <br />
      {images[nowShowing].reference &&
        (isURL(images[nowShowing].reference) ? (
          <a
            className='underline'
            href={images[nowShowing].reference}
            rel='noreferrer'
            target={'_blank'}
          >
            External Link
          </a>
        ) : (
          <div onClick={() => setShowReference(!showReference)}>
            <span className='font-bold text-green-600 cursor-pointer hover:text-green-300'>
              {showReference ? 'Hide Reference' : 'Show Reference'}
            </span>
            {showReference ? (
              <div className='pt-2 italic'>{images[nowShowing].reference}</div>
            ) : (
              ''
            )}
          </div>
        ))}
    </p>
  );

  return (
    <div>
      <div className='main-image'>
        {
          <div
            className={
              'flex items-center ' +
              (captionPosition === 'side' ? 'col-span-1' : 'col-span-2')
            }
          >
            {images.length > 1 && (
              <ArrowCircleLeftIcon
                className={`h-10 w-10 ${
                  nowShowing > 0
                    ? darkBg
                      ? 'cursor-pointer text-gray-300'
                      : 'cursor-pointer text-black'
                    : darkBg
                    ? 'text-black cursor-not-allowed'
                    : 'text-gray-300 cursor-not-allowed'
                }`}
                onClick={() =>
                  nowShowing > 0
                    ? setNowShowing(nowShowing - 1)
                    : setNowShowing(nowShowing)
                }
              />
            )}
            <div className='w-full'>
              {/* <img
                src={images[nowShowing].image}
                /> */}
              <ProgressiveImage
                className='mx-auto mt-10 mb-5 border-4 border-black rounded-3xl'
                alt={images[nowShowing].caption}
                src={images[nowShowing].image}
                placeholderSrc={placeholderSrc.src}
              />
            </div>
            {images.length > 1 && (
              <ArrowCircleRightIcon
                className={`h-10 w-10 ${
                  nowShowing < images.length - 1
                    ? darkBg
                      ? 'cursor-pointer text-gray-300'
                      : 'cursor-pointer text-black'
                    : darkBg
                    ? 'text-black cursor-not-allowed'
                    : 'text-gray-300 cursor-not-allowed'
                }`}
                onClick={() =>
                  nowShowing < images.length - 1
                    ? setNowShowing(nowShowing + 1)
                    : setNowShowing(nowShowing)
                }
              />
            )}
          </div>
        }
        {captionPosition === 'side' ? getCaption('side') : <></>}
      </div>

      {captionPosition === 'bottom' ? getCaption('bottom') : <></>}
      {images.length > 1 && (
        <div className='flex flex-row justify-center thumbnail-gallery'>
          {images.map((imageObj, i) => {
            return (
              <div key={'imageThumbnail' + i}>
                <div
                  className={
                    'p-1 m-4 cursor-pointer imageThumbnailContainer hover:border-2 ' +
                    (nowShowing === i ? 'border-2' : '')
                  }
                >
                  <img
                    src={imageObj.thumbnail}
                    onClick={() => setNowShowing(i)}
                    alt={imageObj.caption}
                  />
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
}
