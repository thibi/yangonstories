import setLanguage from 'next-translate/setLanguage';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

const LocaleSwitcher = (props) => {
  const router = useRouter();

  const setLngHandler = async (value) => {
    if (value == 'en') {
      props.setEngEnb(true);
    } else {
      props.setEngEnb(false);
    }
    await setLanguage(value);
  };

  return (
    <>
      <button
        className={router.locale === 'en' ? 'text-red-300' : 'text-white'}
        // onClick={async () => await setLanguage('en')}
        onClick={() => setLngHandler('en')}
      >
        English
        {/* <span className={router.locale === 'en' ? 'locale active' : 'locale'}>
          EN
        </span> */}
      </button>
      <span className='text-white'> &nbsp;|&nbsp; </span>
      <button
        className={router.locale === 'mm' ? 'text-red-300' : 'text-white'}
        // onClick={async () => await setLanguage('mm')}
        onClick={() => setLngHandler('mm')}
      >
        မြန်မာ
      </button>
    </>
  );
};

export default LocaleSwitcher;
