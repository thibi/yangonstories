const ParseMultilineText = (props) => {
  const { text, boldSpeaker, speakerSpliter } = props;
  const newText = text.split('\n').map((str, i) => {
    if (!boldSpeaker)
      return (
        <p className='m-4' key={`script-${i}`}>
          {str}
        </p>
      );

    const [character, ...text] = str.split(speakerSpliter);
    if (!character) return '';
    if (!text.length)
      return (
        <p className='m-4' key={`script-${i}`}>
          {str}
        </p>
      );
    return (
      <p className='m-4' key={`script-${i}`}>
        <span className='font-bold text-brown'>
          {character + speakerSpliter}
        </span>
        {text.join(speakerSpliter)}
      </p>
    );
  });
  return newText;
};

export default ParseMultilineText;
