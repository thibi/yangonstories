// import axios from 'axios';
import { nest } from 'd3-collection';
import Link from 'next/dist/client/link';
import { useRouter } from 'next/router';
// import { csvParse } from 'd3-dsv';
import React, { useEffect, useRef, useState } from 'react';
import Map, { Layer, Source } from 'react-map-gl';
import { Scrollama, Step } from 'react-scrollama';

import {
  householdSizes,
  mapboxZoomLevels,
  periodStyles,
  yangonCenter,
} from '@/lib/constants';
import { parseMapEvictionsData } from '@/lib/dataparsers';
import * as ga from '@/lib/gtag';
import { MBTOKEN } from '@/lib/mbtoken';
import {
  buildFilteredDataSet,
  filterHandler,
  flattenArray,
  getImagePath,
  makeCrossfilterDimsAndGroups,
  makeEmptyFilters,
} from '@/lib/utils';
import { withBase } from '@/lib/utils';
import useTranslation from 'next-translate/useTranslation';

import ImageGallery from '@/components/ImageGallery';

import evictionsScrollyStepsData from '../../datasets/Evictions_Story.json';

const EvictionsStoryScrolly = () => {
  const { t } = useTranslation('home');
  const router = useRouter();
  // const [currentStepIndex, setCurrentStepIndex] = useState(null);
  // const [stepProgress, setStepProgress] = useState(0);
  // Evictions layer variables
  const [evictionsData, setEvictionsData] = useState();
  const [filtersForEvictionsData, setFiltersForEvictionsData] = useState();
  const [filtersForCurrentEvictionsData, setFiltersForCurrentEvictionsData] =
    useState();
  // const [tableDataForEvictions, setTableDataForEvictions] = useState();
  const menuRef = useRef(null);
  const [showBackground, setShowBackground] = useState(true);
  // const firstStepRef = useRef(null);

  const [xfEvictions, setXfEvictions] = useState();
  const [xfDimsEvictions, setXfDimsEvictions] = useState();
  const [xfGroupsEvictions, setXfGroupsEvictions] = useState();
  const [data, setData] = useState();
  const [storyData, setStoryData] = useState();

  useEffect(() => {
    const tempStoryData = nest()
      .key((d) => d['step'])
      .key((d) => d['type'])
      .object(evictionsScrollyStepsData);
    // console.log("Creating story steps")

    setStoryData(tempStoryData);
  }, []);

  //console.log("Story Data", storyData);

  const filterFunc = filterHandler(
    evictionsData,
    filtersForCurrentEvictionsData,
    setFiltersForCurrentEvictionsData,
    xfEvictions,
    xfDimsEvictions,
    xfGroupsEvictions,
    true
  );

  useEffect(() => {
    const tempData =
      evictionsData &&
      filtersForEvictionsData &&
      xfGroupsEvictions &&
      filtersForCurrentEvictionsData &&
      buildFilteredDataSet(
        evictionsData,
        xfDimsEvictions,
        filtersForCurrentEvictionsData,
        false,
        true
      );
    // console.log(tempData)
    setData(tempData);
  }, [
    evictionsData,
    filtersForCurrentEvictionsData,
    filtersForEvictionsData,
    xfDimsEvictions,
    xfGroupsEvictions,
  ]);

  // Evictions data loading
  useEffect(() => {
    const parsedMapEvictionsData = parseMapEvictionsData();
    setEvictionsData(parsedMapEvictionsData.data);
    setFiltersForEvictionsData(parsedMapEvictionsData.filters);
    setFiltersForCurrentEvictionsData(
      makeEmptyFilters(parsedMapEvictionsData.filters)
    );
    // setTableDataForEvictions(parsedMapEvictionsData.tableData);

    setXfEvictions(parsedMapEvictionsData.xfilter);
    const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(
      parsedMapEvictionsData.filterCats,
      parsedMapEvictionsData.xfilter
    );
    setXfDimsEvictions(xfDims);
    setXfGroupsEvictions(xfGroups);
  }, []);

  const initialViewState = {
    latitude: yangonCenter.latitude,
    longitude: yangonCenter.longitude - 0.15,
    width: '100%',
    height: '100%',
    zoom: 10.5,
    bearing: 0,
    pitch: 0,
    minZoom: 1,
    maxZoom: 15,
  };

  const legends = {
    // 'circle-color': '#007cbf',
    mapboxStyles: {
      'circle-color': {
        property: 'Period',
        type: 'categorical',
        stops: [
          ...Object.keys(periodStyles).map((key) => [
            key,
            periodStyles[key].background,
          ]),
        ],
      },
      'circle-radius': {
        property: 'Households',
        type: 'categorical',
        stops: flattenArray(
          mapboxZoomLevels.map((zLevel) =>
            Object.keys(householdSizes).map((key) => [
              {
                zoom: zLevel,
                value: key,
              },
              householdSizes[key] *
                (zLevel > 10 ? (zLevel > 11 ? 1 : 0.5) : 0.2),
            ])
          )
        ),
      },
      'circle-stroke-color': '#aaa',
      'circle-stroke-width': 1,
    },
    otherStyles: {
      'circle-color': {
        getValue: (d) => {
          return periodStyles[d].background;
        },
      },
      'circle-radius': {
        getValue: (d) => {
          return householdSizes[d] * 2;
        },
      },
      'table-columns': [
        { accessor: 'tableId', Header: 'ID' },
        { accessor: 'Year', Header: 'Year' },
        { accessor: 'Period', Header: 'Period' },
        { accessor: 'Township', Header: 'Township' },
        { accessor: 'Location', Header: 'Location' },
        { accessor: 'HouseholdsFull', Header: 'Households' },
        { accessor: 'Rationale', Header: 'Rationale' },
      ],
    },
  };

  const pointLayer = {
    id: 'points',
    type: 'circle',
    paint: legends.mapboxStyles,
  };

  const onStepEnter = (scrollamaResponse) => {
    if (scrollamaResponse.data) {
      if (scrollamaResponse.data === 'intro') {
        setShowBackground(true);
      } else {
        setShowBackground(false);
        if (scrollamaResponse.data.paragraph) {
          const filterValues = scrollamaResponse.data.paragraph[0].dataFilter;
          filterFunc('Period', filterValues);
        }
      }
    }
  };

  const onStepExit = (scrollamaResponse) => {
    if (scrollamaResponse.data) {
      // console.log("exit",scrollamaResponse.data);
      // const filterValues = scrollamaResponse.data.paragraph[0].dataFilter;
      // filterFunc('Period', filterValues);
    }
  };

  // useEffect(() => {
  //   window.addEventListener("resize", getContainerSize);
  // }, []);

  // const getContainerSize = () => {
  // 	const newWidth = chartRef.current?.clientWidth;
  // 	setChartWidth(newWidth);
  // }

  const menuData = [
    {
      title: 'about',
      description: t('menu_desc_about'),
      style: { background: '#f06161', color: '#fffff5' },
    },
    {
      title: 'timeline',
      description: t('menu_desc_timeline'),
      style: { background: '#8cbfb2', color: '#fffff5' },
    },
    {
      title: 'map',
      description: t('menu_desc_map'),
      style: { background: '#f2b561', color: '#fffff5' },
    },
    {
      title: 'resources',
      description: t('menu_desc_resources'),
      style: { background: '#f2b561', color: '#fffff5' },
    },
    {
      title: 'stories',
      description: t('menu_desc_stories'),
      style: { background: '#702e2e', color: '#fffff5' },
    },
    {
      title: 'contact',
      description: t('menu_desc_contact'),
      style: { background: '#f06161', color: '#fffff5' },
    },
  ];

  return (
    <div className='graphicContainer'>
      <div className='w-full h-screen stickyBlock'>
        <Map
          initialViewState={initialViewState}
          mapStyle='mapbox://styles/mapbox/dark-v9'
          mapboxAccessToken={MBTOKEN}
          scrollZoom={false}
          dragPan={false}
          doubleClickZoom={false}
          // onMouseMove={onHover}
          // onMouseDown={onClick}
          interactiveLayerIds={['points', 'background', 'water', 'building']}
        >
          {data && data.features && (
            <Source type='geojson' data={data}>
              <Layer {...pointLayer} />
            </Source>
          )}
        </Map>

        {showBackground ? (
          <div
            className='fixed w-screen h-screen'
            style={{
              backgroundImage: `url(/images/index-page-bg.jpg)`,
              backgroundSize: 'cover',
              backgroundPosition: 'center',
            }}
          ></div>
        ) : (
          <></>
        )}
      </div>
      <div className='container steps'>
        <Scrollama
          offset={0.5}
          onStepEnter={onStepEnter}
          onStepExit={onStepExit}
        >
          <Step data={'intro'} key={'intro'}>
            <div className='mt-10 lg:ml-[10vw] step' key={'scrolly00'}>
              <p className='steptext'>{t('ys_intro')}</p>
              <p className='steptext'>
                <span
                  className='font-bold text-green-600 cursor-pointer hover:text-green-300'
                  onClick={() => {
                    window.scrollBy({ behavior: 'smooth', top: 900 });
                  }}
                >
                  {` ${t('forced_evictions')}`}
                </span>
                ,
                <Link
                  href={withBase('/stories/nationalisations_story')}
                  passHref
                >
                  <span className='font-bold text-green-600 cursor-pointer hover:text-green-300'>
                    {` ${t('nationalisations')}`}
                  </span>
                </Link>
                , {t('and')}{' '}
                <Link href={withBase('/stories/fires_story')} passHref>
                  <span className='font-bold text-green-600 cursor-pointer hover:text-green-300'>
                    {t('fire')}
                  </span>
                </Link>{' '}
                {t('second_intro')}
              </p>
              <div className='flex flex-col items-center content-center justify-center w-full align-middle'>
                <div
                  className='p-4 m-2 text-center border-2 border-white rounded-full cursor-pointer bg-brown'
                  onClick={() => {
                    if (menuRef.current)
                      menuRef.current.scrollIntoView({
                        behavior: 'smooth',
                        block: 'start',
                      });
                    ga.event({
                      action: 'go_to_menu_button',
                      category: 'Home Page',
                      label: 'Clicked Go to menu button on Home Page',
                    });
                  }}
                >
                  {t('go_to_menu')}
                </div>
                <div
                  className='p-4 m-2 text-center border-2 border-white rounded-full cursor-pointer bg-brown'
                  onClick={() => {
                    // if (firstStepRef.current)
                    // firstStepRef.current.scrollIntoView({ behavior: 'smooth', block: 'start' })
                    window.scrollBy({ behavior: 'smooth', top: 900 });
                  }}
                >
                  {t('scroll_read')}
                </div>
              </div>
            </div>
          </Step>
          {/* <div className='h-10' ref={firstStepRef}>&nbp;</div> */}
          {storyData ? (
            Object.keys(storyData).map((step) => {
              const images =
                storyData[step].image &&
                storyData[step].image.length > 0 &&
                storyData[step].image.map((row) => {
                  const imageId =
                    row['ImageIds'] && row['ImageIds'].length > 0
                      ? row['ImageIds'][0]
                      : '';

                  return {
                    caption:
                      router.locale == 'mm'
                        ? row['caption_mm']
                        : row['caption_en'],
                    image: getImagePath(imageId, 'lg'),
                    thumbnail: getImagePath(imageId, 'sm'),
                    reference: row['reference'],
                  };
                });

              return (
                <Step data={storyData[step]} key={step}>
                  <div className='lg:ml-[10vw] step' key={'scrolly' + step}>
                    <p className='steptext'>
                      {router.locale == 'mm'
                        ? storyData[step].paragraph[0].caption_mm
                        : storyData[step].paragraph[0].caption_en}
                    </p>
                    {storyData[step].image &&
                    storyData[step].image.length > 0 ? (
                      <ImageGallery
                        images={images}
                        captionPosition={'bottom'}
                        darkBg={true}
                      />
                    ) : (
                      <></>
                    )}
                  </div>
                </Step>
              );
            })
          ) : (
            <></>
          )}
          <div className='h-0' ref={menuRef}></div>
          <Step data={null} key={'bottomNav'}>
            <div className='grid justify-center grid-cols-1 pb-10 mx-24 mb-40 gap-y-6 gap-x-6 sm:grid-cols-2 lg:grid-cols-3 xl:gap-x-4 align-center'>
              {menuData.map((menuItem) => {
                return (
                  <Link
                    href={withBase(menuItem.title)}
                    key={'menu' + menuItem.title}
                    passHref
                  >
                    <div
                      className='w-full p-6 overflow-hidden cursor-pointer rounded-3xl min-h-30 aspect-w-1 aspect-h-1 group-hover:opacity-75 lg:h-30 lg:aspect-none'
                      style={menuItem.style}
                    >
                      <h2 className='text-3xl font-extrabold text-center'>
                        {t(`common:${menuItem.title}`)}
                      </h2>
                      <p className='max-w-2xl mt-4 text-sm'>
                        {menuItem.description}
                      </p>
                    </div>
                  </Link>
                );
              })}
            </div>
          </Step>
        </Scrollama>
      </div>
    </div>
  );
};

export default EvictionsStoryScrolly;
