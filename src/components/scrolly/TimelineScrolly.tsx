import { ArrowCircleRightIcon } from '@heroicons/react/solid';
import { scaleLinear } from 'd3-scale';
import Link from 'next/dist/client/link';
import React, { useEffect, useRef, useState } from 'react';
import { Scrollama, Step } from 'react-scrollama';
import { XIcon } from '@heroicons/react/outline';
import {
  detailPeriodToAggregatedPeriod,
  topTimeLineDefaultWidth,
} from '@/lib/constants';
import {
  periodStyles,
  timelineBarWidth,
  timelineHeight,
} from '@/lib/constants';
// import tinycolor from 'tinycolor2';
import { eventTypes } from '@/lib/dataparsers';
import { truncate } from '@/lib/utils';
import { withBase } from '@/lib/utils';
import useTranslation from 'next-translate/useTranslation';

import TopTimelineComponent from '@/components/TopTimelineComponent';

import TimelineTooltip from '../TimelineTooltip';

function TimelineScrolly(props) {
  const { timelineData } = props;
  const { t } = useTranslation('timeline');

  const [currentPeriod, setCurrentPeriod] = useState();
  const [highlightEvent, setHighlightEvent] = useState(false);
  const [fixToolTip, setFixToolTip] = useState(false);
  const [showDescMobile, setShowDescMobile] = useState(false);
  const [currentYear, setCurrentYear] = useState(
    timelineData.allYearsRange ? timelineData.allYearsRange[0] : 1852
  );
  //-------------------------------------------------------------

  const [jumpTo, setJumpTo] = useState();

  const [topTimelineWidth, setTopTimelineWidth] = useState(
    topTimeLineDefaultWidth
  );

  const topTimeLineRef = useRef();

  const jumpToPeriod = (period) => {
    // console.log("Jump to " + period.name)
    setJumpTo(period.name);
    setCurrentPeriod(period);
  };

  // const changeCurrentPeriod = useCallback((newPeriod) => {
  //   setCurrentPeriod(newPeriod)
  // },[setCurrentPeriod])

  useEffect(() => {
    // console.log(timelineData)
    if (
      timelineData &&
      timelineData.periodsSortedArray &&
      timelineData.periodsSortedArray.length > 0
    )
      setCurrentPeriod(timelineData.periodsSortedArray[0]);
  }, [timelineData]);

  const getAggregatePeriod = (periodName) => {
    if (Object.keys(detailPeriodToAggregatedPeriod).includes(periodName)) {
      return detailPeriodToAggregatedPeriod[periodName];
    }
  };

  const checkCurrentAggregatePeriod = (periodNameToCheck) => {
    return (
      currentPeriod &&
      currentPeriod.name &&
      getAggregatePeriod(currentPeriod.name) === periodNameToCheck
    );
  };

  const topTimeLineRefCheck =
    topTimeLineRef.current && topTimeLineRef.current.clientWidth;

  useEffect(() => {
    getTopTimelineWidth();
  }, [topTimeLineRefCheck]);

  useEffect(() => {
    window.addEventListener('resize', getTopTimelineWidth);
  }, []);

  const getTopTimelineWidth = () => {
    if (topTimeLineRef.current && topTimeLineRef.current.clientWidth) {
      setTopTimelineWidth(topTimeLineRef.current.clientWidth);
    } else {
      setTopTimelineWidth(topTimeLineDefaultWidth);
    }
  };
  //--------------------------------------------------

  useEffect(() => {
    if (
      timelineData &&
      timelineData.periodsSortedArray &&
      timelineData.periodsSortedArray.length > 0
    )
      setCurrentPeriod(timelineData.periodsSortedArray[0]);
  }, [timelineData]);

  const periodsRef = useRef([]);
  periodsRef.current = [];

  const scaleY = scaleLinear()
    .domain(timelineData.allYearsRange ? timelineData.allYearsRange : [0, 0])
    .range([0, timelineHeight]);

  useEffect(() => {
    if (jumpTo && periodsRef.current && periodsRef.current.length > 0) {
      // console.log(periodsRef)
      let refToSrollTo = periodsRef.current.filter((r) => {
        if (r && r.id) return r.id === jumpTo + 'periodsvg';
      });
      refToSrollTo = refToSrollTo.length > 0 ? refToSrollTo[0] : null;
      if (refToSrollTo) {
        refToSrollTo.scrollIntoView({ behavior: 'smooth', block: 'start' });
        // const refTop = refToSrollTo.getBoundingClientRect().top;
        // window.scrollBy(0,-180)
      }
    }
    // setJumpFinished(true);
  }, [jumpTo]);

  const onStepEnter = (scrollamaResponse) => {
    if (scrollamaResponse.data.name) {
      // console.log(scrollamaResponse);
      // clickHandler(scrollamaResponse.data);
      setCurrentPeriod(scrollamaResponse.data);
    }
    return null;
  };

  const onStepProgress = (scrollamaResponse) => {
    // console.log(scrollamaResponse)
    if (
      scrollamaResponse &&
      scrollamaResponse.data &&
      scrollamaResponse.data.yearRange &&
      scrollamaResponse.progress
    ) {
      const yearRange = scrollamaResponse.data.yearRange;
      const progress = scrollamaResponse.progress;
      const interpolatedYear = Math.ceil(
        +yearRange[0] + (yearRange[1] - yearRange[0]) * progress
      );
      setCurrentYear(interpolatedYear);
    }
  };

  // const onStepExit = () => {
  //   // console.log(scrollamaResponse.data.name);
  //   return null;
  // };

  const eventMouseEnter = (event, mouseEvent) => {
    // console.log('mouse enter', event)
    const clientRect = mouseEvent.target?.getBoundingClientRect();
    if (!fixToolTip) {
      setHighlightEvent({
        eventId: event.id,
        event: event,
        clientRect: clientRect,
      });
    }
  };

  const eventMouseOut = () => {
    // console.log("mouse leave",event)
    // setHighlightEvent(null);
    if (!fixToolTip) {
      setHighlightEvent(null);
    }
  };

  useEffect(() => {
    if (!fixToolTip) {
      setHighlightEvent(null);
    }
  }, [fixToolTip]);

  return (
    <div className='flex flex-wrap'>
      <div className='w-full' ref={topTimeLineRef}>
        {timelineData &&
          timelineData.periodsSortedArray &&
          timelineData.allYearsRange && (
            <div className='sticky top-0 z-40 mb-5 bg-white md:gap-2'>
              <TopTimelineComponent
                periods={timelineData.periodsSortedArray}
                clickHandler={jumpToPeriod}
                width={topTimelineWidth}
                yearRange={timelineData.allYearsRange}
                currentPeriod={currentPeriod}
                setShowDescMobile={setShowDescMobile}
              />
            </div>
          )}

        <div className='grid grid-cols-12 md:gap-2'>
          {timelineData &&
            timelineData.periodsSortedArray &&
            timelineData.allYearsRange && (
              <div className='col-span-12 md:col-span-3'>
                <div
                  className='sticky w-[50px] p-1 mb-2 text-xs text-center bg-white border-4'
                  style={{
                    top: 185,
                    left: timelineBarWidth,
                    zIndex: 50,
                    pointerEvents: 'none',
                    borderRadius: timelineBarWidth / 2,
                    borderColor: currentPeriod
                      ? periodStyles[currentPeriod.name].background
                      : periodStyles.background.background,
                  }}
                >
                  <p>{currentYear}</p>
                </div>
                <div className='flex flex-row'>
                  <div
                    className='w-[150px] steps'
                    style={{ height: timelineHeight * 1.2 }}
                  >
                    <Scrollama
                      offset={0.2}
                      onStepEnter={onStepEnter}
                      onStepProgress={onStepProgress}
                      // onStepExit={onStepExit}
                    >
                      {Object.values(timelineData.periods).map((period, i) => {
                        const isLastPeriod =
                          i === Object.values(timelineData.periods).length - 1;
                        const height = isLastPeriod
                          ? timelineHeight * 0.2 +
                            scaleY(period.yearRange[1]) -
                            scaleY(period.yearRange[0])
                          : scaleY(period.yearRange[1]) -
                            scaleY(period.yearRange[0]);
                        return (
                          <Step data={period} key={i}>
                            <div
                              className={period.name.split(' ').join('')}
                              style={{ height: height + 'px' }}
                            >
                              <svg
                                width={'100%'}
                                height={height}
                                id={period.name + 'periodsvg'}
                                ref={(el) => {
                                  // console.log(el)
                                  periodsRef.current.push(el);
                                }}
                              >
                                <g key={'period' + i}>
                                  <rect
                                    x={timelineBarWidth * 3}
                                    // y={scaleY(period.yearRange[0])}
                                    y={0}
                                    width={timelineBarWidth}
                                    height={height}
                                    fill={periodStyles[period.name].background}
                                    onClick={() => setCurrentPeriod(period)}
                                    stroke={'white'}
                                    strokeWidth={1}
                                    cursor={'pointer'}
                                  />
                                  <text
                                    x={timelineBarWidth * 3 - 5}
                                    y={0}
                                    textAnchor={'end'}
                                    dominantBaseline={'hanging'}
                                  >
                                    {period.yearRange[0]}
                                  </text>
                                </g>
                              </svg>
                            </div>
                          </Step>
                        );
                      })}
                    </Scrollama>
                  </div>
                  <div className='w-80'>
                    <svg height={timelineHeight * 1.2} width={'100%'}>
                      <g>
                        {timelineData && timelineData.allYearsRange ? (
                          eventTypes.map((d, i) => {
                            return (
                              <rect
                                key={'periodRect' + i}
                                x={i * timelineBarWidth * 2}
                                y={0}
                                width={timelineBarWidth * 1.5}
                                height={timelineHeight * 1.2}
                                fill={periodStyles['background'].background}
                              />
                            );
                          })
                        ) : (
                          <></>
                        )}

                        {timelineData && timelineData.periods ? (
                          Object.values(timelineData.periods).map((d, i) => {
                            return (
                              <g key={'periodText' + i}>
                                {d.events.map((event, j) => {
                                  let height =
                                    scaleY(event.yearRange[1]) -
                                    scaleY(event.yearRange[0]);
                                  if (height < timelineBarWidth)
                                    height = timelineBarWidth;
                                  return (
                                    <rect
                                      key={'event' + j + 'period' + i}
                                      x={
                                        eventTypes.indexOf(event.eventType) *
                                          timelineBarWidth *
                                          2 +
                                        timelineBarWidth * 0.25
                                      }
                                      y={scaleY(event.yearRange[0])}
                                      width={timelineBarWidth}
                                      height={height}
                                      fill={periodStyles[d['name']].background}
                                      stroke={
                                        highlightEvent &&
                                        highlightEvent.eventId === event.id
                                          ? 'black'
                                          : 'white'
                                      }
                                      strokeWidth={3}
                                      rx={timelineBarWidth / 2}
                                      ry={timelineBarWidth / 2}
                                      cursor={'pointer'}
                                      onMouseEnter={(mouseEvent) =>
                                        eventMouseEnter(event, mouseEvent)
                                      }
                                      onMouseDown={() => {
                                        if (!fixToolTip) setFixToolTip(true);
                                      }}
                                      onMouseOut={() => eventMouseOut()}
                                    />
                                  );
                                })}
                                {d.events.map((event) => {
                                  let height =
                                    scaleY(event.yearRange[1]) -
                                    scaleY(event.yearRange[0]);
                                  if (height < timelineBarWidth)
                                    height = timelineBarWidth;
                                  const shouldLabel =
                                    timelineData.validLabels[
                                      event.yearRange[0]
                                    ][0].title === event.title;
                                  const truncateLength =
                                    30 -
                                    eventTypes.indexOf(event.eventType) * 5;
                                  if (shouldLabel)
                                    return (
                                      <text
                                        x={
                                          eventTypes.indexOf(event.eventType) *
                                            timelineBarWidth *
                                            2 +
                                          25
                                        }
                                        y={
                                          scaleY(event.yearRange[0]) +
                                          timelineBarWidth * 0.7
                                        }
                                        // fill={periodStyles[d['name']].background}
                                        fill={'#444'}
                                        fontSize={'12px'}
                                        stroke={'#fff'}
                                        strokeWidth={'2px'}
                                        paintOrder={'stroke'}
                                        pointerEvents={'none'}
                                      >
                                        {truncate(
                                          event['title'],
                                          truncateLength
                                        )}
                                      </text>
                                    );
                                  else return <></>;
                                })}
                              </g>
                            );
                          })
                        ) : (
                          <></>
                        )}
                      </g>
                    </svg>
                  </div>
                </div>

                <TimelineTooltip
                  highlight={highlightEvent}
                  setFixToolTip={setFixToolTip}
                  fixToolTip={fixToolTip}
                />
              </div>
            )}

          <div className='col-span-12 md:col-span-9'>
            <div
              className={`flex flex-col w-full min-w-0 mb-6 break-words bg-white rounded z-50 fixed md:sticky ${
                showDescMobile ? 'block' : 'hidden'
              } md:block top-[156px]`}

              // style={{ position: 'sticky', top: '0' }}
            >
              <XIcon
                className='absolute block w-6 h-6 right-14 top-4 md:hidden'
                onClick={() => setShowDescMobile(false)}
                aria-hidden='true'
              />
              <div className='flex-auto px-4 mx-6'>
                <div className='tab-content tab-space bg-gray-50'>
                  {/* Colonial Period */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('Colonial Period')
                        ? 'block'
                        : 'hidden'
                    }
                    id='link1'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('colonial_period')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-4 md:grid-cols-3'>
                        <div className='p-2 md:col-span-2'>
                          <p className='text-sm'>
                            {t('colonial_period_desc')}
                            <Link href='/periods/colonial-period'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/Colonial1926SancyanBefore.png'
                            alt={t('colonial_period_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('colonial_period_img_desc')}
                          </p>
                        </div>
                      </div>
                      <br />
                    </section>
                  </div>
                  {/* Independence and Post-Independence */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('(Post) Independence')
                        ? 'block'
                        : 'hidden'
                    }
                    id='link2'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('indep_post_indep_period')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-3'>
                        <div className='p-2 md:col-span-2'>
                          <p className='text-sm'>
                            {t('indep_post_indep_period_desc')}
                            <br />
                            <Link href='/periods/the-independence-and-post-independence-periods'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/Independence_BogyokeTunSein_Thaketa1959_60_Years_Jubilee_of_Thakera1958-2018_Kyun_ma_tha_Maw_gun_Magazin_page332.jpg'
                            alt={t('indep_post_indep_period_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('indep_post_indep_period_img_desc')}
                          </p>
                        </div>
                      </div>
                    </section>
                  </div>
                  {/* NeWin */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('Ne Win') ? 'block' : 'hidden'
                    }
                    id='link3'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('ne_win_period')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-3'>
                        <div className='col-span-2 p-2'>
                          <p className='text-sm'>
                            {t('ne_win_period_desc')}
                            <br />
                            <Link href='/periods/ne-win-period'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/Rangoon_(1984)_02.jpeg'
                            alt={t('ne_win_period_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('ne_win_period_img_desc')}
                          </p>
                        </div>
                      </div>
                    </section>
                  </div>
                  {/* SLORC and SPDC */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('SLORC and SPDC')
                        ? 'block'
                        : 'hidden'
                    }
                    id='link4'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('slorc_spdc_period')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-3'>
                        <div className='col-span-2 p-2'>
                          <p className='text-sm'>
                            {t('slorc_spdc_period_desc')}
                          </p>
                          <p className='text-sm'>
                            {t('slorc_spdc_period_desc_quote')}
                            <br />
                            <Link href='/periods/slorc-and-spdc-period'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/SLORC_Period_map.jpeg'
                            alt={t('slorc_spdc_period_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('slorc_spdc_period_img_desc')}
                          </p>
                        </div>
                      </div>
                    </section>
                  </div>
                  {/* Transition */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('Transition')
                        ? 'block'
                        : 'hidden'
                    }
                    id='link5'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('transition_period')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-3'>
                        <div className='col-span-2 p-2'>
                          <p className='text-sm'>
                            {t('transition_period_desc')}
                            <br />
                            <Link href='/periods/transition-period'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                          {/* <p className='text-sm'>
                              “Where there should be flowers, there will be
                              flowers: where there should be trees, there will
                              be trees” (The Minister of Hotels and Tourism).
                            </p> */}
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/Transition_1_201705W67perspective.png'
                            alt={t('transition_period_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('transition_period_img_desc')}
                          </p>
                        </div>
                      </div>
                    </section>
                  </div>
                  {/* Post Coup */}
                  <div
                    className={
                      checkCurrentAggregatePeriod('Post Feb 2021 Coup')
                        ? 'block'
                        : 'hidden'
                    }
                    id='link5'
                  >
                    <section className='w-full px-4 md:px-12 mt-2 max-h-[70vh] overflow-y-auto'>
                      <div className='mt-10'>
                        <p className='text-xl font-bold leading-4'>
                          {t('postcoup_peroid')}
                        </p>
                      </div>
                      <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-3'>
                        <div className='p-2 md:col-span-2'>
                          <p className='text-sm'>
                            {t('postcoup_peroid_desc_1')}
                            <br />
                            {t('postcoup_peroid_desc_2')}
                            <br />
                            <Link href='/periods/post-coup-period'>
                              <div className='w-[100px] text-center p-2 px-4 m-2 text-sm font-medium border-2 rounded-full text-white cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  {t('common:more')}{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                            <Link href='/postcoup/timeline'>
                              <div className='w-[200px] p-2 px-4 m-2 text-sm font-medium text-center text-white border-2 rounded-full cursor-pointer hover:text-red-300 bg-brown'>
                                <a className=''>
                                  Post-coup Timeline{' '}
                                  <span>
                                    <ArrowCircleRightIcon
                                      className={'h-5 w-5 inline'}
                                    />
                                  </span>
                                </a>
                              </div>
                            </Link>
                          </p>
                        </div>
                        <div className='order-first p-2 mt-8 md:order-last'>
                          <img
                            className='object-scale-down w-full border-4 border-black border-solid rounded-xl'
                            src='/images/PostCoup_HTYSquattersEvicted_CreditsRFA_10282021.jpg'
                            alt={t('postcoup_peroid_img_desc')}
                          />
                          <p className='text-sm italic'>
                            {t('postcoup_peroid_img_desc')}{' '}
                            <a
                              href='https://www.rfa.org/english/video?v=1_fbh6636u'
                              className='text-green-600 cursor-pointer hover:text-green-300'
                            >
                              (RFA, 2021.)
                            </a>{' '}
                            Copyright © 1998-2020, RFA. Used with the permission
                            of Radio Free Asia, 2025 M St. NW, Suite 300,
                            Washington DC 20036.
                          </p>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default React.memo(TimelineScrolly);
