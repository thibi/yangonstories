import * as React from 'react';

import Header from './Header';

export default function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className='flex flex-col '>
      <Header />
      <main className='relative bg-white'>{children}</main>
    </div>
  );
}
