import Link from 'next/link';
import { useRouter } from 'next/router';
import * as React from 'react';
import { useState } from 'react';
import { Fragment } from 'react';

import { withBase } from '@/lib/utils';
import useTranslation from 'next-translate/useTranslation';

import { Disclosure, Menu, Transition } from '@headlessui/react';
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline';
import LocaleSwitcher from '@/components/LocaleSwitcher';

function classNames(...classes) {
  return classes.filter(Boolean).join(' ');
}

export default function Header() {
  const { asPath } = useRouter();
  const { t } = useTranslation('common');

  const [englishEnb, setEngEnb] = useState(true);

  const navigation = [
    { name: t('about'), href: '/about', current: false },
    { name: t('timeline'), href: '/timeline', current: false },
    { name: t('map'), href: '/map', current: false },
    { name: t('resources'), href: '/resources', current: false },
    { name: t('stories'), href: '/stories', current: false },
    { name: t('contact'), href: '/contact', current: false },
    { name: t('postcoup'), href: '/postcoup', current: false },
  ];

  const isActive = (href: any) => asPath === href;
  return (
    <Disclosure as='nav' className='sticky top-0 z-10 bg-brown'>
      {({ open }) => (
        <>
          <div className='px-2 mx-auto max-w-7xl sm:px-6 lg:px-8'>
            <div className='relative flex items-center justify-between h-24'>
              <div className='absolute inset-y-0 left-0 flex items-center sm:hidden'>
                {/* Mobile menu button*/}
                <Disclosure.Button className='inline-flex items-center justify-center p-2 text-gray-400 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white'>
                  <span className='sr-only'>Open main menu</span>
                  {open ? (
                    <XIcon className='block w-6 h-6' aria-hidden='true' />
                  ) : (
                    <MenuIcon className='block w-6 h-6' aria-hidden='true' />
                  )}
                </Disclosure.Button>
              </div>
              <div className='flex items-center justify-center flex-1 sm:items-stretch sm:justify-start'>
                <div className='flex items-center flex-shrink-0'>
                  <Link href='/'>
                    <img
                      width='200px'
                      src={`${withBase('/images/YangonStories_White.svg')}`}
                      alt='Yangon Stories'
                      className='cursor-pointer'
                    />
                  </Link>
                </div>
                <div className='hidden sm:block sm:ml-6'>
                  <div className='flex space-x-4'>
                    {navigation.map((item) => (
                      <Link key={item.name} href={item.href}>
                        <a
                          className={classNames(
                            isActive(item.href)
                              ? 'font-bold text-red-300'
                              : 'text-white hover:text-red-300',
                            'px-3 py-2 rounded-md text-md font-medium'
                          )}
                          aria-current={
                            isActive(item.href) ? 'page' : undefined
                          }
                        >
                          {item.name}
                        </a>
                      </Link>
                    ))}
                  </div>
                </div>
              </div>
              <div className='absolute inset-y-0 right-0 flex items-center hidden pr-2 sm:static md:block sm:inset-auto sm:ml-6 sm:pr-0'>
                <LocaleSwitcher setEngEnb={setEngEnb} />
              </div>
            </div>
          </div>

          <Disclosure.Panel className='sm:hidden'>
            <div className='px-2 pt-2 pb-3 space-y-1'>
              {navigation.map((item) => (
                <Disclosure.Button
                  key={item.name}
                  as='a'
                  className={classNames(
                    item.current
                      ? 'bg-gray-900 text-white'
                      : 'text-gray-300 hover:bg-gray-700 hover:text-white',
                    'block px-3 py-2 rounded-md text-base font-medium'
                  )}
                  aria-current={item.current ? 'page' : undefined}
                >
                  <Link href={item.href}>{item.name}</Link>
                </Disclosure.Button>
              ))}
            </div>
            <div className='py-6 text-center border-t border-white'>
              <LocaleSwitcher setEngEnb={setEngEnb} />
            </div>
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
}
