import { scaleLinear } from 'd3-scale';
import React, { useState } from 'react';
import tinycolor from 'tinycolor2';

import {
  periodStyles,
  topTimeLineBarHeight,
  topTimeLineHeight,
} from '@/lib/constants';

export default function TopTimelineComponent(props) {
  const {
    periods,
    yearRange,
    clickHandler,
    width,
    currentPeriod,
    setShowDescMobile,
  } = props;

  const margins = {
    left: 20,
    right: 20,
    bottom: 10,
    top: 10,
    barTop: 30,
  };
  const chartWidth = width - margins.left - margins.right;
  const numPeriods = periods ? periods.length : 0;
  const scaleX = scaleLinear().domain(yearRange).range([0, chartWidth]);

  const [hover, setHover] = useState();

  const periodMouseOver = (period) => {
    setHover(period);
  };

  const periodMouseOut = () => {
    setHover(null);
  };

  const shouldHighlight = (period) => {
    if (hover && hover.name === period.name) return true;
    if (currentPeriod && currentPeriod.name === period.name) return true;
  };

  const getTitleDisplayText = (period) => {
    return (
      period.name +
      ' (' +
      period.yearRange[0] +
      ' - ' +
      period.yearRange[1] +
      ')'
    );
  };

  const getDisplayPeriod = () => {
    return hover && hover.name && hover.yearRange
      ? hover
      : currentPeriod && currentPeriod.name && currentPeriod.yearRange
      ? currentPeriod
      : '';
  };

  return (
    <div className='flex flex-col pb-10'>
      <div className={'w-full'}>
        <svg width={width} height={topTimeLineHeight}>
          <g transform={'translate(' + margins.left + ',' + margins.top + ')'}>
            {
              <clipPath id={'topTimelineExtentClip'}>
                <rect
                  width={chartWidth}
                  height={topTimeLineBarHeight}
                  x={0}
                  y={margins.barTop}
                  rx={5}
                  ry={5}
                />
              </clipPath>
            }
            {periods.map((period, i) => {
              const startX = scaleX(period.yearRange[0]);
              const endX = scaleX(period.yearRange[1]);
              const color = {
                highlight: periodStyles[period.name].background,
                // nonHighlight: tinycolor(periodStyles[period.name].background).desaturate(80)
                nonHighlight: tinycolor(
                  periodStyles[period.name].background
                ).setAlpha(0.4),
              };
              return (
                <g key={'periodBarLabel' + i}>
                  <rect
                    width={endX - startX}
                    height={topTimeLineBarHeight}
                    x={startX}
                    y={margins.barTop}
                    fill={
                      shouldHighlight(period)
                        ? color.highlight
                        : color.nonHighlight
                    }
                    strokeWidth={1}
                    strokeOpacity={1}
                    stroke={'white'}
                    cursor={'pointer'}
                    clipPath={'url(#topTimelineExtentClip)'}
                    onMouseOver={() => periodMouseOver(period)}
                    onMouseOut={() => periodMouseOut(period)}
                    onClick={() => clickHandler(period)}
                  />
                </g>
              );
            })}
            {periods.map((period, i) => {
              const startX = scaleX(period.yearRange[0]);
              const endX = scaleX(period.yearRange[1]);
              const toolSmall = endX - startX < 12 && i < numPeriods - 1;
              const textAlternatePosition =
                i % 2 === 0
                  ? margins.barTop - 5
                  : margins.barTop * 1.5 + topTimeLineBarHeight;
              const circleAlternatePosition =
                i % 2 === 0
                  ? margins.barTop
                  : margins.barTop + topTimeLineBarHeight;
              return (
                <g key={'periodBarLabel' + i}>
                  {toolSmall ? (
                    <></>
                  ) : (
                    <text
                      x={startX}
                      y={textAlternatePosition}
                      textAnchor={'middle'}
                      fill={'#444'}
                      fontSize={'12px'}
                    >
                      {period.yearRange[0]}
                    </text>
                  )}

                  <circle
                    cx={startX}
                    cy={circleAlternatePosition}
                    r={3}
                    stroke={'white'}
                    strokeWidth={1}
                    fill={periodStyles[period.name].background}
                  />
                </g>
              );
            })}
          </g>
        </svg>
      </div>

      <div
        className={
          'flex items-center align-start justify-start w-full h-10 pl-5 pr-5'
        }
        onClick={() => setShowDescMobile(true)}
      >
        {(hover && hover.name) || (currentPeriod && currentPeriod.name) ? (
          <div
            className='flex flex-col items-center justify-center w-full p-2 font-bold align-middle border-4 solid rounded-xl'
            style={{
              backgroundColor: periodStyles.background.background,
              borderColor: periodStyles[getDisplayPeriod().name].background,
              color: periodStyles.background.color,
            }}
          >
            <p>{getTitleDisplayText(getDisplayPeriod())}</p>
          </div>
        ) : (
          <div className=''>
            <p>&nbsp;</p>
          </div>
        )}
      </div>
    </div>
  );
}
