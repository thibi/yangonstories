import { ArrowCircleRightIcon } from '@heroicons/react/solid';
import ImageGallery from '@/components/ImageGallery';
import Link from 'next/link';
import React from 'react';
import { useRouter } from 'next/router';
import { getImagePath } from '@/lib/utils';

export default function FeatureStory(props) {
  const { storyData, pathId, pathLabel } = props;
  const router = useRouter();

  return storyData ? (
    <div>
      {Object.keys(storyData).map((step, i) => {
        //console.log('Step', step, storyData[step]);
        const images =
          storyData[step].image &&
          storyData[step].image.length > 0 &&
          storyData[step].image.map((row) => {
            const imageId =
              row['ImageIds'] && row['ImageIds'].length > 0
                ? row['ImageIds'][0]
                : '';

            return {
              caption:
                router.locale == 'mm' ? row['caption_mm'] : row['caption_en'],
              image: getImagePath(imageId, 'lg'),
              thumbnail: getImagePath(imageId, 'sm'),
              reference: row['reference'],
            };
          });

        return (
          <div
            className='lg:mx-[10vw] sm:mx-4 grid grid-cols-5'
            key={'scrolly' + i}
          >
            {storyData[step].title && (
              <div className='col-span-5 my-20 text-center'>
                <h1 className='text-2xl font-bold'>
                  {storyData[step].title[0].caption}
                </h1>
              </div>
            )}

            <div className='col-span-5 bg-white md:col-span-3'>
              {storyData[step].paragraph &&
                storyData[step].paragraph.map((paragraph, j) => {
                  return (
                    <p className='px-5 my-10' key={'caption' + j}>
                      {router.locale == 'mm'
                        ? paragraph.caption_mm
                        : paragraph.caption_en}
                    </p>
                  );
                })}
            </div>
            <div
              className={
                'col-span-5 px-5 ' +
                (!storyData[step].paragraph
                  ? 'md:col-span-5 my-10'
                  : 'md:col-span-2')
              }
            >
              {storyData[step].image && storyData[step].image.length > 0 ? (
                <ImageGallery images={images} captionPosition={'bottom'} />
              ) : (
                <></>
              )}
            </div>
          </div>
        );
      })}

      <div className='flex items-center justify-center'>
        <Link href={{ pathname: '/map', query: { id: pathId } }}>
          <div className='p-2 px-4 m-2 text-sm font-medium text-center text-white border-2 rounded-full cursor-pointer hover:text-red-300 bg-brown'>
            <a className=''>
              {pathLabel}{' '}
              <span>
                <ArrowCircleRightIcon className={'h-5 w-5 inline'} />
              </span>
            </a>
          </div>
        </Link>
      </div>
    </div>
  ) : (
    <></>
  );
}
