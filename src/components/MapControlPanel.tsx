import React from 'react';

// import { yangonCenter } from '@/lib/constants';

export default function MapControlPanel(props) {
  const { filters, currentFilters, filterHandler, getLegendElement } = props;
  // console.log(filters,currentFilters)

  return (
    <div className='control-panel'>
      <h3>Filters and Legend</h3>
      {/* <p>Dynamically show/hide map layers and change color with Immutable map style.</p> */}
      {/* <div className="source-link">
        <a
          href="https://github.com/visgl/react-map-gl/tree/7.0-release/examples/layers"
          target="_new"
        >
          View Code ↗
        </a>
      </div> */}
      <hr />
      {Object.keys(filters).map((filterKey) => {
        const filter = filters[filterKey];
        return (
          <div key={filterKey} className='m-5 input'>
            <h4 className='text-xl'>{filterKey}</h4>
            {filter.map((filterValue) => {
              // console.log(filterKey)
              return (
                <div key={filterKey + '_' + filterValue}>
                  <input
                    type='checkbox'
                    defaultChecked={currentFilters[filterKey].includes(
                      filterValue
                    )}
                    onChange={() => filterHandler(filterKey, filterValue)}
                  />
                  {getLegendElement(filterKey, filterValue)}
                  <label className='px-2'>{filterValue}</label>
                  <br />
                </div>
              );
            })}
          </div>
        );
      })}
    </div>
  );
}
