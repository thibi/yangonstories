import { isURL } from '@/lib/utils';
import React, { useMemo } from 'react';
import { useTable } from 'react-table';

// import { yangonCenter } from '@/lib/constants';

export default function MapTable(props) {
  // console.log(props)
  const data = useMemo(() => props.data, [props.data]);

  const columns = useMemo(() => props.columns, [props.columns]);

  const { headerGroups, rows, prepareRow } = useTable({ columns, data });

  const getCellRender = (cell, cellType) => {
    if (cellType === 'url') {
      // console.log(cell.value)
      return cell.value === 'Unknown' ? (
        ''
      ) : // <a href={cell.value} className='underline'>
      //   Link
      isURL(cell.value) ? (
        <a
          className='underline'
          href={cell.value}
          rel='noreferrer'
          target={'_blank'}
        >
          Link
        </a>
      ) : (
        cell.value
      );
    }
    return cell.render('Cell');
  };

  return (
    // <div className='px-4 sm:px-6 lg:px-8'>
    <div className='flex flex-col'>
      <div className='-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8'>
        <div className='inline-block min-w-full py-2 align-middle md:px-6 lg:px-8'>
          <div className='overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg'>
            <table className='min-w-full divide-y divide-gray-300'>
              <thead className='bg-brown'>
                {headerGroups.map((headerGroup, i) => (
                  <tr
                    key={'headerrow' + i}
                    {...headerGroup.getHeaderGroupProps()}
                  >
                    {headerGroup.headers.map((column) => (
                      <th
                        key={'headercell' + i}
                        {...column.getHeaderProps()}
                        scope='col'
                        className='px-3 py-3.5 text-left text-sm font-semibold text-gray-900'
                      >
                        {column.render('Header')}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
              <tbody className='bg-white divide-y divide-gray-200'>
                {rows.map((row, i) => {
                  prepareRow(row);
                  return (
                    <tr key={'datarow' + i} {...row.getRowProps()}>
                      {row.cells.map((cell, j) => {
                        const { column } = cell;
                        return (
                          <td
                            key={'cell' + j}
                            {...cell.getCellProps()}
                            className='px-3 py-4 text-sm text-gray-500 align-top'
                          >
                            {getCellRender(cell, column.type)}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      {/* </div> */}
    </div>
  );
}
