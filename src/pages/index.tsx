import React from 'react';
// import axios from 'axios';
// import { withBase } from '@/lib/utils';
// import Map, { Layer, Source } from 'react-map-gl';
// import { nest } from 'd3-collection';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import dynamic from 'next/dynamic';

export default function HomePage() {
  const EvictionsStoryScrolly = dynamic(
    () => import('@/components/scrolly/EvictionsStoryScrolly'),
    { ssr: false }
  );

  return (
    <Layout>
      <Seo />
      <EvictionsStoryScrolly />
    </Layout>
  );
}
