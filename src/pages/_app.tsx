import { AppProps } from 'next/app';
import { useRouter } from 'next/router';
import { useEffect } from 'react';

// import '@/styles/mapbox-gl.css';
import 'mapbox-gl/dist/mapbox-gl.css';
import '@/styles/globals.css';

import * as ga from '@/lib/gtag';

function MyApp({ Component, pageProps }: AppProps) {
  const router = useRouter();
  const { events } = router;

  useEffect(() => {
    const handleRouteChange = (url: URL) => {
      ga.pageview(url);
    };
    events.on('routeChangeComplete', handleRouteChange);
    return () => {
      events.off('routeChangeComplete', handleRouteChange);
    };
  }, [events]);

  return (
    <div className='min-h-screen'>
      <Component {...pageProps} />
    </div>
  );
}

export default MyApp;
