import { useState, useEffect } from 'react';
import useTranslation from 'next-translate/useTranslation';
import { withBase } from '@/lib/utils';
import AboutYS from '../datasets/About_YS.json';
import { nest } from 'd3-collection';
import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import ParagraphParser from '@/components/ParagraphParser';

const AboutPage = () => {
  const { t } = useTranslation('common');

  const [aboutData, setAboutData] = useState();

  useEffect(() => {
    const tempAboutData = nest()
      .key((d) => d['step'])
      .key((d) => d['type'])
      .object(AboutYS);
    setAboutData(tempAboutData);
  }, []);

  return (
    <Layout>
      <Seo title='About' description='Yangon Stories About Page' />
      <section className='container max-w-screen-xl px-6 py-6 mx-auto bg-white md:px-40 sm:mt-5 md:mt-10'>
        <div className='flex flex-row items-center justify-center m-4'>
          <div className='px-10'>
            <img
              width='100'
              height='150'
              src={withBase('images/YangonStories_Icon_Black.svg')}
              alt='Yangon Stories'
            />
          </div>
          <div className=''>
            <p className='text-3xl font-extrabold'>About Yangon Stories</p>
          </div>
        </div>
        <ParagraphParser data={aboutData} />
      </section>
    </Layout>
  );
};

export default AboutPage;
