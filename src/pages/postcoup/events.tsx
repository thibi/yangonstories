import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';

const EventsPage = () => {
  const [menuType, setMenuType] = useState('');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 md:grid-cols-5 gap-4 my-6'>
          <StoriesMenu
            currentSelection='events'
            optionsSet={menuType ? menuType : 'postcoup'}
          />
          <div className='md:col-span-4'>
            <div className='story-wrapper'>
              <div key={'essay'} className='m-5 md:m-10 text-left '>
                <div className='p-8 '>
                  <div className='mb-4 text-center '>
                    <h1 className='text-2xl font-bold'>Category Explanation</h1>
                  </div>
                  <div>
                    <h2 className='my-8 mb-4 text-xl font-bold'>Protest</h2>
                    <ul className='my-4 list-disc'>
                      <li className='my-2'>
                        <span className='font-bold'>Mass Protest:</span> Mass
                        protests happened across the country in cities, suburbs
                        and also in rural settings in the aftermath of the coup,
                        i.e. from early February to late March. On 5th February,
                        what may be considered as the first mass protest was
                        initiated by factory workers from their factory
                        compounds and the next day they were joined by
                        additional groups on the streets of Yangon and Mandalay.
                        A variety of groups based on interest, identity,
                        affiliation, and/or profession, took to the streets of
                        many cities across the country to demand restoration of
                        democracy and the release of their elected leaders from
                        arrest. Before the crackdowns and shooting with real
                        bullets by military forces of the mid and late March,
                        people had been able to peacefully protest in their
                        townships;
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>
                          Civil Disobedience Movement:
                        </span>{' '}
                        The Civil Disobedience Movement (CDM) has been a mass
                        protest movement since the February 1, 2021 coup. Very
                        soon after the coup, government officers and servants
                        refused to work and assume their responsibilities under
                        the new military administration. The movement was then
                        amplified by employees from the private sector who also
                        claimed to be CDMers. CDMers from the government sector
                        have been particularly targeted by military and police
                        forces as the new military administration wants to
                        project the image of a functioning administration;
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>Flash Mob:</span> After the
                        military forces’ brutal crackdowns on street protesters
                        and the many arrests of people at their homes, mass
                        protests on the streets stopped. However different
                        groups of activists have remained mobilised and strongly
                        determined to demonstrate on the streets to show how
                        strong resistance against the coup and desire for
                        democracy, especially among youths, remains. These flash
                        mobs take the form of a sudden assembly in the public
                        space performed for a brief time before quickly
                        dispersing. It has been an on-going form of protest in
                        different public areas and it seems that flash mob
                        protesters remain committed to conducting them until
                        victory of the democracy movement;
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>
                          Labour Union/Organisation Campaign:
                        </span>{' '}
                        Campaigns by labour union organisations have been held
                        so as to stop money flows to the junta and persuade
                        foreign investors to stop any investment in the country,
                        and also to lobby for a ban on new orders at garment
                        companies from large western cloth companies;
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>
                          Home protests consist of the three following actions:
                        </span>{' '}
                        one is the pot-banging, the other digital protest, and
                        the last one silent strike. For the pot-banging,
                        starting from February 2, every night at 8 o’clock,
                        people in their residence banged pots and pans from
                        their kitchen to show solidarity with their arrested
                        leaders and unity against the coup. This is an
                        adaptation of a traditional practice of driving out evil
                        spirits usually performed after sunset and on the first
                        day of the new year in the Myanmar calendar once sermons
                        from monks are over on that first new year’s day.
                        However, banging of pots as home protests gradually
                        decreased starting from late May following soldiers’
                        constant raids in the neighbourhoods and also due to
                        soldiers’ warning that if banging of pots continued,
                        soldiers would throw stones into peoples’ homes and
                        arrests would be conducted. Banging of pots eventually
                        stopped as a type of home protest. Following the brutal
                        crackdowns by military forces on the streets, people
                        turned to another type of home protests, that is digital
                        protest. Pictures of candles, flowers, flowers used as
                        women’s hairpins, peoples’ homes in the dark, signs
                        showing resistance against slave education, or blue
                        shirts worn by people on symbolic days were posted on
                        Facebook posts and other social media and symbolically
                        represented a collective protest. These digital protests
                        have covered a whole range of symbolic acts of
                        resistance from various communities across the country
                        and also across communities. Lastly, silent protests are
                        performed by the public at large, government servants,
                        and any person when the population decides to remain
                        home from mid-morning to mid-afternoon, leaving the
                        impression that cities and their public space have been
                        deserted by people in a show of people power. These
                        protests have generally followed the military forces’
                        brutal crackdown of groups of street protesters. In
                        Hlaing Thayar, the silent protests were even more
                        prominent because of the martial law imposed on the
                        township residents and the restrictions on their
                        mobility;
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>Non-Human Protest:</span>{' '}
                        These acts of resistance consist in showing placards,
                        sticking posters, dolls or toys holding placards, or any
                        materials on which messages criticising the coup are
                        painted or written. Pictures are taken of these items
                        with no presence of human beings around and very often
                        on sites where protests occurred previously and then
                        posted on social media;{' '}
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>
                          Boycott of military administration and its affiliated
                          entities:
                        </span>{' '}
                        This boycott takes the form of three actions: (1) no
                        collaboration with military-linked businesses and
                        government administration (including refusal to bring
                        kids to school); (2) no payments to military
                        administration’s utilities (electricity, IRD, etc...)
                        and purchases of goods linked to military-owned
                        businesses; (3) no money flow sustaining the military
                        activities (especially through banks);{' '}
                      </li>
                      <li className='my-2'>
                        <span className='font-bold'>
                          Memorial for fallen protesters:
                        </span>{' '}
                        Usually held with surrounding banners featuring words of
                        commitment to resistance against the coup, memorials
                        consist of tree-planting ceremonies commemorating fallen
                        protesters (especially those from the March 14 crackdown
                        in Hlaing Thar Yar). Tree-planting ceremonies are
                        performed at locations chosen for their convenience that
                        may not necessarily relate to the place where protesters
                        fell.
                      </li>
                    </ul>
                    <h2 className='my-8 mb-4 text-xl font-bold'>
                      Announcements and statements
                    </h2>
                    <ul className='my-4 list-disc'>
                      <li className='my-2'>
                        Announcement and statements made by SAC, including
                        announcements or notice about declaring groups as
                        ‘terrorist organizations’, death penalty, prison
                        sentence, martial law, clearance of
                        squatters’settlements, ban on motorbikes, etc…;
                      </li>
                      <li className='my-2'>
                        Announcements and statements by CRPH/ NUG, PDF, LDF,
                        EAOs;
                      </li>
                      <li className='my-2'>
                        Statements by Students and Labour Union Organisations;
                      </li>
                      <li className='my-2'>
                        Statements by the United Nations, ASEAN, Foreign
                        Embassies, and INGOs.
                      </li>
                    </ul>
                    <h2 className='my-8 mb-4 text-xl font-bold'>
                      Coercion and Abuse of Power
                    </h2>
                    <ul className='my-4 list-disc'>
                      <li className='my-2'>
                        Coercion and abuse of power have been a constant trait
                        of former administrations in Myanmar; however, since the
                        February 1 coup, they have been exerted by SAC with an
                        even more acute intensity. They manifest themselves in
                        peoples’ lives in many different ways:
                      </li>
                      <li className='my-2'>
                        Pressure by SAC on squatters and residents who had built
                        their homes during Thein Sein’s government to destroy
                        their homes and pressure to pay bribe money to renew
                        guest lists;{' '}
                      </li>
                      <li className='my-2'>
                        Pressure by SAC on the population to pay utility bills
                        on a monthly basis after months of refusing to do so as
                        an act of protest against the coup (bills for
                        electricity, bills for YCDC, etc,...); In some cases,
                        military forces together with ward administrators
                        threatened residents to cut electricity supply at their
                        homes if not paying the due bills. In other cases,
                        electricity lines were cut without prior notice;{' '}
                      </li>
                      <li className='my-2'>
                        Systematic and intrusive verification of personal data
                        and belongings without personal consent: Civilians have
                        been forced to submit overnight guest lists to ward
                        authorities. They have had their mobile phone checked,
                        houses inspected, cars inspected, bags inspected at any
                        time of the day when not at home;{' '}
                      </li>
                      <li className='my-2'>
                        Corvee labour by SAC: Civilians could randomly be forced
                        by military forces to remove barricades built by
                        protesters so as to resume car traffic and make it
                        easier for military vehicles to move around;{' '}
                      </li>
                      <li className='my-2'>
                        Restrictions by military authorities: Access to basic
                        health care, health care equipment, and supply of oxygen
                        has been restricted. Myanmar’s land border checkpoints
                        with China and Thailand were shut during the Covid-19
                        third wave in July 2021 when the death toll was at its
                        highest due to lack of basic health care and equipment
                        and the military ban on access to the relevant
                        equipment;{' '}
                      </li>
                      <li className='my-2'>
                        Ban on motorbike riding and tricycle and restrictions on
                        motorbike-sharing with one passenger on a backseat, and
                        restrictions on certain types of cloth wearing (like
                        black shirts, etc…);{' '}
                      </li>
                      <li className='my-2'>
                        Crackdown on freedom of opinion and expression and
                        restrictions on access to food assistance and medicines
                        as seen with the white and yellow flag campaigns during
                        the Covid-19 third wave;
                      </li>
                      <li className='my-2'>
                        Restrictions imposed by SAC on the imports of healthcare
                        equipment which led to a surge of mortality among
                        communities who lacked access to healthcare.
                      </li>
                    </ul>
                    <h2 className='my-8 mb-4 text-xl font-bold'>Resistance</h2>
                    <ul className='my-4 list-disc'>
                      <li className='my-2'>
                        Confrontation by protestors: This resistance by
                        protestors took the form of an urban guerilla when
                        barricades were built and makeshift shields (some
                        retrofitted cooking pans, etc….), slingshots, molotov
                        cocktail, air-guns were used by protesters to defend
                        themselves against military forces;{' '}
                      </li>
                      <li className='my-2'>
                        Counter attack by urban guerilla protesters and ambush
                        against soldiers, police forces and informers for which
                        responsibility has been claimed by PDFs;
                      </li>
                      <li className='my-2'>
                        Donations made to local PDFs and collective donations to
                        Hlaing Thar Yar squatters;
                      </li>
                      <li className='my-2'>
                        Community support during pandemic.
                      </li>
                    </ul>
                    <h2 className='my-8 mb-4 text-xl font-bold'>Violence</h2>
                    <ul className='my-4 list-disc'>
                      <li className='my-2'>
                        Crackdown by military and police forces on freedom of
                        peaceful assembly and association;
                      </li>
                      <li className='my-2'>
                        Crackdown by military and police forces on journalists
                        and citizen journalists during street reporting;
                      </li>
                      <li className='my-2'>
                        Use of lethal force by military forces;
                      </li>
                      <li className='my-2'>
                        Murders or violent actions on individuals by
                        unidentified or anonymous perpetrators;
                      </li>
                      <li className='my-2'>
                        Abduction, detention, imprisonment;
                      </li>
                      <li className='my-2'>Torture;</li>
                      <li className='my-2'>
                        Enforced disappearance of persons;
                      </li>
                      <li className='my-2'>
                        Forced eviction: The military administration forcibly
                        evicted communities living in official housing estates
                        or informal makeshift homes against their will. This was
                        done without legal procedures and protections for the
                        evicted communities. In Hlaing Thayar, especially after
                        March 14, some evicted residents had to flee back to
                        their hometowns;
                      </li>
                      <li className='my-2'>
                        Seizure of properties (land and real estate) from people
                        allegedly linked to entities like PDFs, NUG, NLD…
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default EventsPage;
