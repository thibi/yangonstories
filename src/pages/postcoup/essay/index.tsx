import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
import Link from 'next/link';
import { useRouter } from 'next/router';

const EssayPage = () => {
  const [menuType, setMenuType] = useState('');
  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-5'>
          <div className='px-10'>
            <StoriesMenu
              currentSelection='essay'
              optionsSet={menuType ? menuType : 'postcoup'}
            />
          </div>
          <div className='md:col-span-4'>
            <div className='story-wrapper'>
              <div key={'essay'} className='m-10 text-left '>
                <div className='mb-8'>
                  <Link href='/postcoup/essay/analysis'>
                    <div className='grid grid-cols-1 gap-8 m-10 text-left cursor-pointer md:grid-cols-3 basis-1/4'>
                      <div className='p-6'>
                        <img
                          className='border-4 border-black rounded-3xl grayscale hover:grayscale-0'
                          src='/images/stories-bg-essays.jpg'
                          alt=''
                        />
                      </div>
                      <div className='p-8 md:col-span-2'>
                        <div className='h-10 mb-4'>
                          <h1 className='text-2xl font-bold'>
                            Analysis of the data collected for the Post-coup
                            Timeline of Spatial Violence in Hlaing Thar Yar and
                            Map of theevents
                          </h1>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
                <div>
                  <Link href='/postcoup/essay/outline'>
                    <div className='grid grid-cols-1 gap-8 m-10 text-left cursor-pointer md:grid-cols-3 basis-1/4'>
                      <div className='p-6'>
                        <img
                          className='border-4 border-black rounded-3xl grayscale hover:grayscale-0'
                          src='/images/stories-bg-essays.jpg'
                          alt=''
                        />
                      </div>
                      <div className='p-8 md:col-span-2'>
                        <div className='h-10 mb-4'>
                          <h1 className='text-2xl font-bold'>
                            Post-coup Spatial Violence in Hlaing Tha Yar in
                            Perspective
                          </h1>
                        </div>
                      </div>
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default EssayPage;
