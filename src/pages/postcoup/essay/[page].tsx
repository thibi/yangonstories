import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
//import StoriesMenu from '@/components/StoriesMenu';
import ParagraphParser from '@/components/ParagraphParser';
import Link from 'next/link';

import { useRouter } from 'next/router';
import { nest } from 'd3-collection';
import essayStepData from '../../../datasets/Post-Coup_Essays_Text.json';

const EssayDetail = () => {
  const [menuType, setMenuType] = useState('');

  const [essayData, setEssayData] = useState();

  const router = useRouter();
  const { page } = router.query;

  useEffect(() => {
    const currentEssayData = essayStepData.filter((val) => {
      return val.page == page;
    });
    const tempEssayData = nest()
      .key((d) => d['step'])
      .key((d) => d['type'])
      .object(currentEssayData);

    setEssayData(tempEssayData);
  }, []);

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-5'>
          {/* <StoriesMenu
            currentSelection='essay'
            optionsSet={menuType ? menuType : 'postcoup'}
          /> */}
          <div className='md:col-span-5'>
            <Link href='/postcoup/essay'>{'< BACK'}</Link>
            <div className='story-wrapper'>
              <div key={'essay'} className='m-10 text-left '>
                <div className='p-8 '>
                  <div className='mb-4'>
                    <div className='lg:mx-[10vw] sm:mx-4'>
                      <ParagraphParser data={essayData} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default EssayDetail;
