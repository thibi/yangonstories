import { timeFormat, timeParse } from 'd3-time-format';
import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
import postCoupTimelineData from '../../datasets/Post-coup_Timelines.json';
import { useRouter } from 'next/router';

const TimelinePage = () => {
  const [menuType, setMenuType] = useState('postcoup');
  const [currentMonth, setCurrentMonth] = useState();

  const router = useRouter();

  useEffect(() => {
    if (postCoupTimelineData && postCoupTimelineData.length > 0) {
      setCurrentMonth(postCoupTimelineData[0]);
    }
  }, []);

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  const formatMonthDisplay = (dateString) => {
    const parsedDate = timeParse('%Y-%m-%d')(dateString);
    const monthString = timeFormat('%b %Y')(parsedDate);
    return monthString;
  };

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 md:grid-cols-5 gap-4 my-6'>
          <StoriesMenu
            currentSelection='timeline'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='col-span-4'>
            <div className='story-wrapper'>
              <div className='flex flex-wrap justify-around w-full pr-4 m-2 top-30'>
                {currentMonth && currentMonth['Month'] ? (
                  postCoupTimelineData.map((story, i) => {
                    return (
                      <div
                        key={'timelineTab' + i}
                        className={
                          'p-2 px-4 m-2 font-medium text-sm rounded-full cursor-pointer ' +
                          (story['Month'] === currentMonth['Month']
                            ? 'bg-brown text-white border-2'
                            : 'bg-white hover:bg-brown border-2')
                        }
                        onClick={() => {
                          setCurrentMonth(story);
                        }}
                      >
                        {formatMonthDisplay(story['Month'])}
                      </div>
                    );
                  })
                ) : (
                  <></>
                )}
              </div>
              {currentMonth && currentMonth['GSheet ID'] ? (
                <iframe
                  src={
                    'https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=' +
                    currentMonth['GSheet ID'] +
                    '&font=Default&lang=en&initial_zoom=2&height=650'
                  }
                  width='100%'
                  height='650'
                  webkitallowfullscreen
                  mozallowfullscreen
                  allowFullScreen
                  frameBorder='0'
                ></iframe>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default TimelinePage;
