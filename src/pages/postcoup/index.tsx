import Link from 'next/link';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import useTranslation from 'next-translate/useTranslation';

const PostcoupPage = () => {
  const { t } = useTranslation('stories');
  return (
    <Layout>
      <Seo title='PostCoup' description='Yangon Stories | PostCoup' />
      <section className='container max-w-screen-xl mx-auto mt-2'>
        <div className='grid grid-cols-1 my-6 md:grid-cols-5 md:gap-4'>
          <div className='items-center col-span-1 p-2 '>
            <h2 className='text-xl font-bold uppercase'>
              {t('common:postcoup_violence')}
            </h2>
          </div>
          <div className='col-span-4 text-center'>
            <div className='grid grid-cols-1 gap-10 md:grid-cols-2 md:p-20'>
              <div className='px-10'>
                <Link href='/postcoup/events' passHref>
                  <div
                    className='mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-events.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold text-center'>
                        {t('common:events')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('postcoup_events_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href={{ pathname: '/postcoup/timeline' }} passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-timeline.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-400/[0.3] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:timeline')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('postcoup_timeline_desc')}</p>
              </div>
              <div className='px-10'>
                <Link
                  href={{ pathname: '/map', query: { id: 'postcoup' } }}
                  passHref
                >
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-map.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-400/[0.3] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>{t('common:map')}</h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('postcoup_map_desc')}</p>
              </div>
              <div className='px-10'>
                <Link
                  href={{
                    pathname: '/stories/interviews',
                    query: { id: 'postcoup' },
                  }}
                  passHref
                >
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-interviews.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:interviews')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('interviews_desc')}</p>
              </div>
              <div className='px-10'>
                <Link
                  href={{
                    pathname: '/stories/videos',
                    query: { id: 'postcoup' },
                  }}
                  passHref
                >
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-videos.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:video_clips')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('postcoup_videos_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href={{ pathname: '/postcoup/essay' }} passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-essays.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-400/[0.3] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold text-center'>
                        {t('common:essay')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('postcoup_essay_desc')}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default PostcoupPage;
