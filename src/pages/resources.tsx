import React, { useEffect, useMemo, useState } from 'react';

import { parseResourcesData } from '@/lib/dataparsers';
import { buildFilteredDataSet, filterHandler } from '@/lib/utils';

import FiltersGroupComponent from '@/components/FiltersGroupComponent';
import Layout from '@/components/layout/Layout';
import MapTable from '@/components/MapTable';
import Seo from '@/components/Seo';
import { ChevronRightIcon, ChevronDownIcon } from '@heroicons/react/outline';
import useTranslation from 'next-translate/useTranslation';

export default function ArchivePage() {
  const {
    filters,
    tableData,
    emptyFilters,
    xFilter,
    xfDims,
    xfGroups,
    tableColumns,
  } = useMemo(() => parseResourcesData(), []);

  const { t } = useTranslation('resources');

  const [currentFilters, setCurrentFilters] = useState(null);

  const [openFilter, setOpenFilter] = useState(false);

  useEffect(() => {
    if (filters && emptyFilters) {
      setCurrentFilters(emptyFilters);
    }
  }, [emptyFilters, filters]);

  // console.log(filterCats, emptyFilters,
  //   filters,tableData,xfGroups,xfDims, currentFilters)

  return (
    <Layout>
      <Seo title='Resources' description='Yangon Stories | Resources' />
      <div className='grid w-full grid-cols-1 md:grid-cols-5 h-content'>
        <div className='min-w-[300px] col-span-1 px-4 overflow-y-auto'>
          {filters && currentFilters && (
            <div className='control-panel m-5'>
              <div
                className='flex panel-title'
                onClick={() => setOpenFilter(!openFilter)}
              >
                <h3 className='flex-1'>{t('filters')}</h3>
                {openFilter ? (
                  <ChevronDownIcon
                    className='inline-block md:hidden h-4 w-4 flex-end'
                    aria-hidden='true'
                  />
                ) : (
                  <ChevronRightIcon
                    className='inline-block md:hidden h-4 w-4 flex-end'
                    aria-hidden='true'
                  />
                )}
              </div>
              <hr />
              <div
                className={`panel-content ${
                  openFilter ? 'block' : 'hidden'
                } md:block`}
              >
                {Object.keys(filters).map((filterKey) => {
                  const filter = filters[filterKey];
                  return (
                    <div key={filterKey} className='input my-5'>
                      <h4 className='text-xl'>{filterKey}</h4>
                      {/* {filter.map((filterValue) => { */}
                      {/* return ( */}
                      <FiltersGroupComponent
                        filterKey={filterKey}
                        filter={filter}
                        currentFilter={
                          currentFilters[filterKey]
                            ? currentFilters[filterKey]
                            : []
                        }
                        currentGroupCounts={xfGroups[filterKey].all()}
                        filterHandler={filterHandler(
                          tableData,
                          currentFilters,
                          setCurrentFilters,
                          xFilter,
                          xfDims,
                          xfGroups
                        )}
                      />
                      {/* ); */}
                      {/* })} */}
                    </div>
                  );
                })}
              </div>
            </div>
          )}
        </div>
        <div role='main' className='flex-grow md:col-span-4 px-3 pt-1'>
          <div className='text-center md:m-10'>{t('info_request')}</div>
          {tableData && xfDims && currentFilters && tableColumns && (
            <MapTable
              columns={tableColumns}
              data={buildFilteredDataSet(
                tableData,
                xfDims,
                currentFilters,
                true,
                false
              )}
            />
          )}
        </div>
      </div>
    </Layout>
  );
}

/*

      <section className='container max-w-screen-xl px-5 mx-auto mt-2 md:mt-8'>
        <h1 className='text-xl font-semibold leading-9'>RESOURCES</h1>
        <div className='grid grid-cols-12 py-2 my-4'>
          <div className='grid-col-span-2'>
            <h1>TYPE</h1>
          </div>
          <div className='grid-col-span-10'>
            <div className='flex flex-row'>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Photograph</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Map</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>News&nbsp;article</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Audio</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Video</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Policy/Law</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Book/Journal</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Artwork</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>Others</span>
                </label>
              </div>
            </div>
          </div>
        </div>
        <div className='grid grid-cols-12 py-2 my-4'>
          <div className='grid-col-span-2'>
            <h1>TIME&nbsp;PERIOD</h1>
          </div>
          <div className='grid-col-span-10'>
            <div className='flex flex-row'>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>1824&#8722;1948</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>1949&#8722;1967</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>1968&#8722;1987</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>1988&#8722;2010</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>2011&#8722;2020</span>
                </label>
              </div>
              <div className='px-2 mx-2'>
                <label className='flex items-center'>
                  <input type='checkbox' className='form-checkbox' />
                  <span className='ml-2'>ALL</span>
                </label>
              </div>
            </div>
          </div>
        </div>

        <div className='grid grid-cols-12 gap-4 mt-20'>
          <div className='items-center col-span-6 p-2 mt-20 text-center'>
            <div className='grid grid-cols-3 gap-4'>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
              <div className='h-40 overflow-hidden text-black shadow sm:rounded-m bg-dirt'></div>
            </div>
          </div>
          <div className='items-center col-span-6 p-2'>
            <div
              className='overflow-hidden text-black shadow sm:rounded-m bg-dirt'
              style={{ height: '30rem' }}
            ></div>
          </div>
        </div>
      </section>
*/
