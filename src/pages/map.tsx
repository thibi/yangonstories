import axios from 'axios';
import { csvParse } from 'd3-dsv';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

// import { resolveHref } from 'next/dist/shared/lib/router/router';
import {
  householdSizes,
  htyCenter,
  mapboxZoomLevels,
  naypyitawCenter,
  periodStyles,
  postCoupCategoryStyles,
} from '@/lib/constants';
import {
  parseMapEvictionsData,
  parseMapFireData,
  parseMapHtyData,
  parseMapNationalisationData,
} from '@/lib/dataparsers';
import {
  buildFilteredDataSet,
  filterHandler,
  flattenArray,
  makeCrossfilterDimsAndGroups,
  makeEmptyFilters,
  swapKeysAndValues,
} from '@/lib/utils';

// import dynamic from 'next/dynamic';
import Layout from '@/components/layout/Layout';
import MapComponent from '@/components/MapWithSidebar';
import Seo from '@/components/Seo';

const tabs = [
  'Evictions',
  'Fire',
  'Nationalisation',
  'Post-coup Hlaing Tharyar',
];

const tabIds = {
  evictions: 'Evictions',
  fire: 'Fire',
  nationalisation: 'Nationalisation',
  postcoup: 'Post-coup Hlaing Tharyar',
};

const tabIdsReversed = swapKeysAndValues(tabIds);

function classNames(...classes: string[]): string {
  return classes.filter(Boolean).join(' ');
}

export default function MapPage() {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  const [currentDataSetName, setCurrentDatasetName] = useState('Evictions');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      if (
        Object.keys(tabIds).includes(router.query.id) &&
        router.query.id !== tabIdsReversed[currentDataSetName]
      ) {
        setCurrentDatasetName(tabIds[router.query.id]);
      }
    }
  }, [router]);

  const setMapId = (mapName) => {
    router.replace({
      query: { ...router.query, id: tabIdsReversed[mapName] },
    });
  };

  // useEffect(() => {
  //   router.replace({
  //     query: { ...router.query, id: tabIdsReversed[currentDataSetName] },
  //  })
  // },[currentDataSetName])

  // Evictions layer variables
  const [evictionsData, setEvictionsData] = useState();
  const [filtersForEvictionsData, setFiltersForEvictionsData] = useState();
  const [filtersForCurrentEvictionsData, setFiltersForCurrentEvictionsData] =
    useState();
  const [tableDataForEvictions, setTableDataForEvictions] = useState();

  const [xfEvictions, setXfEvictions] = useState();
  const [xfDimsEvictions, setXfDimsEvictions] = useState();
  const [xfGroupsEvictions, setXfGroupsEvictions] = useState();

  // Fire layer variables
  const [fireData, setFireData] = useState();
  const [filtersForFireData, setFiltersForFireData] = useState();
  const [filtersForCurrentFireData, setFiltersForCurrentFireData] = useState();
  const [tableDataForFire, setTableDataForFire] = useState();

  const [xfFire, setXfFire] = useState();
  const [xfDimsFire, setXfDimsFire] = useState();
  const [xfGroupsFire, setXfGroupsFire] = useState();

  // Nationalisation layer variables
  const [nationalisationData, setNationalisationData] = useState();
  const [filtersForNationalisationData, setFiltersForNationalisationData] =
    useState();
  const [
    filtersForCurrentNationalisationData,
    setFiltersForCurrentNationalisationData,
  ] = useState();
  const [tableDataForNationalisation, setTableDataForNationalisation] =
    useState();

  const [xfNationalisation, setXfNationalisation] = useState();
  const [xfDimsNationalisation, setXfDimsNationalisation] = useState();
  const [xfGroupsNationalisation, setXfGroupsNationalisation] = useState();

  // Hty layer variables
  const [htyData, setHtyData] = useState();
  const [filtersForHtyData, setFiltersForHtyData] = useState();
  const [filtersForCurrentHtyData, setFiltersForCurrentHtyData] = useState();
  const [tableDataForHty, setTableDataForHty] = useState();

  const [xfHty, setXfHty] = useState();
  const [xfDimsHty, setXfDimsHty] = useState();
  const [xfGroupsHty, setXfGroupsHty] = useState();

  /*
  ******************************  
    Set map styling
  ******************************
  */

  const evictionsLegends = {
    // 'circle-color': '#007cbf',
    mapboxStyles: {
      'circle-color': {
        property: 'Period',
        type: 'categorical',
        stops: [
          ...Object.keys(periodStyles).map((key) => [
            key,
            periodStyles[key].background,
          ]),
        ],
      },
      'circle-radius': {
        property: 'Households',
        type: 'categorical',
        stops: flattenArray(
          mapboxZoomLevels.map((zLevel) =>
            Object.keys(householdSizes).map((key) => [
              {
                zoom: zLevel,
                value: key,
              },
              householdSizes[key] *
                (zLevel > 10 ? (zLevel > 11 ? 1 : 0.5) : 0.2),
            ])
          )
        ),
      },
      'circle-stroke-color': '#aaa',
      'circle-stroke-width': 1,
    },
    otherStyles: {
      'circle-color': {
        getValue: (d) => {
          return periodStyles[d].background;
        },
      },
      'circle-radius': {
        getValue: (d) => {
          return householdSizes[d] * 2;
        },
      },
      'table-columns': [
        { accessor: 'tableId', Header: 'ID' },
        { accessor: 'Location', Header: 'Location' },
        { accessor: 'Township', Header: 'Township' },
        { accessor: 'HouseholdsFull', Header: 'Number of affected households' },
        { accessor: 'Year', Header: 'Year' },
        { accessor: 'Period', Header: 'Period' },
        { accessor: 'Remarks', Header: 'Remarks' },
        { accessor: 'Rationale', Header: 'Rationale for eviction' },
        { accessor: 'DetailedRationale', Header: 'Detailed Rationale' },
        { accessor: 'InfoSource', Header: 'Source', type: 'url' },
      ],
    },
  };

  const fireLegends = {
    // 'circle-color': '#007cbf',
    mapboxStyles: {
      'circle-color': {
        property: 'Period',
        type: 'categorical',
        stops: [
          ...Object.keys(periodStyles).map((key) => [
            key,
            periodStyles[key].background,
          ]),
        ],
      },
      'circle-radius': {
        property: 'Households',
        type: 'categorical',
        // stops: [
        //   ...Object.keys(householdSizes).map((key) => [
        //     key,
        //     householdSizes[key],
        //   ]),
        // ],
        stops: flattenArray(
          mapboxZoomLevels.map((zLevel) =>
            Object.keys(householdSizes).map((key) => [
              {
                zoom: zLevel,
                value: key,
              },
              householdSizes[key] *
                (zLevel > 10 ? (zLevel > 11 ? 1 : 0.8) : 0.5),
            ])
          )
        ),
      },
      'circle-stroke-color': '#aaa',
      'circle-stroke-width': 1,
    },
    otherStyles: {
      'circle-color': {
        getValue: (d) => {
          return periodStyles[d].background;
        },
      },
      'circle-radius': {
        getValue: (d) => {
          return householdSizes[d] * 2;
        },
      },
      'table-columns': [
        { accessor: 'tableId', Header: 'ID' },
        { accessor: 'Year', Header: 'Year' },
        // { accessor: 'Period', Header: 'Period' },
        { accessor: 'Township', Header: 'Township' },
        { accessor: 'Location', Header: 'Location' },
        { accessor: 'HouseholdsFull', Header: 'Households' },
        { accessor: 'InfoSource', Header: 'Source', type: 'url' },
      ],
    },
    initialViewState: {
      latitude: naypyitawCenter.latitude,
      longitude: naypyitawCenter.longitude,
      zoom: 4.5,
    },
  };

  const nationalisationLegends = {
    // 'circle-color': '#007cbf',
    mapboxStyles: {
      'circle-color': {
        property: 'Period',
        type: 'categorical',
        stops: [
          ...Object.keys(periodStyles).map((key) => [
            key,
            periodStyles[key].background,
          ]),
        ],
      },
      'circle-radius': {
        property: 'Nationalisations',
        // type: 'categorical',
        stops: [
          [1, 5],
          [5, 15],
        ],
      },
      'circle-stroke-color': '#aaa',
      'circle-stroke-width': 1,
    },
    otherStyles: {
      'circle-color': {
        getValue: (d) => {
          return periodStyles[d].background;
        },
      },
      'circle-radius': {
        getValue: () => {
          return 10 * 2;
        },
      },
      'table-columns': [
        { accessor: 'tableId', Header: 'ID' },
        { accessor: 'Year', Header: 'Year' },
        // { accessor: 'Period', Header: 'Period' },
        { accessor: 'Township', Header: 'Township' },
        { accessor: 'Location', Header: 'Location' },
        { accessor: 'Detail', Header: 'Detail' },
        { accessor: 'WhatIsThereNow', Header: 'What is there now?' },
        { accessor: 'InfoSource', Header: 'Source', type: 'url' },
      ],
    },
    initialViewState: {
      latitude: naypyitawCenter.latitude,
      longitude: naypyitawCenter.longitude,
      zoom: 4.5,
    },
  };

  const htyLegends = {
    // 'circle-color': '#007cbf',
    mapboxStyles: {
      'circle-color': {
        property: 'Category',
        type: 'categorical',
        stops: [
          ...Object.keys(postCoupCategoryStyles).map((key) => [
            key,
            postCoupCategoryStyles[key].background,
          ]),
        ],
      },
      'circle-radius': [
        'interpolate',
        ['linear'],
        ['zoom'],
        // zoom is 5 (or less) -> circle radius will be 1px
        10,
        1,
        // zoom is 10 (or greater) -> circle radius will be 5px
        12,
        5,
      ],
      'circle-stroke-color': '#aaa',
      'circle-stroke-width': 1,
    },
    otherStyles: {
      'circle-color': {
        getValue: (d) => {
          return postCoupCategoryStyles[d].background;
        },
      },
      'circle-radius': 5,
      'table-columns': [
        { accessor: 'id', Header: 'ID' },
        { accessor: 'Date', Header: 'Date' },
        { accessor: 'Category', Header: 'Category' },
        { accessor: 'Location', Header: 'Location' },
        { accessor: 'Headline', Header: 'Headline' },
        // { accessor: 'Media', Header: 'Media', type: 'url' },
        // { accessor: 'MediaCredit', Header: 'Media Credit' },
        { accessor: 'Source', Header: 'Source', type: 'url' },
        { accessor: 'Text', Header: 'Text' },
      ],
    },
    initialViewState: {
      latitude: htyCenter.latitude,
      longitude: htyCenter.longitude,
      zoom: 11.5,
      maxZoom: 15,
    },
  };

  const evictionsPopUpFields = {
    Location: { key: 'Location', label: 'Location' },
    HouseholdsNum: {
      key: 'HouseholdsNum',
      label: 'Number of affected households',
    },
    Year: { key: 'Year', label: 'Year' },
    Rationale: { key: 'Rationale', label: 'Rationale' },
  };

  const firePopUpFields = {
    Township: { key: 'Township', label: 'Township' },
    HouseholdsNum: {
      key: 'HouseholdsNum',
      label: 'Number of affected households',
    },
  };

  const nationalisationPopUpFields = {
    Township: { key: 'Township', label: 'Township' },
    Location: { key: 'Location', label: 'Approximate location' },
  };

  const htyPopUpFields = {
    Headline: { key: 'Headline', label: 'Headline' },
    Category: { key: 'Category', label: 'Category' },
    Date: { key: 'Date', label: 'Date' },
  };

  /*
  ******************************  
    Load initial data
  ******************************
  */

  // Evictions data loading
  useEffect(() => {
    // axios({
    //   method: 'get',
    //   url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVVQEo0-8PCRxyvcUEPtiLTM88JSnnYVhsc5pg-9EwfkB0bOPNs-ImGhFapeY3xo8HAxnEn3cZXb8J/pub?gid=1930624359&single=true&output=csv',
    //   responseType: 'stream',
    // })
    //   .then(function (response) {
    //     // console.log(response)
    //     if (response && response.data) {
    //       return new Promise((resolve) => {
    //         const parsedMapEvictionsData = parseMapEvictionsData(
    //           csvParse(response.data)
    //         );
    //         resolve(parsedMapEvictionsData);
    //       });
    //     }
    //   })
    //   .then(function (parsedMapEvictionsData) {
    // console.log(parsedMapEvictionsData)
    const parsedMapEvictionsData = parseMapEvictionsData();
    setEvictionsData(parsedMapEvictionsData.data);
    setFiltersForEvictionsData(parsedMapEvictionsData.filters);
    setFiltersForCurrentEvictionsData(
      makeEmptyFilters(parsedMapEvictionsData.filters)
    );
    setTableDataForEvictions(parsedMapEvictionsData.tableData);

    setXfEvictions(parsedMapEvictionsData.xfilter);
    const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(
      parsedMapEvictionsData.filterCats,
      parsedMapEvictionsData.xfilter
    );
    setXfDimsEvictions(xfDims);
    setXfGroupsEvictions(xfGroups);
    // });
  }, []);

  // Fire data loading
  useEffect(() => {
    // axios({
    //   method: 'get',
    //   url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVVQEo0-8PCRxyvcUEPtiLTM88JSnnYVhsc5pg-9EwfkB0bOPNs-ImGhFapeY3xo8HAxnEn3cZXb8J/pub?gid=972531146&single=true&output=csv',
    //   responseType: 'stream',
    // })
    //   .then(function (response) {
    // console.log(response)
    //   if (response && response.data) {
    //     return new Promise((resolve) => {
    //       const parsedMapFireData = parseMapFireData(csvParse(response.data));
    //       resolve(parsedMapFireData);
    //     });
    //   }
    // })
    // .then(function (parsedMapFireData) {
    // console.log(parsedMapFireData)
    const parsedMapFireData = parseMapFireData();
    setFireData(parsedMapFireData.data);
    setFiltersForFireData(parsedMapFireData.filters);
    setFiltersForCurrentFireData(makeEmptyFilters(parsedMapFireData.filters));
    setTableDataForFire(parsedMapFireData.tableData);

    setXfFire(parsedMapFireData.xfilter);
    const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(
      parsedMapFireData.filterCats,
      parsedMapFireData.xfilter
    );
    setXfDimsFire(xfDims);
    setXfGroupsFire(xfGroups);
    // });
  }, []);

  // console.log(filtersForCurrentFireData)

  // Nationalisation data loading
  useEffect(() => {
    // axios({
    //   method: 'get',
    //   url: 'https://docs.google.com/spreadsheets/d/e/2PACX-1vSVVQEo0-8PCRxyvcUEPtiLTM88JSnnYVhsc5pg-9EwfkB0bOPNs-ImGhFapeY3xo8HAxnEn3cZXb8J/pub?gid=1352776799&single=true&output=csv',
    //   responseType: 'stream',
    // })
    //   .then(function (response) {
    //     // console.log(response)
    //     if (response && response.data) {
    //       return new Promise((resolve) => {
    //         const parsedMapNationalisationData = parseMapNationalisationData(
    //           csvParse(response.data)
    //         );
    //         resolve(parsedMapNationalisationData);
    //       });
    //     }
    //   })
    //   .then(function (parsedMapNationalisationData) {
    // console.log(parsedMapNationalisationData)
    const parsedMapNationalisationData = parseMapNationalisationData();
    setNationalisationData(parsedMapNationalisationData.data);
    setFiltersForNationalisationData(parsedMapNationalisationData.filters);
    setFiltersForCurrentNationalisationData(
      makeEmptyFilters(parsedMapNationalisationData.filters)
    );
    setTableDataForNationalisation(parsedMapNationalisationData.tableData);

    setXfNationalisation(parsedMapNationalisationData.xfilter);
    const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(
      parsedMapNationalisationData.filterCats,
      parsedMapNationalisationData.xfilter
    );
    setXfDimsNationalisation(xfDims);
    setXfGroupsNationalisation(xfGroups);
    // });
  }, []);

  // Hty data loading
  useEffect(() => {
    const parsedMapHtyData = parseMapHtyData();
    setHtyData(parsedMapHtyData.data);
    setFiltersForHtyData(parsedMapHtyData.filters);
    setFiltersForCurrentHtyData(makeEmptyFilters(parsedMapHtyData.filters));
    setTableDataForHty(parsedMapHtyData.tableData);

    setXfHty(parsedMapHtyData.xfilter);
    const { xfDims, xfGroups } = makeCrossfilterDimsAndGroups(
      parsedMapHtyData.filterCats,
      parsedMapHtyData.xfilter
    );
    setXfDimsHty(xfDims);
    setXfGroupsHty(xfGroups);
  }, []);

  return (
    <Layout>
      <Seo title='Map' description='Yangon Stories | Map' />

      <div className='fixed z-40 flex justify-end w-screen pr-4 m-2 pointer-events-none md:w-screen md:top-30 md:bottom-auto bottom-2 flex-end'>
        <nav
          className='flex space-x-4 pointer-events-auto inline-block whitespace-nowrap p-4 md:p-1 overflow-x-scroll'
          aria-label='Tabs'
        >
          {tabs.map((tab) => (
            <a
              key={tab}
              href='#'
              className={classNames(
                tab === currentDataSetName
                  ? 'bg-brown text-white'
                  : 'bg-white hover:bg-brown',
                'px-3 py-2 font-medium text-sm rounded-md '
              )}
              aria-current={tab ? 'page' : false}
              onClick={() => {
                // console.log('clicking on tab')
                setMapId(tab);
                setSidebarOpen(true);
              }}
            >
              {tab}
            </a>
          ))}
        </nav>
      </div>
      {!sidebarOpen ? (
        <div className='fixed z-40 flex justify-end w-screen pr-4 m-2 pointer-events-none md:w-screen top-30 md:top-40 flex-end'>
          <nav
            className='flex space-x-4 pointer-events-auto'
            aria-label='Filters'
          >
            <a
              key={'filters'}
              href='#'
              className={classNames(
                sidebarOpen ? 'bg-brown text-white' : 'bg-white hover:bg-brown',
                'px-3 py-2 font-medium text-sm rounded-md'
              )}
              aria-current={'filters'}
              onClick={() => {
                // console.log('clicking toggle sidebar')
                setSidebarOpen(!sidebarOpen);
              }}
            >
              {sidebarOpen ? 'Close Filters' : 'Open Filters'}
            </a>
          </nav>
        </div>
      ) : (
        <></>
      )}

      <div className='w-full h-full md:w-auto'>
        {currentDataSetName === 'Evictions' &&
          evictionsData &&
          filtersForEvictionsData &&
          xfGroupsEvictions &&
          filtersForCurrentEvictionsData && (
            <MapComponent
              data={buildFilteredDataSet(
                evictionsData,
                xfDimsEvictions,
                filtersForCurrentEvictionsData
              )}
              filters={filtersForEvictionsData}
              currentFilters={filtersForCurrentEvictionsData}
              filterHandler={filterHandler(
                evictionsData,
                filtersForCurrentEvictionsData,
                setFiltersForCurrentEvictionsData,
                xfEvictions,
                xfDimsEvictions,
                xfGroupsEvictions
              )}
              legends={evictionsLegends}
              popUpFields={evictionsPopUpFields}
              currentGroupCounts={xfGroupsEvictions}
              tableData={buildFilteredDataSet(
                tableDataForEvictions,
                xfDimsEvictions,
                filtersForCurrentEvictionsData,
                true
              )}
              sidebarOpen={sidebarOpen}
              setSidebarOpen={setSidebarOpen}
            />
          )}

        {currentDataSetName === 'Fire' &&
          fireData &&
          filtersForFireData &&
          xfGroupsFire &&
          filtersForCurrentFireData && (
            <MapComponent
              data={buildFilteredDataSet(
                fireData,
                xfDimsFire,
                filtersForCurrentFireData
              )}
              filters={filtersForFireData}
              currentFilters={filtersForCurrentFireData}
              filterHandler={filterHandler(
                fireData,
                filtersForCurrentFireData,
                setFiltersForCurrentFireData,
                xfFire,
                xfDimsFire,
                xfGroupsFire
              )}
              legends={fireLegends}
              popUpFields={firePopUpFields}
              currentGroupCounts={xfGroupsFire}
              tableData={buildFilteredDataSet(
                tableDataForFire,
                xfDimsFire,
                filtersForCurrentFireData,
                true
              )}
              sidebarOpen={sidebarOpen}
              setSidebarOpen={setSidebarOpen}
            />
          )}

        {currentDataSetName === 'Nationalisation' &&
          nationalisationData &&
          filtersForNationalisationData &&
          xfGroupsNationalisation &&
          filtersForCurrentNationalisationData && (
            <MapComponent
              data={buildFilteredDataSet(
                nationalisationData,
                xfDimsNationalisation,
                filtersForCurrentNationalisationData
              )}
              filters={filtersForNationalisationData}
              currentFilters={filtersForCurrentNationalisationData}
              filterHandler={filterHandler(
                nationalisationData,
                filtersForCurrentNationalisationData,
                setFiltersForCurrentNationalisationData,
                xfNationalisation,
                xfDimsNationalisation,
                xfGroupsNationalisation
              )}
              legends={nationalisationLegends}
              popUpFields={nationalisationPopUpFields}
              currentGroupCounts={xfGroupsNationalisation}
              tableData={buildFilteredDataSet(
                tableDataForNationalisation,
                xfDimsNationalisation,
                filtersForCurrentNationalisationData,
                true
              )}
              sidebarOpen={sidebarOpen}
              setSidebarOpen={setSidebarOpen}
            />
          )}

        {currentDataSetName === 'Post-coup Hlaing Tharyar' &&
          htyData &&
          filtersForHtyData &&
          xfGroupsHty &&
          filtersForCurrentHtyData && (
            <MapComponent
              data={buildFilteredDataSet(
                htyData,
                xfDimsHty,
                filtersForCurrentHtyData
              )}
              filters={filtersForHtyData}
              currentFilters={filtersForCurrentHtyData}
              filterHandler={filterHandler(
                htyData,
                filtersForCurrentHtyData,
                setFiltersForCurrentHtyData,
                xfHty,
                xfDimsHty,
                xfGroupsHty
              )}
              legends={htyLegends}
              popUpFields={htyPopUpFields}
              currentGroupCounts={xfGroupsHty}
              tableData={buildFilteredDataSet(
                tableDataForHty,
                xfDimsHty,
                filtersForCurrentHtyData,
                true
              )}
              sidebarOpen={sidebarOpen}
              setSidebarOpen={setSidebarOpen}
            />
          )}
      </div>
    </Layout>
  );
}
