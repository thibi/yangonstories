import Link from 'next/dist/client/link';
import React from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';

const Tabs = () => {
  const [openTab, setOpenTab] = React.useState(1);
  return (
    <div className='flex flex-wrap'>
      <div className='w-full'>
        <div className='grid grid-cols-12'>
          <div className='col-span-1'></div>
          <div className='col-span-2'>
            <div className='flex flex-col mt-20'>
              <div className='my-3 p-2'>
                <h2 className='text-2xl font-bold leading-7 mx-20'>Category</h2>
              </div>
              <div className='font-semibold mx-20 p-2 text-left leading-7 w-4/6'>
                <ul>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 1
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(1);
                        }}
                        data-toggle='tab'
                        href='#link5'
                        role='tablist'
                      >
                        - Publications
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 2
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(2);
                        }}
                        data-toggle='tab'
                        href='#link1'
                        role='tablist'
                      >
                        - Articles
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 3
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(3);
                        }}
                        data-toggle='tab'
                        href='#link1'
                        role='tablist'
                      >
                        - Platforms
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 4
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(4);
                        }}
                        data-toggle='tab'
                        href='#link1'
                        role='tablist'
                      >
                        - Policy
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 5
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(5);
                        }}
                        data-toggle='tab'
                        href='#link1'
                        role='tablist'
                      >
                        - Legal Frameworks
                      </a>
                    </Link>
                  </li>
                  <li>
                    <Link href='/' passHref>
                      <a
                        className={`rounded block leading-normal hover:text-red-300 bg-transparent py-4 ${
                          openTab === 6
                            ? `text-red-300 font-bold`
                            : `text-black`
                        }`}
                        onClick={(e) => {
                          e.preventDefault();
                          setOpenTab(6);
                        }}
                        data-toggle='tab'
                        href='#link1'
                        role='tablist'
                      >
                        - Others.
                      </a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className='col-span-9'>
            <div className='relative flex flex-col min-w-0 break-words w-full mb-6 rounded'>
              <div className='px-4 flex-auto'>
                <div className='tab-content tab-space'>
                  <div className='mt-5 md:mx-26 md:px-10 mx-2 md:mt-20 md:col-span-2'>
                    <div className='shadow overflow-hidden sm:rounded-m bg-dirt text-black'>
                      <div
                        className={openTab === 1 ? 'block' : 'hidden'}
                        id='link1'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Publications
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                      <div
                        className={openTab === 2 ? 'block' : 'hidden'}
                        id='link2'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Articles
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                      <div
                        className={openTab === 3 ? 'block' : 'hidden'}
                        id='link3'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Platforms
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                      <div
                        className={openTab === 4 ? 'block' : 'hidden'}
                        id='link4'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Policy
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                      <div
                        className={openTab === 5 ? 'block' : 'hidden'}
                        id='link5'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Legal Frameworks
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                      <div
                        className={openTab === 6 ? 'block' : 'hidden'}
                        id='link5'
                      >
                        <section className='container max-w-screen-xl mx-auto mt-2 px-5 h-96'>
                          <p className='text-2xl leading-4 font-bold p-2 mb-6'>
                            Others
                          </p>
                          <p className='text-sm p-2'>
                            Show the list of materials(links) of selected
                            category
                          </p>
                        </section>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default function Overview() {
  return (
    <Layout>
      <Seo />
      <Tabs />
    </Layout>
  );
}
