/* To Do 
[ ] Add Title Translation
[ ] Change slugs in Timeline Page 
[ ] Add all periods in periods object
*/
import * as React from 'react';
import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import TimelineData from '../../datasets/Timeline_Long_Text.json';
import { nest } from 'd3-collection';
import useTranslation from 'next-translate/useTranslation';

import ParagraphParser from '@/components/ParagraphParser';

export default function ColonialPeriod() {
  const router = useRouter();
  const { slug } = router.query;

  const { t } = useTranslation('timeline');

  const [currentPeriodData, setCurrentPeriodData] = useState();

  const periods = {
    'colonial-period': {
      name: 'Colonial period',
      key: 'colonialperiod',
      t_key: 'colonial_period',
    },
    'the-independence-and-post-independence-periods': {
      name: 'The Independence and Post-Independence Periods',
      key: 'postindependenceperiod',
      t_key: 'indep_post_indep_period',
    },
    'ne-win-period': {
      name: 'Ne Win',
      key: 'newinperiod',
      t_key: 'ne_win_period',
    },
    'slorc-and-spdc-period': {
      name: 'SLORC and SPDC',
      key: 'slorc_spdcperiod',
      t_key: 'slorc_spdc_period',
    },
    'transition-period': {
      name: 'Transition',
      key: 'transitionperiod',
      t_key: 'transition_period',
    },
    'post-coup-period': {
      name: 'Post 2021 Coup',
      key: 'postcoupperiod',
      t_key: 'postcoup_peroid',
    },
  };

  useEffect(() => {
    const currentPeriod = periods[slug].name;
    const colonialPeriodData = TimelineData.filter((val) => {
      return val.period == currentPeriod;
    });
    const tempPeriodData = nest()
      .key((d) => d['step'])
      .key((d) => d['type'])
      .object(colonialPeriodData);
    // console.log("Creating story steps")

    setCurrentPeriodData(tempPeriodData);
  }, []);

  //console.log("Period Data", currentPeriodData);

  return (
    <Layout>
      <Seo />

      <section className='w-full p-12 mt-2'>
        <h1 className='mb-10 text-2xl font-bold leading-4'>
          {/* To replace with translation text */}
          {t(periods[slug].t_key)}
        </h1>
        {currentPeriodData ? (
          <ParagraphParser data={currentPeriodData} />
        ) : (
          <></>
        )}
      </section>
    </Layout>
  );
}
