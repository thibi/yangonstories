import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import useTranslation from 'next-translate/useTranslation';

const ContactPage = () => {
  const { t } = useTranslation('common');
  return (
    <Layout>
      <Seo title='Contact' description='Yangon Stories | Contact' />
      <section className='container max-w-screen-xl px-5 mx-auto mt-2 bg-white md:mt-16'>
        <div className='px-4 py-5 sm:p-6'>
          <div className='grid items-center justify-center grid-cols-8 gap-4 mb-6'>
            <div className='col-span-1'></div>
            <div className='col-span-6'>
              <p className='block text-sm font-medium text-gray-700'>
                {t('contact_info')}
              </p>
            </div>
            <div className='col-span-1'></div>
          </div>
        </div>

        <div className='mt-10 sm:mt-0'>
          <div className='mx-2 mt-5 md:mx-24 md:px-10 md:mt-0 md:col-span-2'>
            <form
              action='https://docs.google.com/forms/u/3/d/e/1FAIpQLSeIzjycDd9anCGurvOkokH7gm_UW17ak6KJycQKqrMmJgcBaA/formResponse'
              method='POST'
            >
              <div className='overflow-hidden shadow sm:rounded-md'>
                <div className='px-4 py-5 bg-white sm:p-6'>
                  <div className='grid items-center justify-center grid-cols-8 gap-4 mb-6'>
                    <label
                      htmlFor='subject'
                      className='block col-span-1 text-sm font-medium text-right text-gray-700'
                    >
                      Subject * :
                    </label>
                    <div className='col-span-6'>
                      <input
                        type='text'
                        name='entry.443757817'
                        id='subject'
                        autoComplete='given-name'
                        className='block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                      />
                    </div>
                  </div>
                  <div className='grid items-start justify-center grid-cols-8 gap-4 mb-6'>
                    <label
                      htmlFor='message'
                      className='block col-span-1 mt-2 text-sm font-medium text-right text-gray-700'
                    >
                      Message * :
                    </label>
                    <div className='col-span-6'>
                      <textarea
                        id='message'
                        name='entry.226377114'
                        rows={8}
                        className='block w-full mt-1 border border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                        placeholder='your message ...'
                      ></textarea>
                    </div>
                  </div>
                  {/* <div className='grid items-center justify-center grid-cols-8 gap-4 mb-6'>
                    <label className='block col-span-1 text-sm font-medium text-right text-gray-700'>
                      Attachment * :
                    </label>
                    <div className='col-span-6'>
                      <div className='flex justify-center px-6 pt-5 pb-6 mt-1 border-2 border-gray-300 border-dashed rounded-md'>
                        <div className='space-y-1 text-center'>
                          <div className='flex text-sm text-gray-600'>
                            <label
                              htmlFor='file-upload'
                              className='relative font-medium text-indigo-600 bg-white rounded-md cursor-pointer hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500'
                            >
                              <span>Upload a file</span>
                              <input
                                id='file-upload'
                                name='file-upload'
                                type='file'
                                className='sr-only'
                              />
                            </label>
                            <p className='pl-1'>or drag and drop</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> */}
                  <div className='grid items-center justify-center grid-cols-8 gap-4 mb-6'>
                    <label
                      htmlFor='name'
                      className='block col-span-1 text-sm font-medium text-right text-gray-700'
                    >
                      Name * :
                    </label>
                    <div className='col-span-6'>
                      <input
                        type='text'
                        name='entry.1039264132'
                        id='name'
                        autoComplete='given-name'
                        className='block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                      />
                    </div>
                  </div>
                  <div className='grid items-center justify-center grid-cols-8 gap-4 mb-6'>
                    <label
                      htmlFor='email-address'
                      className='block col-span-1 text-sm font-medium text-right text-gray-700'
                    >
                      Email * :
                    </label>
                    <div className='col-span-6'>
                      <input
                        type='text'
                        name='entry.147100589'
                        id='email-address'
                        autoComplete='email'
                        className='block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm'
                      />
                    </div>
                  </div>
                </div>
                <div className='px-4 py-3 text-right bg-white sm:px-6'>
                  <button
                    type='submit'
                    className='inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
                  >
                    Submit
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default ContactPage;
