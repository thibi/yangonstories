import { useEffect, useMemo, useState } from 'react';

import ImageGallery from '@/components/ImageGallery';
import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
import cartoonStoriesData from '../../datasets/Cartoon_Stories.json';
import { getImagePath } from '@/lib/utils';
import { nest } from 'd3-collection';
import { useRouter } from 'next/router';

const CartoonsPage = () => {
  const [currentDisplay, setCurrentDisplay] = useState();
  const [menuType, setMenuType] = useState('stories');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  const CartoonsData = useMemo(() => {
    return nest()
      .key((d) => d.ID)
      .key((d) => d.Type)
      .object(cartoonStoriesData);
  }, []);

  //console.log(Object.values(nestCartoonsData).sort());

  const getCurrentDisplay = (id) => {
    if (!id)
      return {
        title: '',
        synopsis: '',
        images: [],
      };

    const currentStoryRows = CartoonsData[id];

    const storyTitle =
      router.locale == 'mm' && currentStoryRows.title[0].script_mm
        ? currentStoryRows.title[0].script_mm
        : currentStoryRows.title[0].script_en;
    const synopsis =
      router.locale == 'mm' && currentStoryRows.title[0].script_mm
        ? currentStoryRows.synopsis[0].script_mm
        : currentStoryRows.synopsis[0].script_en;

    const images = currentStoryRows.image.map((row) => {
      const imageId =
        row['ImageIds'] && row['ImageIds'].length > 0 ? row['ImageIds'][0] : '';

      return {
        caption:
          router.locale == 'mm' && row['script_mm']
            ? row['script_mm']
            : row['script_en'],
        image: getImagePath(imageId, 'lg'),
        thumbnail: getImagePath(imageId, 'sm'),
      };
    });

    return {
      title: storyTitle,
      synopsis: synopsis,
      images: images,
    };
  };

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-5'>
          <StoriesMenu
            currentSelection='cartoons'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='md:col-span-4'>
            {currentDisplay ? (
              <>
                <div
                  className='cursor-pointer hover:underline'
                  onClick={() => setCurrentDisplay(null)}
                >
                  {'< BACK'}
                </div>
                <div className='text-center'>
                  <div className='p-4 m-4'>
                    <h1 className='text-2xl font-bold'>
                      {currentDisplay.title}
                    </h1>
                  </div>
                  <div className='p-2 m-4 mb-8'>
                    <p>{currentDisplay.synopsis}</p>
                  </div>
                  <ImageGallery
                    images={currentDisplay.images}
                    captionPosition={'side'}
                  />
                </div>
              </>
            ) : (
              <div>
                <div className='story-wrapper'>
                  {Object.keys(CartoonsData)
                    .sort()
                    .map((story, i) => {
                      const storyTitle =
                        router.locale == 'mm' &&
                        CartoonsData[story].title[0].script_mm
                          ? CartoonsData[story].title[0].script_mm
                          : CartoonsData[story].title[0].script_en;
                      const storySynopsis =
                        router.locale == 'mm' &&
                        CartoonsData[story].title[0].script_mm
                          ? CartoonsData[story].synopsis[0].script_mm
                          : CartoonsData[story].synopsis[0].script_en;
                      return (
                        <div
                          key={'cartoonStory' + i}
                          className='grid grid-cols-1 gap-8 m-10 text-left cursor-pointer md:grid-cols-3 basis-1/4'
                        >
                          <div className='p-6'>
                            <img
                              className='border-4 border-black rounded-3xl grayscale hover:grayscale-0'
                              src={getImagePath(
                                CartoonsData[story].image[0].ImageIds[0],
                                'lg'
                              )}
                              onClick={() =>
                                setCurrentDisplay(getCurrentDisplay(story))
                              }
                              alt={storyTitle + ' - ' + storySynopsis}
                            />
                          </div>
                          <div className='p-8 md:col-span-2'>
                            <div className='h-10 mb-4'>
                              <h1 className='text-2xl font-bold'>
                                {storyTitle}
                              </h1>
                            </div>
                            <p className='py-6 text-sm text-gray-500'>
                              {storySynopsis}
                            </p>
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default CartoonsPage;
