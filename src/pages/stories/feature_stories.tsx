import Link from 'next/link';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
import useTranslation from 'next-translate/useTranslation';

const FeatureStoriesPage = () => {
  // const [currentDisplay, setCurrentDisplay] = useState();
  const [menuType, setMenuType] = useState('stories');
  const { t } = useTranslation('stories');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  const stories = [
    {
      storyTitle: t('common:evictions'),
      synopsis: '',
      imagePath: '/images/stories-bg-events.jpg',
      storyPath: '/',
    },
    {
      storyTitle: t('common:fire'),
      synopsis: '',
      imagePath: '/images/stories-bg-events.jpg',
      storyPath: '/stories/fires_story',
    },
    {
      storyTitle: t('common:nationalisation'),
      synopsis: '',
      imagePath: '/images/stories-bg-events.jpg',
      storyPath: '/stories/nationalisations_story',
    },
    {
      storyTitle: t('common:post_coup_essay'),
      synopsis: '',
      imagePath: '/images/stories-bg-events.jpg',
      storyPath: '/postcoup/essay',
    },
  ];

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 my-6 md:grid-cols-5 md:gap-4'>
          <StoriesMenu
            currentSelection='featured_stories'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='md:col-span-4'>
            {stories.map((story, i) => {
              const { storyTitle, synopsis, imagePath, storyPath } = story;
              return (
                <Link href={storyPath} passHref key={'featureStory' + i}>
                  <div className='grid grid-cols-1 gap-8 m-10 text-left cursor-pointer md:grid-cols-3 basis-1/4'>
                    <div className='p-6'>
                      <img
                        className='border-4 border-black rounded-3xl grayscale hover:grayscale-0'
                        src={imagePath}
                        alt={storyTitle + ' - ' + synopsis}
                      />
                    </div>
                    <div className='p-8 md:col-span-2'>
                      <div className='h-10 mb-4'>
                        <h1 className='text-2xl font-bold'>{storyTitle}</h1>
                      </div>
                      <p className='py-6 text-sm text-gray-500'>{synopsis}</p>
                    </div>
                  </div>
                </Link>
              );
            })}
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default FeatureStoriesPage;
