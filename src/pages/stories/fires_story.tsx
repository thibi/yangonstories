import { useEffect, useState } from 'react';

import FeatureStory from '@/components/FeatureStory';
import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import { nest } from 'd3-collection';
import storyStepsData from '../../datasets/Fire_Story.json';

const FiresStoryPage = () => {
  // const [currentDisplay, setCurrentDisplay] = useState();

  // const [menuType, setMenuType] = useState('stories');

  // const router = useRouter();

  // useEffect(() => {
  //   if (router && router.query && router.query.id) {
  //     setMenuType(router.query.id);
  //   }
  // }, [router]);

  // const getCurrentStory = (id) => {
  //   const currentStory = interviewsData.filter((d) => d['ID'] === id);

  //   setCurrentDisplay(currentStory[0]);
  // };

  const [storyData, setStoryData] = useState();

  useEffect(() => {
    const tempStoryData = nest()
      .key((d) => d['step'])
      .key((d) => d['type'])
      .object(storyStepsData);
    // console.log("Creating story steps")

    setStoryData(tempStoryData);
  }, []);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2'>
        <div className='grid grid-cols-5 gap-4 my-6'>
          {/* <StoriesMenu
            currentSelection='Interviews'
            optionsSet={menuType ? menuType : 'stories'}
          /> */}
          <div className='col-span-5'>
            <FeatureStory
              storyData={storyData}
              pathId={'fire'}
              pathLabel={'Interactive map of Fires'}
            />
          </div>
          <div className='col-span-5'></div>
        </div>
      </section>
    </Layout>
  );
};

export default FiresStoryPage;
