import { useState } from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import useTranslation from 'next-translate/useTranslation';
import StoriesMenu from '@/components/StoriesMenu';
import { useRouter } from 'next/router';

const BookletPage = () => {
  const [menuType, setMenuType] = useState('stories');
  const { t } = useTranslation('stories');
  const router = useRouter();
  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-5'>
          <StoriesMenu
            currentSelection='booklet'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='col-span-4 text-center'>
            <div className='story-wrapper'>
              <h1 className='text-2xl font-bold text-center'>
                {t('booklet_desc')}
              </h1>
              <img
                className='w-full mx-auto my-8 md:w-96'
                src={`/booklet/${
                  router.locale === 'mm'
                    ? 'booklet_mm_cover.jpg'
                    : 'booklet_cover.png'
                }`}
              />
              <p className='my-8'>{t('booklet_about')}</p>
              <button className='inline-flex items-center px-4 py-2 m-4 font-medium text-white border border-transparent rounded-md shadow-sm text-md bg-brown'>
                <a
                  href='/booklet/Stories of displacement from Yangon (online).pdf'
                  target='_blank'
                  download
                >
                  {t('download_booklet_en')}
                </a>
              </button>
              <button className='inline-flex items-center px-4 py-2 m-4 font-medium text-white border border-transparent rounded-md shadow-sm text-md bg-brown'>
                <a
                  href='/booklet/Stories of Displacement_MM (online).pdf'
                  target='_blank'
                  download
                >
                  {t('download_booklet_mm')}
                </a>
              </button>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default BookletPage;
