import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import ParseMultilineText from '@/components/ParseMultilineText';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
import YouTube from 'react-youtube';
import poemsData from '../../datasets/Poems.json';
import { useRouter } from 'next/router';

const PoemsPage = () => {
  const [menuType, setMenuType] = useState('stories');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 md:grid-cols-5 gap-4 my-6'>
          <StoriesMenu
            currentSelection='poems'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='col-span-4'>
            <div className='story-wrapper'>
              {poemsData.map((story, i) => {
                return (
                  <div key={'poem' + i} className='md:m-10 text-left '>
                    <div className='p-8 '>
                      <div className='mb-4 text-center '>
                        <h1 className='text-2xl font-bold'>
                          {story['Title (English)']}
                        </h1>
                        <h1 className='text-2xl font-bold'>
                          {story['Title (Burmese)']}
                        </h1>

                        <p className='mt-2 text-center text-gray-500'>
                          {story['Name of poet']}
                        </p>
                        {/* <p
                          className='p-4 m-4 text-gray-500 border-4 border-black rounded-3xl border-brown'
                        > */}
                        <div className='flex flex-wrap p-4 m-4 text-gray-500 border-4 border-black rounded-3xl border-brown'>
                          <div className='flex-1 md:p-8'>
                            <ParseMultilineText
                              text={story['Script (Burmese)']}
                              boldSpeaker={false}
                            />
                          </div>
                          <div className='flex-1 md:p-8'>
                            <ParseMultilineText
                              text={story['English Translation']}
                              boldSpeaker={false}
                            />
                          </div>
                        </div>

                        {/* </p> */}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default PoemsPage;
