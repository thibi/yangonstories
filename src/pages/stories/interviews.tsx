import { useEffect, useState } from 'react';

import Layout from '@/components/layout/Layout';
import ParseMultilineText from '@/components/ParseMultilineText';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';
//import interviewsData from '../../datasets/Interviews.json';
import { useRouter } from 'next/router';

const InterviewsPage = () => {
  const [currentDisplay, setCurrentDisplay] = useState();

  const [menuType, setMenuType] = useState('stories');

  const router = useRouter();

  const interviewsData: any = [];

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  const getCurrentStory = (id) => {
    const currentStory = interviewsData.filter((d) => d['ID'] === id);

    setCurrentDisplay(currentStory[0]);
  };

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2'>
        <div className='grid grid-cols-1 gap-4 my-6 md:grid-cols-5'>
          <StoriesMenu
            currentSelection='interviews'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='md:col-span-4'>
            {currentDisplay ? (
              <>
                <div
                  className='cursor-pointer hover:underline'
                  onClick={() => setCurrentDisplay(null)}
                >
                  {'< BACK'}
                </div>
                <div className='mt-6'>
                  <h1 className='text-2xl font-bold text-center'>
                    {router.locale == 'mm' && currentDisplay['title_mm']
                      ? currentDisplay['title_mm']
                      : currentDisplay['title_en']}
                  </h1>
                  <p className='mt-2 text-center text-gray-500'>
                    {router.locale == 'mm' &&
                    currentDisplay['date_of_interview_mm']
                      ? currentDisplay['date_of_interview_mm']
                      : currentDisplay['date_of_interview_en']}
                  </p>
                  <p className='mt-2 text-center text-gray-500'>
                    {`${
                      router.locale == 'mm' && currentDisplay['pseudonym_mm']
                        ? currentDisplay['pseudonym_mm']
                        : currentDisplay['pseudonym_en']
                    } | 
                      ${
                        router.locale == 'mm' && currentDisplay['gender_mm']
                          ? currentDisplay['gender_mm']
                          : currentDisplay['gender_en']
                      } | Age 
                      ${
                        router.locale == 'mm' && currentDisplay['age_appx_mm']
                          ? currentDisplay['age_appx_mm']
                          : currentDisplay['age_appx_en']
                      } | 
                      ${
                        router.locale == 'mm' && currentDisplay['job_mm']
                          ? currentDisplay['job_mm']
                          : currentDisplay['job_en']
                      }`}
                  </p>
                  <p className='p-10 mt-4 ml-4 text-xl text-gray-500'>
                    <span className='text-3xl'>&ldquo;</span>
                    {router.locale == 'mm' && currentDisplay['quotes_mm']
                      ? currentDisplay['quotes_mm']
                      : currentDisplay['quotes_en']}
                    <span className='text-3xl'>&rdquo;</span>
                  </p>
                  <ParseMultilineText
                    text={
                      router.locale == 'mm' && currentDisplay['script_mm']
                        ? currentDisplay['script_mm']
                        : currentDisplay['script_en']
                    }
                    boldSpeaker={true}
                    speakerSpliter={router.locale == 'mm' ? '။' : ':'}
                  />
                </div>
              </>
            ) : (
              <>
                <div className='story-wrapper'>
                  {interviewsData.length > 0 ? (
                    <>
                      {interviewsData.map((interviews, i) => {
                        return (
                          <div
                            key={'interviewStory-' + i}
                            className='m-10 text-left cursor-pointer'
                          >
                            <h2
                              className='text-xl font-bold '
                              onClick={() => getCurrentStory(interviews.ID)}
                            >
                              {router.locale == 'mm' && interviews['title_mm']
                                ? interviews['title_mm']
                                : interviews['title_en']}
                            </h2>
                            <p
                              className='p-4 m-4 text-gray-500 border-4 border-black rounded-3xl border-brown'
                              onClick={() => getCurrentStory(interviews.ID)}
                            >
                              <span className='text-3xl'>&ldquo;</span>
                              {router.locale == 'mm' && interviews['quotes_mm']
                                ? interviews['quotes_mm']
                                : interviews['quotes_en']}
                              <span className='text-3xl'>&rdquo;</span>
                            </p>
                          </div>
                        );
                      })}
                    </>
                  ) : (
                    <>
                      <div className='mt-8 border rounded-md'>
                        <h4 className='p-10 font-bold text-center text-grey'>
                          Coming Soon .....
                        </h4>
                      </div>
                    </>
                  )}
                </div>
              </>
            )}
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default InterviewsPage;
