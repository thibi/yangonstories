import Link from 'next/link';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import useTranslation from 'next-translate/useTranslation';

const StoriesPage = () => {
  const { t } = useTranslation('stories');
  return (
    <Layout>
      <Seo title='Stories' description='Yangon Stories | Stories' />
      <section className='container max-w-screen-xl mx-auto mt-2 text-center'>
        <div className='grid grid-cols-1 my-6 md:grid-cols-5 md:gap-4'>
          <div className='items-center col-span-1 p-2 '>
            <h2 className='text-xl font-bold uppercase'>
              {t('common:stories')}
            </h2>
          </div>
          <div className='md:col-span-4'>
            <div className='grid grid-cols-1 gap-10 md:grid-cols-2 md:px-20'>
              <div className='px-10'>
                <Link href='/stories/feature_stories' passHref>
                  <div
                    className='mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-events.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold text-center'>
                        {t('common:featured_stories')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('featured_stories_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href='/stories/cartoons' passHref>
                  <div
                    className='mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-cartoons.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold text-center'>
                        {t('common:cartoons')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('cartoons_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href='/stories/booklet' passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/booklet/booklet_background.png)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-400/[0.3] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:booklet')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('booklet_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href='/stories/interviews' passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-interviews.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:interviews')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('interviews_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href='/stories/videos' passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-videos.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-500/[0.5] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:video_clips')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('video_clips_desc')}</p>
              </div>
              <div className='px-10'>
                <Link href='/stories/poems' passHref>
                  <div
                    className='flex items-center justify-center mx-auto border-4 border-black w-60 h-60 rounded-3xl'
                    style={{
                      backgroundImage: `url(/images/stories-bg-poems.jpg)`,
                      backgroundSize: 'cover',
                      backgroundPosition: 'center',
                    }}
                  >
                    <div className='w-full h-full rounded-2xl flex items-center justify-center bg-gray-400/[0.3] hover:bg-gray-600/[0.7] cursor-pointer hover:text-white'>
                      <h1 className='text-2xl font-bold'>
                        {t('common:poems')}
                      </h1>
                    </div>
                  </div>
                </Link>
                <p className='mt-10'>{t('poems_desc')}</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default StoriesPage;
