import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
// import YouTube from 'react-youtube';
import Vimeo from '@u-wave/react-vimeo';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';
import StoriesMenu from '@/components/StoriesMenu';

import videoStoriesData from '../../datasets/Video_Stories.json';

const VideosPage = () => {
  // const [currentDisplay, setCurrentDisplay] = useState();

  const [menuType, setMenuType] = useState('stories');

  const router = useRouter();

  useEffect(() => {
    if (router && router.query && router.query.id) {
      setMenuType(router.query.id);
    }
  }, [router]);

  // const opts = {
  //   // height: "390",
  //   // width: "640",
  //   playerVars: {
  //     autoplay: 0,
  //   },
  // };

  // console.log(videoStoriesData);

  return (
    <Layout>
      <Seo />
      <section className='container max-w-screen-xl mx-auto mt-2 '>
        <div className='grid grid-cols-1 md:grid-cols-5 gap-4 my-6'>
          <StoriesMenu
            currentSelection='videos'
            optionsSet={menuType ? menuType : 'stories'}
          />
          <div className='md:col-span-4'>
            <div className='story-wrapper'>
              {videoStoriesData.map((story, i) => {
                // console.log(story['VideoID']);
                return (
                  <div key={'videoStory' + i} className='m-10 text-left '>
                    <div className='p-8 '>
                      <div className='mb-4 text-center'>
                        <h1 className='text-2xl font-bold'>{story['Title']}</h1>
                      </div>
                    </div>
                    <div className='flex items-center justify-center p-6'>
                      {/* <img
                        className='border-4 border-black rounded-3xl grayscale hover:grayscale-0'
                        src={getImagePath(imageId, 'lg')}
                        onClick={() =>
                          setCurrentDisplay(getCurrentDisplay(id))
                        }
                        alt={storyTitle + ' - ' + synopsis}
                      /> */}
                      <div className='w-full px-5 bg-black border-4 border-black rounded-3xl'>
                        <Vimeo video={story['VideoID']} responsive={true} />
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default VideosPage;
