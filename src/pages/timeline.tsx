import dynamic from 'next/dynamic';
import React, { useEffect, useState } from 'react';

import { parseTimelineData } from '@/lib/dataparsers';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';

const Tabs = () => {
  // console.log("Rendering timline page...")
  useEffect(() => {
    setTimelineData(parseTimelineData());
  }, []);

  const [timelineData, setTimelineData] = useState();
  // const [currentPeriod, setCurrentPeriod] = useState();

  const TimelineScrolly = dynamic(
    () => import('@/components/scrolly/TimelineScrolly'),
    { ssr: false }
  );

  return (
    <>
      <TimelineScrolly timelineData={timelineData} />
    </>
  );
};

export default function Overview() {
  return (
    <Layout>
      <Seo title='Timeline' description='Yangon Stories | Timeline' />
      <Tabs />
    </Layout>
  );
}
