import * as React from 'react';

import Layout from '@/components/layout/Layout';
import Seo from '@/components/Seo';

export default function NotFoundPage() {
  return (
    <Layout>
      <Seo title='Not Found' />
      <main>
        <h1>404!</h1>
      </main>
    </Layout>
  );
}
