const metadata = {
  siteName: 'Yangon Stories',
  description: "Yangon's Stories",
  siteUrl: `https://www.yangonstories.com/`,
  siteImage: 'preview.png',
  siteContent: 'Yangon, Yangon Stories, Myanmar, Rangoon',
  siteContentType: 'website',
};
export { metadata };
