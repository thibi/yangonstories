module.exports = {
  siteUrl: 'https://www.yangonstories.com/',
  generateRobotsTxt: true,
  robotsTxtOptions: {
    policies: [{ userAgent: '*', allow: '/' }],
  }
};
